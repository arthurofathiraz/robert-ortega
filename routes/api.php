<?php

use App\Models\Configuration;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api', 'cors'], 'prefix' => 'v1'], function ($router) {
    Route::post('login', [ApiController::class, 'login']);
    Route::post('logout', [ApiController::class, 'logout']);
    Route::post('refresh', [ApiController::class, 'refresh_token']);
    Route::get('configuration', [ApiController::class, 'get_configuration']);
    Route::post('rsvp', [ApiController::class, 'post_rsvp_code']);
    Route::post('rsvp_send', [ApiController::class, 'post_rsvp_send']);
});
