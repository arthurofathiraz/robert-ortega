<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurations', function (Blueprint $table) {
            $table->id();
            $table->string('male_name')->nullable();
            $table->string('male_nickname')->nullable();
            $table->text('male_photo')->nullable();
            $table->string('male_description')->nullable();
            $table->string('female_name')->nullable();
            $table->string('female_nickname')->nullable();
            $table->text('female_photo')->nullable();
            $table->string('female_description')->nullable();
            $table->date('akad_date')->nullable();
            $table->string('akad_name')->nullable();
            $table->string('akad_time')->nullable();
            $table->text('akad_longitude')->nullable();
            $table->text('akad_latitude')->nullable();
            $table->text('akad_maps')->nullable();
            $table->date('walimah_date')->nullable();
            $table->string('walimah_name')->nullable();
            $table->string('walimah_time')->nullable();
            $table->text('walimah_longitude')->nullable();
            $table->text('walimah_latitude')->nullable();
            $table->text('walimah_maps')->nullable();
            $table->text('guide_file')->nullable();
            $table->text('invitation_file')->nullable();
            $table->text('qris_file')->nullable();
            $table->text('protocol_file')->nullable();
            $table->text('song_file')->nullable();
            $table->text('bank_account_number')->nullable();
            $table->text('bank_account_name')->nullable();
            $table->text('bank_account')->nullable();
            $table->text('zoom_live_url')->nullable();
            $table->text('meet_live_url')->nullable();
            $table->text('instagram_live_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configurations');
    }
}
