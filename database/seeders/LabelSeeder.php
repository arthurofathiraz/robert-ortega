<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('labels')->insert([
            [
                'name' => 'max_50_pax',
                'description' => 'Undangan 50 pax',
                'whatsapp_template' => "*[UNDANGAN TERBATAS]*
Assalamu'alaykum Warahmatullahi Wabarakatuh

Bismillahirahmanirrahim.
Tanpa mengurangi rasa hormat, perkenankan kami mengundang *{invitation_name}* sebagai teman sekaligus sahabat, untuk menghadiri acara resepsi pernikahan kami:

*Talita & Fathariz Arthuri*
📅 Hari/Tanggal : Minggu, 07 November 2021
⏱️ Pukul : 07.30 - 13:00 WIB
📍 Lokasi : Masjid Nurul Jamil

Dalam upaya pemutusan rantai penyebaran virus COVID-19, dimohon tamu undangan agar dapat mematuhi protokol kesehatan dengan menjaga jarak, wajib menggunakan masker.

Merupakan suatu kebahagiaan bagi kami apabila Saudara/i berkenan untuk hadir dan memberikan doa restu dengan mengharapkan keberkahan.

Wassalamu'alaykum Warahmatullahi Wabarakatuh

Hormat Kami,
*_Talita & Fathariz_*",
                'priority' => 1,
            ],
            [
                'name' => 'max_125_pax',
                'description' => 'Undangan 125 pax',
                'whatsapp_template' => "*[UNDANGAN]*
Assalamu'alaykum Warahmatullahi Wabarakatuh

Bismillahirahmanirrahim.
Tanpa mengurangi rasa hormat, perkenankan kami mengundang *{invitation_name}* sebagai keluarga, teman sekaligus sahabat, untuk menghadiri acara resepsi pernikahan kami:

*Talita & Fathariz Arthuri*
📅 Hari/Tanggal : Minggu, 07 November 2021
⏱️ Pukul : 07.30 - 13:00 WIB
📍 Lokasi : Masjid Nurul Jamil

Dalam upaya pemutusan rantai penyebaran virus COVID-19, dimohon tamu undangan agar dapat mematuhi protokol kesehatan dengan menjaga jarak, wajib menggunakan masker.

Merupakan suatu kebahagiaan bagi kami apabila Saudara/i berkenan untuk hadir dan memberikan doa restu dengan mengharapkan keberkahan.

Wassalamu'alaykum Warahmatullahi Wabarakatuh

Hormat Kami,
*_Talita & Fathariz_*",
                'priority' => 2,
            ],
            [
                'name' => 'streaming',
                'description' => 'Undangan melalui platform streaming',
                'whatsapp_template' => "*[UNDANGAN LIVE STREAMING]*
Assalamu'alaykum Warahmatullahi Wabarakatuh

Bismillahirahmanirrahim.
Tanpa mengurangi rasa hormat, perkenankan kami mengundang *{invitation_name}* sebagai keluarga, teman sekaligus sahabat, untuk menghadiri acara live streaming pernikahan kami:

*Talita & Fathariz Arthuri*
📅 Hari/Tanggal : Minggu, 07 November 2021
⏱️ Pukul : 07.30 - 13:00 WIB

Merupakan suatu kebahagiaan bagi kami apabila Saudara/i berkenan untuk menonton live streaming dan memberikan doa restu dengan mengharapkan keberkahan.

Wassalamu'alaykum Warahmatullahi Wabarakatuh

Hormat Kami,
*_Talita & Fathariz_*",
                'priority' => 3,
            ],
        ]);
    }
}
