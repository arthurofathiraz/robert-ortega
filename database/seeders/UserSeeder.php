<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'fathariz arthuri',
                'email' => 'arthurofathariz@gmail.com',
                'username' => 'fathariz',
                'password' => Hash::make('B1smillahFathariz'),
                'role' => 'ADMIN',
            ],
            [
                'name' => 'talita',
                'email' => 'talitaini@gmail.com',
                'username' => 'talita',
                'password' => Hash::make('B1smillahTalita'),
                'role' => 'ADMIN',
            ],
            [
                'name' => 'rizqi reza',
                'email' => 'kija@gmail.com',
                'username' => 'kija',
                'password' => Hash::make('B1smillahKija'),
                'role' => 'USER',
            ],
            [
                'name' => 'wedding organizer rostic',
                'email' => 'rostic@gmail.com',
                'username' => 'rostic',
                'password' => Hash::make('B1smillahRostic'),
                'role' => 'WO',
            ],
            [
                'name' => 'keluarga',
                'email' => 'keluarga@gmail.com',
                'username' => 'keluarga',
                'password' => Hash::make('B1smillahKeluarga'),
                'role' => 'USER',
            ],
            [
                'name' => 'nana',
                'email' => 'nanasuryana@gmail.com',
                'username' => 'nana',
                'password' => Hash::make('B1smillahNana'),
                'role' => 'USER',
            ],
        ]);
    }
}
