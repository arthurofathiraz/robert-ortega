<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configurations')->insert([
            [
                'male_name' => 'Fathariz Arthuri',
                'male_nickname' => 'Faraz',
                'male_photo' => '',
                'male_description' => 'Putra ke-2 dari
Bapak Sony Sumarsono (Alm) dan
Ibu Wiwiek Rosa Fatmawaty',
                'female_name' => 'Talita',
                'female_nickname' => 'Talita',
                'female_photo' => '',
                'female_description' => 'Putri ke-1 dari
Bapak H. Abdillah dan
Ibu Hj. Sri Subardini',
                'akad_date' => '2021-11-07',
                'akad_name' => 'Masjid Nurul Jamil',
                'akad_time' => '07:30',
                'akad_longitude' => '107.61972906957389',
                'akad_latitude' => '-6.8693488623365235',
                'akad_maps' => 'https://goo.gl/maps/pvFwejVixY8AagGd6',
                'walimah_date' => null,
                'walimah_name' => 'Masjid Nurul Jamil',
                'walimah_time' => '11:00',
                'walimah_longitude' => '107.61972906957389',
                'walimah_latitude' => '-6.8693488623365235',
                'walimah_maps' => 'https://goo.gl/maps/pvFwejVixY8AagGd6',
                'guide_file' => '',
                'invitation_file' => '',
                'qris_file' => '',
                'protocol_file' => '',
                'song_file' => '',
                'bank_account_number' => '8850707031',
                'bank_account_name' => 'Fathariz Arthuri',
                'bank_account' => 'BCA',
                'zoom_live_url' => 'https://google.co.id',
                'meet_live_url' => 'https://google.co.id',
                'instagram_live_id' => 'fathariz',
            ]
        ]);
    }
}
