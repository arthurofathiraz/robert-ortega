import Dropzone from "dropzone";
import helper from "./helper";

(function (cash) {
    "use strict";

    // Dropzone
    Dropzone.autoDiscover = false;
    cash(".dropzone").each(function () {
        let options = {
            accept: (file, done) => {
                done();
            },
            complete: (file) => {
                helper.showNotification('success', 'File successfuly uploaded', '');
                setTimeout(function () {
                    window.location.reload(true);
                            window.location.reload(true);
                }, 1000);
            }
        };

        if (cash(this).data("single")) {
            options.maxFiles = 1;
        }

        if (cash(this).data("max-filesize")) {
            options.maxFilesize = parseInt(cash(this).data("max-filesize"));
        }

        if (cash(this).data("file-types")) {
            options.accept = (file, done) => {
                if (
                    cash(this)
                        .data("file-types")
                        .split("|")
                        .indexOf(file.type) === -1
                ) {
                    helper.showNotification('error', 'Files of this type are not accepted', '');
                    done();
                } else {
                    done();
                }
            };
            options.complete = (file) => {
                helper.showNotification('success', 'File successfuly uploaded', '');
                setTimeout(function () {
                    window.location.reload(true);
                            window.location.reload(true);
                }, 1000);
            }
        }

        let dz = new Dropzone(this, options);

        dz.on("maxfilesexceeded", (file) => {
            helper.showNotification('error', 'No more files can be added!', '');
        });
    });
})(cash);
