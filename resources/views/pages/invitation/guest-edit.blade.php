@extends('../../layout/' . $layout)

@section('subhead')
    <title>Fathariz Talita - Wedding CMS - Guest Invitation Edit</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/tabulator/4.9.3/css/tabulator.min.css" rel="stylesheet">
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Guest Invitation Edit</h2>
    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 lg:col-span-6">
            <!-- BEGIN: Form Validation -->
            <div class="intro-y box">
                <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Edit</h2>
                </div>
                <div class="p-5">
                    <form id="inputan-form">
                        <input type="hidden" id="qr_code_token" name="qr_code_token"
                               value="{{ $guest['qr_code_token'] }}"/>
                        <input type="hidden" id="rsvp_code" name="rsvp_code"
                               value="{{ $guest['rsvp_code'] }}"/>
                        <div>
                            <label for="name" class="form-label">Name</label>
                            <input id="name" type="text" class="form-control form__inputan"
                                   placeholder="Input guest name"
                                   value="{{ $guest->name }}">
                            <div id="error-name"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="description" class="form-label">Description</label>
                            <input id="description" type="text" class="form-control form__inputan"
                                   placeholder="Input guest description"
                                   value="{{ $guest->description }}">
                            <div id="error-description"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div>
                            <label for="mobile" class="form-label">Mobile</label>
                            <input id="mobile" type="text" class="form-control form__inputan"
                                   placeholder="Input guest mobile"
                                   value="{{ $guest->mobile }}">
                            <div id="error-mobile"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="invitee_count" class="form-label">Invitee Count</label>
                            <div class="input-group">
                                <input id="invitee_count" type="number" max="8" class="form-control form__inputan"
                                       placeholder="Input guest invitee count"
                                       value="{{ $guest->invitee_count }}" aria-describedby="input-group-invitee-count">
                                <div id="input-group-invitee-count" class="input-group-text">invitee</div>
                            </div>
                            <div id="error-invitee_count"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div>
                            <label for="base" class="form-label">Base</label>
                            <input id="base" type="text" class="form-control form__inputan"
                                   placeholder="Input guest base"
                                   value="{{ $guest->base }}">
                            <div id="error-base"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="category" class="form-label">Category</label>
                            <select id="category" class="tom-select w-full form__inputan"
                                    data-placeholder="Select category">
                                <option value="MALE" {{ ($guest->category == 'MALE' ? 'selected' : '') }}>MALE</option>
                                <option value="FEMALE" {{ ($guest->category == 'FEMALE' ? 'selected' : '') }}>
                                    FEMALE
                                </option>
                            </select>
                            <div id="error-category"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        {{--                        <div class="mt-3">--}}
                        {{--                            <label for="status" class="form-label">Status</label>--}}
                        {{--                            <select id="status" class="tom-select w-full form__inputan"--}}
                        {{--                                    data-placeholder="Select status">--}}
                        {{--                                <option value="DRAFT" {{ ($guest->status == 'DRAFT' ? 'selected' : '') }}>DRAFT</option>--}}
                        {{--                                <option value="APPROVED" {{ ($guest->status == 'APPROVED' ? 'selected' : '') }}>--}}
                        {{--                                    APPROVED--}}
                        {{--                                </option>--}}
                        {{--                                <option value="SENT" {{ ($guest->status == 'SENT' ? 'selected' : '') }}>SENT</option>--}}
                        {{--                                <option value="RSVP" {{ ($guest->status == 'RSVP' ? 'selected' : '') }}>RSVP</option>--}}
                        {{--                            </select>--}}
                        {{--                            <div id="error-status"--}}
                        {{--                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>--}}
                        {{--                        </div>--}}
                        <div class="mt-3">
                            <label for="label" class="form-label">Label</label>
                            <select id="label" data-placeholder="Select label" class="tom-select w-full form__inputan"
                                    multiple>
                                @foreach($label as $lab)
                                    @php
                                        $selected = '';
                                        foreach ($guest->invitation_labels as $invitation) {
                                            if ($invitation->label_id == $lab->id) {
                                                $selected = 'selected';
                                            }
                                        }
                                    @endphp
                                    <option value="{{ $lab->id }}" {{ $selected }}><strong>{{ $lab->name }}</strong>
                                        ({{ $lab->description }})
                                    </option>
                                @endforeach
                            </select>
                            <div id="error-label"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label>VIP</label>
                            <div class="mt-2">
                                <div class="form-check">
                                    <input id="is_vip" class="form-check-switch form__inputan" type="checkbox"
                                           value="{{ ($guest->is_vip) ? 'yes' : 'no' }}" {{ ($guest->is_vip) ? 'checked' : '' }}>
                                    <label class="form-check-label" for="is_vip"
                                           id="label-is_vip">{{ ($guest->is_vip) ? 'Yes' : 'No' }}</label>
                                </div>
                            </div>
                            <div id="error-is_vip"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label>Physical Invitation</label>
                            <div class="mt-2">
                                <div class="form-check">
                                    <input id="is_physic_invitation" class="form-check-switch form__inputan"
                                           type="checkbox"
                                           value="{{ ($guest->is_physic_invitation) ? 'yes' : 'no' }}" {{ ($guest->is_physic_invitation) ? 'checked' : '' }}>
                                    <label class="form-check-label" for="is_physic_invitation"
                                           id="label-is_physic_invitation">{{ ($guest->is_physic_invitation) ? 'Yes' : 'No' }}</label>
                                </div>
                            </div>
                            <div id="error-is_physic_invitation"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label>Auto RSVP</label>
                            <div class="mt-2">
                                <div class="form-check">
                                    <input id="is_auto_rsvp" class="form-check-switch form__inputan"
                                           type="checkbox"
                                           value="{{ ($guest->is_auto_rsvp) ? 'yes' : 'no' }}" {{ ($guest->is_auto_rsvp) ? 'checked' : '' }}>
                                    <label class="form-check-label" for="is_auto_rsvp"
                                           id="label-is_auto_rsvp">{{ ($guest->is_auto_rsvp) ? 'Yes' : 'No' }}</label>
                                </div>
                            </div>
                            <div id="error-is_auto_rsvp"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        @if(!empty($guest->name) && !empty($guest->mobile) && !empty($guest->qr_code_token))
                            <div class="mt-3">
                                <label>QR Code</label>
                                <div class="w-full image-fit mt-2" id="qr-code">
                                    {!! \App\Helper::generate_qr_code($guest->name. '<|separator|>' . $guest->mobile . '<|separator|>' . $guest->qr_code_token) !!}
                                </div>
                            </div>
                        @endif
                    </form>
                    <button id="btn-back" class="btn btn-secondary mt-4">Back</button>
                    <button id="btn-update" class="btn btn-primary mt-4">Update</button>
                </div>
            </div>
            <!-- END: Form Validation -->
        </div>
    </div>
@endsection

@section('script')
    <script>
        cash(function () {
            async function update() {
                // Reset state
                cash('#inputan-form').find('.form__inputan').removeClass('border-theme-6')
                cash('#inputan-form').find('.form__inputan-error').html('')

                // Post form
                let name = cash('#name').val()
                let description = cash('#description').val()
                let mobile = cash('#mobile').val()
                let invitee_count = cash('#invitee_count').val()
                let base = cash('#base').val()
                let category = cash('#category').val()
                // let status = cash('#status').val()
                let label = cash('#label').val()
                let is_vip = cash('#is_vip').val()
                let is_physic_invitation = cash('#is_physic_invitation').val()
                let is_auto_rsvp = cash('#is_auto_rsvp').val()
                let qr_code_token = cash('#qr_code_token').val()
                let rsvp_code = cash('#rsvp_code').val()

                // Loading state
                cash('#btn-update').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                cash('#btn-back').attr('disabled', true);
                await helper.delay(1000)

                axios.post('{{ route('invitation-guest-edit', $id) }}', {
                    name: name,
                    description: description,
                    mobile: mobile,
                    invitee_count: invitee_count,
                    base: base,
                    category: category,
                    // status: status,
                    label: label,
                    is_vip: is_vip,
                    is_physic_invitation: is_physic_invitation,
                    is_auto_rsvp: is_auto_rsvp,
                    qr_code_token: qr_code_token,
                    rsvp_code: rsvp_code,
                }).then(res => {
                    cash('#btn-update').html('Update');
                    cash('#btn-back').removeAttr('disabled');
                    if (!res.data.success) {
                        for (const [key, val] of Object.entries(res.data.errors)) {
                            cash(`#${key}`).addClass('border-theme-6')
                            cash(`#error-${key}`).html(val)
                        }
                        helper.showNotification('error', 'error update data', 'harap cek kembali inputan anda');
                    } else {
                        helper.showNotification('success', 'success update data', 'halaman akan di refresh');
                        setTimeout(function () {
                            window.location.reload(true);
                            window.location.reload(true);
                        }, 500);

                    }
                }).catch(err => {
                    cash('#btn-update').html('Update');
                    cash('#btn-back').removeAttr('disabled');
                    helper.showNotification('error', 'error update data');
                })
            }

            cash('form input').on('keydown', function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            })

            cash('#btn-update').on('click', function () {
                update()
            })

            cash('#btn-back').on('click', function () {
                window.location.href = "{{ route('invitation-guest-view') }}";
            })

            cash('#is_vip').on('click', function () {
                var is_vip = cash('#is_vip').val();
                if (is_vip == 'yes') {
                    cash('#is_vip').attr('value', 'no');
                    cash('#label-is_vip').html('No');
                } else {
                    cash('#is_vip').attr('value', 'yes');
                    cash('#label-is_vip').html('Yes');
                }
            })

            cash('#is_physic_invitation').on('click', function () {
                var is_physic_invitation = cash('#is_physic_invitation').val();
                if (is_physic_invitation == 'yes') {
                    cash('#is_physic_invitation').attr('value', 'no');
                    cash('#label-is_physic_invitation').html('No');
                } else {
                    cash('#is_physic_invitation').attr('value', 'yes');
                    cash('#label-is_physic_invitation').html('Yes');
                }
            })

            cash('#is_auto_rsvp').on('click', function () {
                var is_auto_rsvp = cash('#is_auto_rsvp').val();
                if (is_auto_rsvp == 'yes') {
                    cash('#is_auto_rsvp').attr('value', 'no');
                    cash('#label-is_auto_rsvp').html('No');
                } else {
                    cash('#is_auto_rsvp').attr('value', 'yes');
                    cash('#label-is_auto_rsvp').html('Yes');
                }
            })
        })
    </script>
@endsection
