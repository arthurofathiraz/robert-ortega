@extends('../../layout/' . $layout)

@section('subhead')
    <title>Fathariz Talita - Wedding CMS - Guest Invitation</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/tabulator/4.9.3/css/tabulator.min.css" rel="stylesheet">
@endsection

@section('subcontent')
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Guest</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <a href="{{ route('invitation-guest-add-view') }}" class="btn btn-primary shadow-md mr-2">Add New Guest</a>
            <div class="dropdown ml-auto sm:ml-0">
                <button class="dropdown-toggle btn px-2 box text-gray-700 dark:text-gray-300" aria-expanded="false">
                    <span class="w-5 h-5 flex items-center justify-center">
                        <i class="w-4 h-4" data-feather="plus"></i>
                    </span>
                </button>
                <div class="dropdown-menu w-40">
                    <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                        <a href="{{ route('invitation-guest-add-view') }}"
                           class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">
                            <i data-feather="file-plus" class="w-4 h-4 mr-2"></i> New Guest
                        </a>
                        {{--                        <a href="{{ route('invitation-guest-add-view') }}"--}}
                        {{--                           class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">--}}
                        {{--                            <i data-feather="file" class="w-4 h-4 mr-2"></i> Import Guest--}}
                        {{--                        </a>--}}
                        {{--                        <a href="{{ route('invitation-guest-add-view') }}"--}}
                        {{--                           class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">--}}
                        {{--                            <i data-feather="send" class="w-4 h-4 mr-2"></i> Whatsapp--}}
                        {{--                        </a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN: HTML Table Data -->
    <div class="intro-y box p-5 mt-5">
        <div class="flex flex-col sm:flex-row sm:items-end xl:items-start">
            <form id="tabulator-html-filter-form" class="xl:flex sm:mr-auto">
                <div class="sm:flex items-center sm:mr-4">
                    <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Field</label>
                    <select id="tabulator-html-filter-field"
                            class="form-select w-full sm:w-32 2xl:w-full mt-2 sm:mt-0 sm:w-auto">
                        <option value="name">Name</option>
                        <option value="status">Status</option>
                        <option value="category">Category</option>
                        <option value="label">Label</option>
                    </select>
                </div>
                <div class="sm:flex items-center sm:mr-4 mt-2 xl:mt-0">
                    <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Value</label>
                    <input id="tabulator-html-filter-value" type="text"
                           class="form-control sm:w-40 2xl:w-full mt-2 sm:mt-0" placeholder="Search...">
                </div>
                <div class="mt-2 xl:mt-0">
                    <button id="tabulator-html-filter-go" type="button" class="btn btn-primary w-full sm:w-16">Go
                    </button>
                    <button id="tabulator-html-filter-reset" type="button"
                            class="btn btn-secondary w-full sm:w-16 mt-2 sm:mt-0 sm:ml-1">Reset
                    </button>
                </div>
            </form>
            <div class="flex mt-5 sm:mt-0">
                {{--                <button id="tabulator-whatsapp" class="btn btn-outline-secondary w-1/2 sm:w-auto mr-2">--}}
                {{--                    <i data-feather="send" class="w-4 h-4 mr-2"></i> Whatsapp--}}
                {{--                </button>--}}
                <button id="tabulator-print" class="btn btn-outline-secondary w-1/2 sm:w-auto mr-2">
                    <i data-feather="printer" class="w-4 h-4 mr-2"></i> Print
                </button>
                @if(auth()->user()->role == 'ADMIN')
                    <button id="tabulator-imports" class="btn btn-outline-secondary w-1/2 sm:w-auto mr-2">
                        <i data-feather="file" class="w-4 h-4 mr-2"></i> Import
                    </button>
                    <button id="tabulator-bulkapprove" class="btn btn-outline-secondary w-1/2 sm:w-auto mr-2">
                        <i data-feather="check" class="w-4 h-4 mr-2"></i> Bulk Approve
                    </button>
                    <button id="tabulator-bulkapprove-cancel" class="btn btn-outline-secondary w-1/2 sm:w-auto mr-2"
                            style="display: none">
                        <i data-feather="x" class="w-4 h-4 mr-2"></i> Cancel
                    </button>
                    <button id="tabulator-bulkapprove-ok" class="btn btn-primary w-1/2 sm:w-auto mr-2"
                            style="display: none">
                        <i data-feather="check" class="w-4 h-4 mr-2"></i> Approve
                    </button>
                @endif
                <div class="dropdown w-1/2 sm:w-auto" id="tabulator-export">
                    <button class="dropdown-toggle btn btn-outline-secondary w-full sm:w-auto" aria-expanded="false">
                        <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export <i data-feather="chevron-down"
                                                                                        class="w-4 h-4 ml-auto sm:ml-2"></i>
                    </button>
                    <div class="dropdown-menu w-40">
                        <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                            <a id="tabulator-export-csv" href="javascript:;"
                               class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">
                                <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export CSV
                            </a>
                            <a id="tabulator-export-example" href="javascript:;"
                               class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">
                                <i data-feather="file" class="w-4 h-4 mr-2"></i> Export Example
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="overflow-x-auto scrollbar-hidden">
            <div id="tabulator-guest" class="mt-5 table-report table-report--tabulator"></div>
        </div>
    </div>
    <!-- END: HTML Table Data -->

    <div id="btn-modal-delete" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <a data-dismiss="modal" href="javascript:;">
                    <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
                </a>
                <div class="modal-body p-0">
                    <div class="p-5 text-center">
                        <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Warning</div>
                        <div class="text-gray-600 mt-2">Are you sure want to delete this guest?</div>
                    </div>
                    <div class="px-5 pb-8 text-center">
                        <button id="btn-remove-cancel" type="button" data-dismiss="modal"
                                class="btn btn-secondary w-24">
                            Cancel
                        </button>
                        <button id="btn-remove" type="button" class="btn btn-primary w-24">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="btn-modal-bulkapprove" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <a data-dismiss="modal" href="javascript:;">
                    <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
                </a>
                <div class="modal-body p-0">
                    <div class="p-5 text-center">
                        <i data-feather="info" class="w-16 h-16 text-theme-3 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Info</div>
                        <div class="text-gray-600 mt-2">Are you sure want to approve these guests?</div>
                    </div>
                    <div class="px-5 pb-8 text-center">
                        <button id="btn-bulkapprove-cancel" type="button" data-dismiss="modal"
                                class="btn btn-secondary w-24">
                            Cancel
                        </button>
                        <button id="btn-bulkapprove" type="button" class="btn btn-primary w-24">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="btn-modal-approve" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <a data-dismiss="modal" href="javascript:;">
                    <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
                </a>
                <div class="modal-body p-0">
                    <div class="p-5 text-center">
                        <i data-feather="info" class="w-16 h-16 text-theme-3 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Info</div>
                        <div class="text-gray-600 mt-2">Are you sure want to approve this guest?</div>
                    </div>
                    <div class="px-5 pb-8 text-center">
                        <button id="btn-approve-cancel" type="button" data-dismiss="modal"
                                class="btn btn-secondary w-24">
                            Cancel
                        </button>
                        <button id="btn-approve" type="button" class="btn btn-primary w-24">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="btn-modal-whatsapp" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <a data-dismiss="modal" href="javascript:;">
                    <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
                </a>
                <div class="modal-body p-0">
                    <div class="p-5 text-center">
                        <i data-feather="info" class="w-16 h-16 text-theme-3 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Info</div>
                        <div class="text-gray-600 mt-2 mb-5">Are you sure want to send whatsapp to this guest?</div>
                        <input id="is_whatsapp_rsvp" class="form__inputan" type="hidden" value="yes">
                        <div class="mt-3 ml-20 mr-20">
                            <select id="label" data-placeholder="Select label"
                                    class="form-select w-full form__inputan">
                                @foreach($label as $lab)
                                    <option id="option-label-{{ $lab->id }}" value="{{ $lab->id }}">
                                        <strong>{{ $lab->name }}</strong>
                                        ({{ $lab->description }})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mt-3 ml-20 mr-20">
                            <div class="mt-2">
                                <div id="checkbox-whatsapp-qr" class="form-check content-center" style="display: none">
                                    <input id="is_whatsapp_qr" class="form-check-switch form__inputan"
                                           type="checkbox"
                                           value="">
                                    <label class="form-check-label" for="is_whatsapp_qr" id="label-is_whatsapp_qr">
                                        send qr code
                                    </label>
                                </div>
                                <div class="form-check content-center mt-2">
                                    <input id="is_whatsapp_maps" class="form-check-switch form__inputan"
                                           type="checkbox"
                                           value="">
                                    <label class="form-check-label" for="is_whatsapp_maps" id="label-is_whatsapp_maps">
                                        send maps
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="px-5 pb-8 text-center mt-5">
                        <button id="btn-whatsapp-cancel" type="button" data-dismiss="modal"
                                class="btn btn-secondary w-24">
                            Cancel
                        </button>
                        <button id="btn-whatsapp-preview" type="button" class="btn btn-warning w-24">
                            Preview
                        </button>
                        <div id="div-whatsapp-preview" style="display: none" class="mr-20 ml-20">
                            <input id="input-whatsapp-preview" type="text" class="form-control"
                                   placeholder="6287885267892"/>
                            <button id="btn-whatsapp-preview-cancel" type="button" class="btn btn-secondary w-24 mt-2">
                                Back
                            </button>
                            <button id="btn-whatsapp-preview-ok" type="button" class="btn btn-primary w-24 mt-2">Send
                            </button>
                        </div>
                        <button id="btn-whatsapp" type="button" class="btn btn-primary w-24">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="btn-modal-imports" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <a data-dismiss="modal" href="javascript:;">
                    <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
                </a>
                <div class="modal-body p-0">
                    <div class="p-5 text-center">
                        <i data-feather="info" class="w-16 h-16 text-theme-3 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Import</div>
                        <div class="text-gray-600 mt-2 mb-5">
                            Import data using csv, click
                            <strong>
                                <a href="javascript:;" id="tabulator-export-example-modal">this</a>
                            </strong>
                            to download example file.
                        </div>
                        <input id="input-imports" class="mt-3 form-control form__inputan" type="file" value="">
                    </div>
                    <div class="px-5 pb-8 text-center mt-5">
                        <button id="btn-imports-cancel" type="button" data-dismiss="modal"
                                class="btn btn-secondary w-24">
                            Cancel
                        </button>
                        <button id="btn-imports" type="button" class="btn btn-primary w-24">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="btn-modal-imports-table" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-xl"
             style="width: 100%;height: 100%;margin: 0;padding: 0;overflow-y: initial !important">
            <div class="modal-content" style="height: auto;min-height: 100%;border-radius: 0;overflow-y: auto;">
                <a data-dismiss="modal" href="javascript:;">
                    <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
                </a>
                <div class="modal-body p-0">
                    <div class="p-5 text-center">
                        <i data-feather="info" class="w-16 h-16 text-theme-3 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Import</div>
                        <div class="text-gray-600 mt-2 mb-5">
                            Imported data from csv.
                        </div>
                        <form id="form-imports-table">
                            <div class="overflow-x-auto scrollbar-hidden">
                                <div id="tabulator-guest-modal" class="mt-5 table-report table-report--tabulator"></div>
                            </div>
                        </form>
                    </div>
                    <div class="px-5 pb-8 text-center mt-5">
                        <button id="btn-imports-table-cancel" type="button" data-dismiss="modal"
                                class="btn btn-secondary w-24">
                            Cancel
                        </button>
                        <button id="btn-imports-table" type="button" class="btn btn-primary w-24">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="btn-modal-changestatus" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <a data-dismiss="modal" href="javascript:;">
                    <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
                </a>
                <div class="modal-body p-0">
                    <div class="p-5 text-center">
                        <i data-feather="info" class="w-16 h-16 text-theme-3 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Info</div>
                        <div class="text-gray-600 mt-2 mb-5">Are you sure want to change status this guest?</div>
                        <div class="mt-3 ml-20 mr-20">
                            <select id="status" data-placeholder="Select status"
                                    class="form-select w-full form__inputan">
                                <option id="option-status-draft" value="DRAFT">
                                    <strong>DRAFT</strong>
                                </option>
                                <option id="option-status-approved" value="APPROVED">
                                    <strong>APPROVED</strong>
                                </option>
                                <option id="option-status-sent" value="SENT">
                                    <strong>SENT</strong>
                                </option>
                                <option id="option-status-rsvp" value="RSVP">
                                    <strong>RSVP</strong>
                                </option>
                                <option id="option-status-checked_in" value="CHECKED_IN">
                                    <strong>CHECKED_IN</strong>
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="px-5 pb-8 text-center mt-5">
                        <button id="btn-changestatus-cancel" type="button" data-dismiss="modal"
                                class="btn btn-secondary w-24">
                            Cancel
                        </button>
                        <button id="btn-changestatus" type="button" class="btn btn-primary w-24">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="btn-modal-reminder" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <a data-dismiss="modal" href="javascript:;">
                    <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
                </a>
                <div class="modal-body p-0">
                    <div class="p-5 text-center">
                        <i data-feather="info" class="w-16 h-16 text-theme-3 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Info</div>
                        <div class="text-gray-600 mt-2 mb-5">Are you sure want to send reminder to this guest?</div>
                        <div class="mt-3 ml-20 mr-20">
                            <select id="reminder" data-placeholder="Select reminder"
                                    class="form-select w-full form__inputan">
                                <optgroup label="Sahabat">
                                    <option id="option-reminder-sesi_1_sahabat" value="sesi_1_sahabat">
                                        <strong>Sesi-1 (sahabat)</strong>
                                    </option>
                                    <option id="option-reminder-sesi_2_sahabat" value="sesi_2_sahabat">
                                        <strong>Sesi-2 (sahabat)</strong>
                                    </option>
                                    <option id="option-reminder-h_2_sahabat" value="h_2_sahabat">
                                        <strong>H-2 (sahabat)</strong>
                                    </option>
                                    <option id="option-reminder-h_7_sahabat" value="h_7_sahabat">
                                        <strong>H-7 (sahabat)</strong>
                                    </option>
                                    <option id="option-reminder-guide_sahabat" value="guide_sahabat">
                                        <strong>Guide (sahabat)</strong>
                                    </option>
                                    <option id="option-reminder-qr_code_sahabat" value="qr_code_sahabat">
                                        <strong>QR Code (sahabat)</strong>
                                    </option>
                                    <option id="option-reminder-maps_sahabat" value="maps_sahabat">
                                        <strong>Maps (sahabat)</strong>
                                    </option>
                                </optgroup>
                                <optgroup label="Orang Tua">
                                    <option id="option-reminder-sesi_1_ortu" value="sesi_1_ortu">
                                        <strong>Sesi-1 (orang tua)</strong>
                                    </option>
                                    <option id="option-reminder-sesi_2_ortu" value="sesi_2_ortu">
                                        <strong>Sesi-2 (orang tua)</strong>
                                    </option>
                                    <option id="option-reminder-h_2_ortu" value="h_2_ortu">
                                        <strong>H-2 (orang tua)</strong>
                                    </option>
                                    <option id="option-reminder-h_7_ortu" value="h_7_ortu">
                                        <strong>H-7 (orang tua)</strong>
                                    </option>
                                    <option id="option-reminder-guide_ortu" value="guide_ortu">
                                        <strong>Guide (orang tua)</strong>
                                    </option>
                                    <option id="option-reminder-qr_code_ortu" value="qr_code_ortu">
                                        <strong>QR Code (orang tua)</strong>
                                    </option>
                                    <option id="option-reminder-maps_ortu" value="maps_ortu">
                                        <strong>Maps (orang tua)</strong>
                                    </option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="px-5 pb-8 text-center mt-5">
                        <button id="btn-reminder-cancel" type="button" data-dismiss="modal"
                                class="btn btn-secondary w-24">
                            Cancel
                        </button>
                        <button id="btn-reminder-preview" type="button" class="btn btn-warning w-24">
                            Preview
                        </button>
                        <div id="div-reminder-preview" style="display: none" class="mr-20 ml-20">
                            <input id="input-reminder-preview" type="text" class="form-control"
                                   placeholder="6287885267892"/>
                            <button id="btn-reminder-preview-cancel" type="button" class="btn btn-secondary w-24 mt-2">
                                Back
                            </button>
                            <button id="btn-reminder-preview-ok" type="button" class="btn btn-primary w-24 mt-2">Send
                            </button>
                        </div>
                        <button id="btn-reminder" type="button" class="btn btn-primary w-24">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/tabulator/4.9.3/js/tabulator.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.17.1/xlsx.min.js"></script>
    <script>
        let table, table_modal;
        (function (cash) {
            "use strict";

            if (cash("#tabulator-guest").length) {
                table = new Tabulator("#tabulator-guest", {
                    ajaxURL: "{{ route('invitation-guest-get') }}",
                    ajaxFiltering: true,
                    ajaxSorting: true,
                    printAsHtml: true,
                    printStyled: true,
                    pagination: "remote",
                    paginationSize: 10,
                    paginationSizeSelector: [10, 20, 30, 40, 50, true],
                    layout: "fitDataStretch",
                    responsiveLayout: "collapse",
                    placeholder: "No matching records found",
                    columns: [
                        // For HTML table
                        {
                            title: "#",
                            field: "bulkcheck",
                            vertAlign: "middle",
                            headerSort: false,
                            visible: false,
                            print: false,
                            download: false,
                            formatter(cell, formatterParams) {
                                var status = cell.getData().status.toString();
                                let divCheck = '';
                                switch (status) {
                                    case "DRAFT":
                                        divCheck += `<input id="bulkcheck-approve-${cell.getData().id}" class="form-control form-check-input" type="checkbox" value="${cell.getData().id}" style="display: none"/>`;
                                        break;
                                    case "APPROVED" :
                                        divCheck = `<input id="bulkcheck-send-${cell.getData().id}" class="form-control form-check-input" type="checkbox" value="${cell.getData().id}" style="display: none"/>`;
                                        break;
                                    case "SENT":
                                        divCheck = `<input id="bulkcheck-resend-${cell.getData().id}" class="form-control form-check-input" type="checkbox" value="${cell.getData().id}" style="display: none"/>`;
                                        break;
                                    case "RSVP":
                                        divCheck = `<input id="bulkcheck-reminder-${cell.getData().id}" class="form-control form-check-input" type="checkbox" value="${cell.getData().id}" style="display: none"/>`;
                                        break;
                                }

                                return divCheck;
                            }
                        },
                        {
                            title: "NAME",
                            minWidth: 50,
                            responsive: 0,
                            field: "name",
                            vertAlign: "middle",
                            print: false,
                            download: false,
                            formatter(cell, formatterParams) {
                                return `<div>
                                    <div class="font-medium whitespace-nowrap">${
                                    cell.getData().name
                                }</div>
                                    <div class="text-gray-600 text-xs whitespace-nowrap">${
                                    cell.getData().description
                                }</div>
                                </div>`;
                            },
                        },
                        {
                            title: "STATUS",
                            minWidth: 200,
                            field: "status",
                            hozAlign: "center",
                            vertAlign: "middle",
                            print: false,
                            download: false,
                            // DRAFT (pertama kali di upload lewat excel
                            // APPROVED (ketika data sudah valid, setelah klik tombol submit)
                            // SENT (undangan dikirim)
                            // RSVP (ketika sudah melakukan registrasi, untuk keluarga auto set RSVP)
                            formatter(cell, formatterParams) {
                                var status = cell.getData().status.toString();
                                let divStatus = ``;
                                switch (status) {
                                    case "DRAFT":
                                        divStatus = `<button class="px-2 py-1 btn btn-secondary changestatus" data-guest-id="` + cell.getData().id + `" data-status="` + status + `">` + status + `</button>`;
                                        break;
                                    case "APPROVED" :
                                        divStatus = `<button class="px-2 py-1 btn btn-primary changestatus" data-guest-id="` + cell.getData().id + `" data-status="` + status + `">` + status + `</button>`;
                                        break;
                                    case "SENT":
                                        divStatus = `<button class="px-2 py-1 btn btn-warning changestatus" data-guest-id="` + cell.getData().id + `" data-status="` + status + `">` + status + `</button>`;
                                        break;
                                    case "RSVP":
                                        divStatus = `<button class="px-2 py-1 btn btn-danger changestatus" data-guest-id="` + cell.getData().id + `" data-status="` + status + `">` + status + `</button>`;
                                        break;
                                    case "CHECKED_IN":
                                        divStatus = `<button class="px-2 py-1 btn btn-success changestatus" data-guest-id="` + cell.getData().id + `" data-status="` + status + `">` + status + `</button>`;
                                        break;
                                }

                                let a = cash(divStatus);
                                cash(a)
                                    .on("click", function () {
                                        cash('#btn-modal-changestatus').modal('show');
                                        cash('#status').find('option').removeAttr('selected');
                                        cash('#option-status-' + status.toLowerCase()).attr('selected', true);
                                        cash('#btn-changestatus').data('guest-id', cell.getData().id);
                                    });

                                return a[0];
                            },
                        },
                        {
                            title: "CATEGORY",
                            minWidth: 200,
                            field: "category",
                            hozAlign: "center",
                            vertAlign: "middle",
                            print: false,
                            download: false,
                        },
                        {
                            title: "LABEL",
                            minWidth: 200,
                            field: "invitation_labels",
                            hozAlign: "center",
                            vertAlign: "middle",
                            headerSort: true,
                            print: false,
                            download: false,
                            formatter(cell, formatterParams) {
                                var invitation_labels = cell.getData().invitation_labels;
                                var divInvitation = '';
                                invitation_labels.forEach(function (invitation) {
                                    divInvitation += `<span class="px-2 py-1 mr-1 btn btn-sm btn-outline-primary">
                                        ` + invitation.labels.name + `
                                    </span>`;
                                });
                                return `<div class="md:flex">` + divInvitation + `</div>`;
                            },
                        },

                            @if(auth()->user()->role == "ADMIN")
                        {
                            title: "ACTIONS",
                            minWidth: 200,
                            field: "actions",
                            responsive: 1,
                            hozAlign: "left",
                            vertAlign: "middle",
                            headerSort: false,
                            print: false,
                            download: false,
                            formatter(cell, formatterParams) {
                                var status = cell.getData().status.toString();
                                var mobile = cell.getData().mobile.toString();
                                var priority_labels = cell.getData().priority_labels;
                                var is_whatsapp_maps = cell.getData().is_whatsapp_maps;
                                var is_whatsapp_qr = cell.getData().is_whatsapp_qr;
                                var is_whatsapp_rsvp = cell.getData().is_whatsapp_rsvp;
                                var is_auto_rsvp = cell.getData().is_auto_rsvp;
                                let html = `<a class="edit flex items-center mr-1 btn btn-secondary btn-sm" href="javascript:;">
                                            <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit
                                        </a>
                                        <button class="delete flex items-center mr-1 btn btn-danger btn-sm" data-guest-id="` + cell.getData().id + `">
                                            <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete
                                        </button>`;

                                if (status == 'APPROVED' && mobile != '') {
                                    html += `<button class="whatsapp flex items-center btn btn-success btn-sm">
                                            <i data-feather="send" class="w-4 h-4 mr-1"></i> Send
                                        </button>`;
                                } else if (status == 'DRAFT') {
                                    html += `<button class="approve flex items-center btn btn-success btn-sm" data-guest-id="` + cell.getData().id + `">
                                            <i data-feather="check" class="w-4 h-4 mr-1"></i> Approve
                                        </button>`;
                                } else if (status == 'SENT' && mobile != '') {
                                    html += `<button class="resend-whatsapp flex items-center btn btn-success btn-sm">
                                            <i data-feather="send" class="w-4 h-4 mr-1"></i> Resend
                                        </button>`;
                                } else if (status == 'RSVP') {
                                    html += `<button class="reminder flex items-center btn btn-success btn-sm" data-guest-id="` + cell.getData().id + `">
                                            <i data-feather="bell" class="w-4 h-4 mr-1"></i> Reminder
                                        </button>`;
                                }

                                let a = cash(`<div class="flex lg:justify-center items-center">` + html + `</div>`);
                                cash(a)
                                    .find(".edit")
                                    .on("click", function () {
                                        window.location.href = "invitation-guest/" + cell.getData().id;
                                    });
                                cash(a)
                                    .find(".delete")
                                    .on("click", function () {
                                        cash('#btn-modal-delete').modal('show');
                                        cash('#btn-remove').data('guest-id', cell.getData().id);
                                    });
                                cash(a)
                                    .find(".whatsapp")
                                    .on("click", function () {
                                        cash('#btn-modal-whatsapp').modal('show');
                                        cash('#btn-whatsapp').data('guest-id', cell.getData().id);
                                        cash('#btn-whatsapp').data('mobile', cell.getData().mobile);
                                        cash('#btn-whatsapp-preview-ok').data('guest-id', cell.getData().id);
                                        cash('#label').find('option').removeAttr('selected');
                                        cash('#option-label-' + priority_labels.id).attr('selected', true);

                                        if (is_whatsapp_maps == 'yes') {
                                            cash('#is_whatsapp_maps').prop('checked', true);
                                            cash('#is_whatsapp_maps').attr('value', 'yes');
                                            cash('#label-is_whatsapp_maps').html('send maps (yes)');
                                        } else {
                                            cash('#is_whatsapp_maps').removeProp('checked');
                                            cash('#is_whatsapp_maps').attr('value', 'no');
                                            cash('#label-is_whatsapp_maps').html('send maps (no)');
                                        }

                                        if (is_whatsapp_qr == 'yes') {
                                            cash('#is_whatsapp_qr').prop('checked', true);
                                            cash('#is_whatsapp_qr').attr('value', 'yes');
                                            cash('#label-is_whatsapp_qr').html('send qr code (yes)');
                                        } else {
                                            cash('#is_whatsapp_qr').removeProp('checked');
                                            cash('#is_whatsapp_qr').attr('value', 'no');
                                            cash('#label-is_whatsapp_qr').html('send qr code (no)');
                                        }

                                        // hide if non auto rsvp
                                        if (is_auto_rsvp) {
                                            cash('#checkbox-whatsapp-qr').show();
                                        } else {
                                            cash('#checkbox-whatsapp-qr').hide();
                                        }

                                        cash('#is_whatsapp_rsvp').attr('value', is_whatsapp_rsvp);
                                    });
                                cash(a)
                                    .find(".resend-whatsapp")
                                    .on("click", function () {
                                        cash('#btn-modal-whatsapp').modal('show');
                                        cash('#btn-whatsapp').data('guest-id', cell.getData().id);
                                        cash('#btn-whatsapp').data('mobile', cell.getData().mobile);
                                        cash('#btn-whatsapp-preview-ok').data('guest-id', cell.getData().id);
                                        cash('#label').find('option').removeAttr('selected');
                                        cash('#option-label-' + priority_labels.id).attr('selected', true);

                                        if (is_whatsapp_maps == 'yes') {
                                            cash('#is_whatsapp_maps').prop('checked', true);
                                            cash('#is_whatsapp_maps').attr('value', 'yes');
                                            cash('#label-is_whatsapp_maps').html('send maps (yes)');
                                        } else {
                                            cash('#is_whatsapp_maps').removeProp('checked');
                                            cash('#is_whatsapp_maps').attr('value', 'no');
                                            cash('#label-is_whatsapp_maps').html('send maps (no)');
                                        }

                                        if (is_whatsapp_qr == 'yes') {
                                            cash('#is_whatsapp_qr').prop('checked', true);
                                            cash('#is_whatsapp_qr').attr('value', 'yes');
                                            cash('#label-is_whatsapp_qr').html('send qr code (yes)');
                                        } else {
                                            cash('#is_whatsapp_qr').removeProp('checked');
                                            cash('#is_whatsapp_qr').attr('value', 'no');
                                            cash('#label-is_whatsapp_qr').html('send qr code (no)');
                                        }

                                        // hide if non auto rsvp
                                        if (is_auto_rsvp) {
                                            cash('#checkbox-whatsapp-qr').show();
                                        } else {
                                            cash('#checkbox-whatsapp-qr').hide();
                                        }

                                        cash('#is_whatsapp_rsvp').attr('value', is_whatsapp_rsvp);
                                    });
                                cash(a)
                                    .find(".reminder")
                                    .on("click", function () {
                                        cash('#btn-modal-reminder').modal('show');
                                        cash('#btn-reminder').data('guest-id', cell.getData().id);
                                        cash('#btn-reminder').data('mobile', cell.getData().mobile);
                                        cash('#btn-reminder-preview-ok').data('guest-id', cell.getData().id);
                                    });
                                cash(a)
                                    .find(".approve")
                                    .on("click", function () {
                                        cash('#btn-modal-approve').modal('show');
                                        cash('#btn-approve').data('guest-id', cell.getData().id);
                                    });

                                return a[0];
                            },
                        },
                        @endif

                        // For print format
                        {
                            title: "NAME",
                            field: "name",
                            visible: false,
                            print: true,
                            download: true,
                        },
                        {
                            title: "DESCRIPTION",
                            field: "description",
                            visible: false,
                            print: true,
                            download: true,
                        },
                        {
                            title: "MOBILE",
                            field: "mobile",
                            visible: false,
                            print: true,
                            download: true,
                        },
                        {
                            title: "INVITEE COUNT",
                            field: "invitee_count",
                            visible: false,
                            print: true,
                            download: true,
                        },
                        {
                            title: "BASE",
                            field: "base",
                            visible: false,
                            print: true,
                            download: true,
                        },
                        {
                            title: "CATEGORY",
                            field: "category",
                            visible: false,
                            print: true,
                            download: true,
                        },
                        {
                            title: "LABEL",
                            field: 'label_download',
                            visible: false,
                            print: true,
                            download: true,
                        },
                        {
                            title: "VIP",
                            field: 'is_vip_download',
                            visible: false,
                            print: true,
                            download: true,
                        },
                        {
                            title: "PHYSICAL INVITATION",
                            field: 'is_physic_invitation_download',
                            visible: false,
                            print: true,
                            download: true,
                        },
                        {
                            title: "AUTO RSVP",
                            field: 'is_auto_rsvp_download',
                            visible: false,
                            print: true,
                            download: true,
                        },
                    ],
                    renderComplete() {
                        feather.replace({
                            "stroke-width": 1.5,
                        });
                    },
                });

                // Redraw table onresize
                window.addEventListener("resize", () => {
                    table.redraw();
                    feather.replace({
                        "stroke-width": 1.5,
                    });
                });

                // Filter function
                function filterHTMLForm() {
                    let field = cash("#tabulator-html-filter-field").val();
                    let type = 'like';
                    let value = cash("#tabulator-html-filter-value").val();
                    table.setFilter(field, type, value);
                }

                // Function download example
                function downloadExample() {
                    axios({
                        url: "{{ 'storage/public/data.csv' }}",
                        method: 'GET',
                        responseType: 'blob'
                    }).then((response) => {
                        const url = window.URL.createObjectURL(new Blob([response.data]));
                        const link = document.createElement('a');
                        link.href = url;
                        link.setAttribute('download', 'data.csv');
                        document.body.appendChild(link);
                        link.click();
                    });
                }

                // On submit filter form
                cash("#tabulator-html-filter-form")[0].addEventListener(
                    "keypress",
                    function (event) {
                        let keycode = event.keyCode ? event.keyCode : event.which;
                        if (keycode == "13") {
                            event.preventDefault();
                            filterHTMLForm();
                        }
                    }
                );

                // On click go button
                cash("#tabulator-html-filter-go").on("click", function (event) {
                    filterHTMLForm();
                });

                // On reset filter form
                cash("#tabulator-html-filter-reset").on("click", function (event) {
                    cash("#tabulator-html-filter-field").val("name");
                    cash("#tabulator-html-filter-value").val("");
                    filterHTMLForm();
                });

                // Export
                cash("#tabulator-export-csv").on("click", function (event) {
                    table.download("csv", "data.csv");
                });

                // Export example
                cash("#tabulator-export-example").on("click", function (event) {
                    downloadExample();
                });

                // Export example
                cash("#tabulator-export-example-modal").on("click", function (event) {
                    downloadExample();
                });

                // Print
                cash("#tabulator-print").on("click", function (event) {
                    table.print();
                });

                // Import
                cash("#tabulator-imports").on("click", function (event) {
                    cash('#btn-modal-imports').modal('show');
                });

                // Bulk approve
                cash("#tabulator-bulkapprove").on("click", function (event) {
                    table.toggleColumn('bulkcheck');

                    cash(`#tabulator-bulkapprove`).hide();
                    cash(`#tabulator-print`).hide();
                    cash(`#tabulator-imports`).hide();
                    cash(`#tabulator-export`).hide();
                    cash(`#tabulator-bulkapprove-cancel`).show();
                    cash(`#tabulator-bulkapprove-ok`).show();

                    cash(`input[id^="bulkcheck-approve-"]`).show();
                });

                cash("#tabulator-bulkapprove-cancel").on("click", function (event) {
                    table.toggleColumn('bulkcheck');

                    cash(`#tabulator-bulkapprove`).show();
                    cash(`#tabulator-print`).show();
                    cash(`#tabulator-imports`).show();
                    cash(`#tabulator-export`).show();
                    cash(`#tabulator-bulkapprove-cancel`).hide();
                    cash(`#tabulator-bulkapprove-ok`).hide();

                    cash(`input[id^="bulkcheck-approve-"]`).hide();
                    cash(`input[id^="bulkcheck-approve-"]`).each(function (i, element) {
                        cash(element).prop('checked', false)
                    })
                });

                cash("#tabulator-bulkapprove-ok").on("click", function (event) {
                    cash('#btn-modal-bulkapprove').modal('show');
                });
            }

            if (cash("#tabulator-guest-modal").length) {
                table_modal = new Tabulator("#tabulator-guest-modal", {
                    printAsHtml: true,
                    printStyled: true,
                    layout: "fitColumns",
                    responsiveLayout: 'collapse',
                    placeholder: "No matching records found",
                    columns: [
                        {
                            title: "NAME",
                            field: "name",
                            hozAlign: "center",
                            vertAlign: "middle",
                            headerSort: false,
                            print: false,
                            download: false,
                            editor: "input",
                            editorParams: {
                                elementAttributes: {
                                    class: "form-control form-control-sm",
                                    maxlength: 100,
                                }
                            },
                            resizable: true,
                            minWidth: 150,
                            validator: "required",
                            formatter(cell, formatterParams) {
                                var no = cell.getData().no;
                                var value = cell.getData().name;
                                var input = `<input name="name[]" id="input-imports-name-` + no + `" type="hidden" value="` + value + `"/>`;
                                return input + value;
                            },
                            cellEdited: function (cell) {
                                var no = cell.getData().no;
                                var value = cell.getData().name;
                                cash(`#input-imports-name-` + no).attr('value', value);
                            },
                        },
                        {
                            title: "DESCRIPTION",
                            field: "description",
                            hozAlign: "center",
                            vertAlign: "middle",
                            headerSort: false,
                            print: false,
                            download: false,
                            editor: "textarea",
                            editorParams: {
                                elementAttributes: {
                                    class: "form-control form-control-sm",
                                    rows: 20,
                                },
                                verticalNavigation: "editor",
                            },
                            resizable: true,
                            minWidth: 500,
                            validator: "required",
                            formatter(cell, formatterParams) {
                                var no = cell.getData().no;
                                var value = cell.getData().description;
                                var input = `<input name="description[]" id="input-imports-description-` + no + `" type="hidden" value="` + value + `"/>`;
                                return input + value;
                            },
                            cellEdited: function (cell) {
                                var no = cell.getData().no;
                                var value = cell.getData().description;
                                cash(`#input-imports-description-` + no).attr('value', value);
                            },
                        },
                        {
                            title: "MOBILE",
                            field: "mobile",
                            hozAlign: "center",
                            vertAlign: "middle",
                            headerSort: false,
                            print: false,
                            download: false,
                            editor: "input",
                            editorParams: {
                                elementAttributes: {
                                    class: "form-control form-control-sm",
                                    maxlength: 50,
                                }
                            },
                            resizable: true,
                            minWidth: 100,
                            validator: "required",
                            formatter(cell, formatterParams) {
                                var no = cell.getData().no;
                                var value = cell.getData().mobile;
                                var input = `<input name="mobile[]" id="input-imports-mobile-` + no + `" type="hidden" value="` + value + `"/>`;
                                return input + value;
                            },
                            cellEdited: function (cell) {
                                var no = cell.getData().no;
                                var value = cell.getData().mobile;
                                cash(`#input-imports-mobile-` + no).attr('value', value);
                            },
                        },
                        {
                            title: "INVITEE COUNT",
                            field: "invitee_count",
                            hozAlign: "center",
                            vertAlign: "middle",
                            headerSort: false,
                            print: false,
                            download: false,
                            editor: "number",
                            editorParams: {
                                elementAttributes: {
                                    class: "form-control form-control-sm",
                                    maxlength: 50,
                                }
                            },
                            resizable: true,
                            validator: "required",
                            formatter(cell, formatterParams) {
                                var no = cell.getData().no;
                                var value = cell.getData().invitee_count;
                                var input = `<input name="invitee_count[]" id="input-imports-invitee_count-` + no + `" type="hidden" value="` + value + `"/>`;
                                return input + value;
                            },
                            cellEdited: function (cell) {
                                var no = cell.getData().no;
                                var value = cell.getData().invitee_count;
                                cash(`#input-imports-invitee_count-` + no).attr('value', value);
                            },
                        },
                        {
                            title: "BASE",
                            field: "base",
                            hozAlign: "center",
                            vertAlign: "middle",
                            headerSort: false,
                            print: false,
                            download: false,
                            editor: "input",
                            editorParams: {
                                elementAttributes: {
                                    class: "form-control form-control-sm",
                                    maxlength: 200,
                                }
                            },
                            resizable: true,
                            minWidth: 100,
                            formatter(cell, formatterParams) {
                                var no = cell.getData().no;
                                var value = cell.getData().base;
                                var input = `<input name="base[]" id="input-imports-base-` + no + `" type="hidden" value="` + value + `"/>`;
                                return input + value;
                            },
                            cellEdited: function (cell) {
                                var no = cell.getData().no;
                                var value = cell.getData().base;
                                cash(`#input-imports-base-` + no).attr('value', value);
                            },
                        },
                        {
                            title: "CATEGORY",
                            field: "category",
                            hozAlign: "center",
                            vertAlign: "middle",
                            headerSort: false,
                            print: false,
                            download: false,
                            editor: "input",
                            editorParams: {
                                elementAttributes: {
                                    class: "form-control form-control-sm",
                                    maxlength: 200,
                                }
                            },
                            resizable: true,
                            validator: "required",
                            formatter(cell, formatterParams) {
                                var no = cell.getData().no;
                                var value = cell.getData().category;
                                var input = `<input name="category[]" id="input-imports-category-` + no + `" type="hidden" value="` + value + `"/>`;
                                return input + value;
                            },
                            cellEdited: function (cell) {
                                var no = cell.getData().no;
                                var value = cell.getData().category;
                                cash(`#input-imports-category-` + no).attr('value', value);
                            },
                        },
                        {
                            title: "LABEL",
                            field: "label",
                            hozAlign: "center",
                            vertAlign: "middle",
                            headerSort: false,
                            print: false,
                            download: false,
                            editor: "select",
                            editorParams: {
                                values: ["streaming", "max_50_pax", "max_125_pax"],
                                defaultValue: "streaming",
                                verticalNavigation: "hybrid",
                                multiselect: true,
                            },
                            resizable: true,
                            minWidth: 300,
                            validator: "required",
                            formatter(cell, formatterParams) {
                                var no = cell.getData().no;
                                var value = cell.getData().label;
                                var input = `<input name="label[]" id="input-imports-label-` + no + `" type="hidden" value="` + value + `"/>`;
                                return input + value;
                            },
                            cellEdited: function (cell) {
                                var no = cell.getData().no;
                                var value = cell.getData().label;
                                cash(`#input-imports-label-` + no).attr('value', value);
                            },
                        },
                        {
                            title: "VIP",
                            field: "is_vip",
                            hozAlign: "center",
                            vertAlign: "middle",
                            headerSort: false,
                            print: false,
                            download: false,
                            editor: "select",
                            editorParams: {
                                values: ["yes", "no"],
                                defaultValue: "no",
                                verticalNavigation: "hybrid",
                            },
                            resizable: true,
                            validator: "required",
                            formatter(cell, formatterParams) {
                                var no = cell.getData().no;
                                var value = cell.getData().is_vip;
                                var input = `<input name="is_vip[]" id="input-imports-is_vip-` + no + `" type="hidden" value="` + value + `"/>`;
                                return input + value;
                            },
                            cellEdited: function (cell) {
                                var no = cell.getData().no;
                                var value = cell.getData().is_vip;
                                cash(`#input-imports-is_vip-` + no).attr('value', value);
                            },
                        },
                        {
                            title: "PHYSICAL INVITATION",
                            field: "is_physic_invitation",
                            hozAlign: "center",
                            vertAlign: "middle",
                            headerSort: false,
                            print: false,
                            download: false,
                            editor: "select",
                            editorParams: {
                                values: ["yes", "no"],
                                defaultValue: "no",
                                verticalNavigation: "hybrid",
                            },
                            resizable: true,
                            validator: "required",
                            formatter(cell, formatterParams) {
                                var no = cell.getData().no;
                                var value = cell.getData().is_physic_invitation;
                                var input = `<input name="is_physic_invitation[]" id="input-imports-is_physic_invitation-` + no + `" type="hidden" value="` + value + `"/>`;
                                return input + value;
                            },
                            cellEdited: function (cell) {
                                var no = cell.getData().no;
                                var value = cell.getData().is_physic_invitation;
                                cash(`#input-imports-is_physic_invitation-` + no).attr('value', value);
                            },
                        },
                        {
                            title: "AUTO RSVP",
                            field: "is_auto_rsvp",
                            hozAlign: "center",
                            vertAlign: "middle",
                            headerSort: false,
                            print: false,
                            download: false,
                            editor: "select",
                            editorParams: {
                                values: ["yes", "no"],
                                defaultValue: "no",
                                verticalNavigation: "hybrid",
                            },
                            resizable: true,
                            validator: "required",
                            formatter(cell, formatterParams) {
                                var no = cell.getData().no;
                                var value = cell.getData().is_auto_rsvp;
                                var input = `<input name="is_auto_rsvp[]" id="input-imports-is_auto_rsvp-` + no + `" type="hidden" value="` + value + `"/>`;
                                return input + value;
                            },
                            cellEdited: function (cell) {
                                var no = cell.getData().no;
                                var value = cell.getData().is_auto_rsvp;
                                cash(`#input-imports-is_auto_rsvp-` + no).attr('value', value);
                            },
                        },
                    ],
                    renderComplete() {
                        feather.replace({
                            "stroke-width": 1.5,
                        });
                    },
                });

                // Redraw table onresize
                window.addEventListener("resize", () => {
                    table.redraw();
                    feather.replace({
                        "stroke-width": 1.5,
                    });
                });
            }
        })(cash);

        cash(function () {
            // remove
            async function remove(id) {
                // set link data
                cash('#btn-remove').data('guest-id', id);

                // Loading state
                cash('#btn-remove-cancel').hide()
                cash('#btn-remove').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post(`invitation-guest/` + id + `/remove`, {
                    id: id,
                }).then(res => {
                    cash('#btn-remove-cancel').show();
                    cash('#btn-remove').html('Ok');
                    cash('#btn-modal-delete').modal('hide');
                    if (!res.data.success) {
                        helper.showNotification('error', 'error remove data', 'harap ulangi');
                    } else {
                        helper.showNotification('success', 'success remove data', '');
                        table.setData(table.getAjaxUrl());
                    }
                }).catch(err => {
                    cash('#btn-remove-cancel').show();
                    cash('#btn-remove').html('Ok');
                    cash('#btn-modal-delete').modal('hide');
                    helper.showNotification('error', 'error remove data');
                })
            }

            cash('#btn-remove').on('click', function () {
                remove(cash(this).data('guest-id'))
            })

            // approve
            async function approve(id) {
                // set link data
                cash('#btn-approve').data('guest-id', id);

                // Loading state
                cash('#btn-approve-cancel').hide()
                cash('#btn-approve').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post(`invitation-guest/` + id + `/approve`, {
                    id: id,
                }).then(res => {
                    cash('#btn-approve-cancel').show();
                    cash('#btn-approve').html('Ok');
                    cash('#btn-modal-approve').modal('hide');
                    if (!res.data.success) {
                        helper.showNotification('error', 'error approve data', 'harap ulangi');
                    } else {
                        helper.showNotification('success', 'success approve data', '');
                        table.setData(table.getAjaxUrl());
                    }
                }).catch(err => {
                    cash('#btn-approve-cancel').show();
                    cash('#btn-approve').html('Ok');
                    cash('#btn-modal-approve').modal('hide');
                    helper.showNotification('error', 'error approve data');
                })
            }

            cash('#btn-approve').on('click', function () {
                approve(cash(this).data('guest-id'))
            })

            // approve
            async function bulkapprove(id) {

                // set link data
                cash('#btn-bulkapprove').data('guest-id', id);

                // Loading state
                cash('#btn-bulkapprove-cancel').hide()
                cash('#btn-bulkapprove').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post(`invitation-guest/bulkapprove`, {
                    id: id,
                }).then(res => {
                    cash('#btn-bulkapprove-cancel').show();
                    cash('#btn-bulkapprove').html('Ok');
                    cash('#btn-modal-bulkapprove').modal('hide');
                    if (!res.data.success) {
                        helper.showNotification('error', 'error bulkapprove data', 'harap ulangi');
                    } else {
                        helper.showNotification('success', 'success bulkapprove data', '');
                        table.setData(table.getAjaxUrl());
                    }
                    table.toggleColumn('bulkcheck');

                    cash(`#tabulator-bulkapprove`).show();
                    cash(`#tabulator-print`).show();
                    cash(`#tabulator-imports`).show();
                    cash(`#tabulator-export`).show();
                    cash(`#tabulator-bulkapprove-cancel`).hide();
                    cash(`#tabulator-bulkapprove-ok`).hide();

                    cash(`input[id^="bulkcheck-approve-"]`).hide();
                    cash(`input[id^="bulkcheck-approve-"]`).each(function (i, element) {
                        cash(element).prop('checked', false)
                    })
                }).catch(err => {
                    cash('#btn-bulkapprove-cancel').show();
                    cash('#btn-bulkapprove').html('Ok');
                    cash('#btn-modal-bulkapprove').modal('hide');
                    helper.showNotification('error', 'error bulkapprove data');
                    table.toggleColumn('bulkcheck');

                    cash(`#tabulator-bulkapprove`).show();
                    cash(`#tabulator-print`).show();
                    cash(`#tabulator-imports`).show();
                    cash(`#tabulator-export`).show();
                    cash(`#tabulator-bulkapprove-cancel`).hide();
                    cash(`#tabulator-bulkapprove-ok`).hide();

                    cash(`input[id^="bulkcheck-approve-"]`).hide();
                    cash(`input[id^="bulkcheck-approve-"]`).each(function (i, element) {
                        cash(element).prop('checked', false)
                    })
                })
            }

            cash('#btn-bulkapprove').on('click', function () {
                var ids = [];
                cash(`input[id^="bulkcheck-approve-"]`).each(function (i, element) {
                    if (cash(element).prop('checked')) {
                        ids.push(cash(element).val());
                    }
                })

                bulkapprove(ids.length > 0 ? ids.join(",") : '')
            })

            // whatsapp
            async function whatsapp(id, mobile) {
                // Loading state
                cash('#btn-whatsapp-cancel').hide();
                cash('#btn-whatsapp-preview').hide();
                cash('#btn-whatsapp').attr('disabled', true);
                cash('#btn-whatsapp').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post(`invitation-guest/` + id + `/send`, {
                    id: id,
                    mobile: mobile,
                    is_whatsapp_maps: cash('#is_whatsapp_maps').val(),
                    is_whatsapp_qr: cash('#is_whatsapp_qr').val(),
                    is_whatsapp_rsvp: cash('#is_whatsapp_rsvp').val(),
                    label_id: cash('#label').val(),
                }).then(res => {
                    cash('#btn-whatsapp-cancel').show();
                    cash('#btn-whatsapp-preview').show();
                    cash('#btn-whatsapp').removeAttr('disabled');
                    cash('#btn-whatsapp').html('Ok');
                    cash('#btn-modal-whatsapp').modal('hide');
                    if (!res.data.success) {
                        helper.showNotification('error', 'error whatsapp data', 'harap ulangi');
                    } else {
                        helper.showNotification('success', 'success whatsapp data', '');
                        table.setData(table.getAjaxUrl());
                    }
                }).catch(err => {
                    cash('#btn-whatsapp-cancel').show();
                    cash('#btn-whatsapp-preview').show();
                    cash('#btn-whatsapp').removeAttr('disabled');
                    cash('#btn-whatsapp').html('Ok');
                    cash('#btn-modal-whatsapp').modal('hide');
                    helper.showNotification('error', 'error whatsapp data');
                })
            }

            cash('#btn-whatsapp').on('click', function () {
                whatsapp(cash(this).data('guest-id'), cash(this).data('mobile'))
            })

            cash('#btn-whatsapp-preview').on('click', function () {
                cash('#div-whatsapp-preview').show();
                cash('#btn-whatsapp-cancel').hide();
                cash('#btn-whatsapp').hide();
                cash('#btn-whatsapp-preview').hide();
            })

            cash('#btn-whatsapp-preview-cancel').on('click', function () {
                cash('#div-whatsapp-preview').hide();
                cash('#btn-whatsapp-cancel').show();
                cash('#btn-whatsapp').show();
                cash('#btn-whatsapp-preview').show();
            })

            // whatsapp_preview
            async function whatsapp_preview(id) {
                // set link data
                cash('#btn-whatsapp-preview-ok').data('guest-id', id);

                // Loading state
                cash('#btn-whatsapp-preview-cancel').hide()
                cash('#btn-whatsapp-preview-ok').attr('disabled', true);
                cash('#btn-whatsapp-preview-ok').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post(`invitation-guest/` + id + `/send-preview`, {
                    id: id,
                    mobile: cash('#input-whatsapp-preview').val(),
                    is_whatsapp_maps: cash('#is_whatsapp_maps').val(),
                    is_whatsapp_qr: cash('#is_whatsapp_qr').val(),
                    is_whatsapp_rsvp: cash('#is_whatsapp_rsvp').val(),
                    label_id: cash('#label').val(),
                }).then(res => {
                    cash('#btn-whatsapp-preview-cancel').show();
                    cash('#btn-whatsapp-preview-ok').removeAttr('disabled');
                    cash('#btn-whatsapp-preview-ok').html('Ok');
                    if (!res.data.success) {
                        helper.showNotification('error', 'error send preview whatsapp data', 'harap ulangi');
                    } else {
                        helper.showNotification('success', 'success send preview whatsapp data', '');
                        table.setData(table.getAjaxUrl());
                    }
                }).catch(err => {
                    cash('#btn-whatsapp-preview-cancel').show();
                    cash('#btn-whatsapp-preview-ok').removeAttr('disabled');
                    cash('#btn-whatsapp-preview-ok').html('Ok');
                    helper.showNotification('error', 'error send preview whatsapp data');
                })
            }

            cash('#btn-whatsapp-preview-ok').on('click', function () {
                whatsapp_preview(cash(this).data('guest-id'))
            })

            // reminder
            async function reminder(id, mobile) {
                // Loading state
                cash('#btn-reminder-cancel').hide();
                cash('#btn-reminder-preview').hide();
                cash('#btn-reminder').attr('disabled', true);
                cash('#btn-reminder').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post(`invitation-guest/` + id + `/reminder`, {
                    id: id,
                    mobile: mobile,
                    type: cash('#reminder').val(),
                }).then(res => {
                    cash('#btn-reminder-cancel').show();
                    cash('#btn-reminder-preview').show();
                    cash('#btn-reminder').removeAttr('disabled');
                    cash('#btn-reminder').html('Ok');
                    if (!res.data.success) {
                        helper.showNotification('error', 'error reminder data', 'harap ulangi');
                    } else {
                        helper.showNotification('success', 'success reminder data', '');
                        table.setData(table.getAjaxUrl());
                    }
                }).catch(err => {
                    cash('#btn-reminder-cancel').show();
                    cash('#btn-reminder-preview').show();
                    cash('#btn-reminder').removeAttr('disabled');
                    cash('#btn-reminder').html('Ok');
                    helper.showNotification('error', 'error reminder data');
                })
            }

            cash('#btn-reminder').on('click', function () {
                reminder(cash(this).data('guest-id'), cash(this).data('mobile'))
            })

            cash('#btn-reminder-preview').on('click', function () {
                cash('#div-reminder-preview').show();
                cash('#btn-reminder-cancel').hide();
                cash('#btn-reminder').hide();
                cash('#btn-reminder-preview').hide();
            })

            cash('#btn-reminder-preview-cancel').on('click', function () {
                cash('#div-reminder-preview').hide();
                cash('#btn-reminder-cancel').show();
                cash('#btn-reminder').show();
                cash('#btn-reminder-preview').show();
            })

            // reminder_preview
            async function reminder_preview(id) {
                // set link data
                cash('#btn-reminder-preview-ok').data('guest-id', id);

                // Loading state
                cash('#btn-reminder-preview-cancel').hide()
                cash('#btn-reminder-preview-ok').attr('disabled', true);
                cash('#btn-reminder-preview-ok').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post(`invitation-guest/` + id + `/reminder-preview`, {
                    id: id,
                    mobile: cash('#input-reminder-preview').val(),
                    type: cash('#reminder').val(),
                }).then(res => {
                    cash('#btn-reminder-preview-cancel').show();
                    cash('#btn-reminder-preview-ok').removeAttr('disabled');
                    cash('#btn-reminder-preview-ok').html('Ok');
                    if (!res.data.success) {
                        helper.showNotification('error', 'error send preview reminder data', 'harap ulangi');
                    } else {
                        helper.showNotification('success', 'success send preview reminder data', '');
                        table.setData(table.getAjaxUrl());
                    }
                }).catch(err => {
                    cash('#btn-reminder-preview-cancel').show();
                    cash('#btn-reminder-preview-ok').removeAttr('disabled');
                    cash('#btn-reminder-preview-ok').html('Ok');
                    helper.showNotification('error', 'error send preview reminder data');
                })
            }

            cash('#btn-reminder-preview-ok').on('click', function () {
                reminder_preview(cash(this).data('guest-id'))
            })

            // imports
            async function imports() {
                var formData = new FormData();
                var imagefile = cash('#input-imports');
                formData.append("file", imagefile[0].files[0]);

                // Loading state
                cash('#btn-imports-cancel').hide()
                cash('#btn-imports').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post(`invitation-guest-import`, formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }).then(res => {
                    cash('#btn-imports-cancel').show();
                    cash('#btn-imports').html('Ok');
                    cash('#btn-modal-imports').modal('hide');
                    if (!res.data.success) {
                        helper.showNotification('error', 'error import data', 'harap ulangi');
                    } else {
                        helper.showNotification('success', 'success import data', '');

                        // clear table
                        table_modal.clearData();

                        // add to table row
                        for (row of res.data.data) {
                            table_modal.addRow({
                                no: row.no,
                                name: row.name,
                                description: row.description,
                                mobile: row.mobile,
                                invitee_count: row.invitee_count,
                                base: row.base,
                                category: row.category,
                                label: row.label,
                                is_vip: row.is_vip,
                                is_physic_invitation: row.is_physic_invitation,
                                is_auto_rsvp: row.is_auto_rsvp,
                            }, false);
                        }

                        // show table modal
                        cash('#btn-modal-imports-table').modal('show');
                    }
                }).catch(err => {
                    cash('#btn-imports-cancel').show();
                    cash('#btn-imports').html('Ok');
                    cash('#btn-modal-imports').modal('hide');
                    helper.showNotification('error', 'error import data');
                })
            }

            cash('#btn-imports').on('click', function () {
                imports()
            })

            // imports table
            async function imports_table() {
                // form
                var formData = new FormData();

                // set to form data
                cash('#form-imports-table input').each(function () {
                    formData.append(cash(this).attr('name'), cash(this).val());
                });

                // Loading state
                cash('#btn-imports-table-cancel').hide()
                cash('#btn-imports-table').attr('disabled', true);
                cash('#btn-imports-table').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post(`invitation-guest-import-table`, formData).then(res => {
                    cash('#btn-imports-table-cancel').show();
                    cash('#btn-imports-table').removeAttr('disabled');
                    cash('#btn-imports-table').html('Ok');
                    if (!res.data.success) {
                        helper.showNotification('error', 'error import data', 'harap ulangi');
                        for (const [key, val] of Object.entries(res.data.errors)) {
                            var element = key.split('.');
                            var name = element[0];
                            var index = parseInt(element[1]) + 1;
                            var errors = [];
                            for (const v of val) {
                                errors.push(v.replace(`${name}.${element[1]}`, `${element[0]}`));
                            }
                            var cell = cash(table_modal.searchRows("no", "=", index)[0].getCell(name).getElement());
                            var cellvalue = table_modal.searchRows("no", "=", index)[0].getCell(name).getData()[name];
                            var celldiv = `<input name="${name}[]" id="input-imports-${name}-${index}" type="hidden" value="${cellvalue}"/>`;
                            cell.addClass('tabulator-validation-fail');
                            cell.html(celldiv + cellvalue + '<br/>' + errors.join(","));
                        }
                    } else {
                        helper.showNotification('success', 'success import data', '');
                        cash('#btn-modal-imports-table').modal('hide');
                        table.setData(table.getAjaxUrl());
                    }
                }).catch(err => {
                    cash('#btn-imports-table-cancel').show();
                    cash('#btn-imports-table').removeAttr('disabled');
                    cash('#btn-imports-table').html('Ok');
                    helper.showNotification('error', 'error import data');
                })

            }

            cash('#btn-imports-table').on('click', function () {
                imports_table()
            })

            // changestatus
            async function changestatus(id) {
                // set link data
                cash('#btn-changestatus').data('guest-id', id);

                // Loading state
                cash('#btn-changestatus-cancel').hide()
                cash('#btn-wchangestatus').attr('disabled', true);
                cash('#btn-changestatus').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post(`invitation-guest/` + id + `/changestatus`, {
                    id: id,
                    status: cash('#status').val(),
                }).then(res => {
                    cash('#btn-changestatus-cancel').show();
                    cash('#btn-changestatus').removeAttr('disabled');
                    cash('#btn-changestatus').html('Ok');
                    if (!res.data.success) {
                        helper.showNotification('error', 'error change status data', 'harap ulangi');
                    } else {
                        helper.showNotification('success', 'success change status data', '');
                        table.setData(table.getAjaxUrl());
                    }
                    cash('#btn-modal-changestatus').modal('hide');
                }).catch(err => {
                    cash('#btn-changestatus-cancel').show();
                    cash('#btn-changestatus').removeAttr('disabled');
                    cash('#btn-changestatus').html('Ok');
                    helper.showNotification('error', 'error change status data');
                })
            }

            cash('#btn-changestatus').on('click', function () {
                changestatus(cash(this).data('guest-id'))
            })

            cash('#is_whatsapp_qr').on('click', function () {
                if (cash('#is_whatsapp_qr').val() == 'yes') {
                    cash('#is_whatsapp_qr').removeProp('checked');
                    cash('#is_whatsapp_qr').attr('value', 'no');
                    cash('#label-is_whatsapp_qr').html('send qr code (no)');
                } else {
                    cash('#is_whatsapp_qr').prop('checked', true);
                    cash('#is_whatsapp_qr').attr('value', 'yes');
                    cash('#label-is_whatsapp_qr').html('send qr code (yes)');
                }
            })

            cash('#is_whatsapp_maps').on('click', function () {
                if (cash('#is_whatsapp_maps').val() == 'yes') {
                    cash('#is_whatsapp_maps').removeProp('checked');
                    cash('#is_whatsapp_maps').attr('value', 'no');
                    cash('#label-is_whatsapp_maps').html('send maps (no)');
                } else {
                    cash('#is_whatsapp_maps').prop('checked', true);
                    cash('#is_whatsapp_maps').attr('value', 'yes');
                    cash('#label-is_whatsapp_maps').html('send maps (yes)');
                }
            })
        })
    </script>
@endsection
