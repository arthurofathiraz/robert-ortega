@extends('../../layout/' . $layout)

@section('subhead')
    <title>Fathariz Talita - Wedding CMS - Label Invitation</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/tabulator/4.9.3/css/tabulator.min.css" rel="stylesheet">
@endsection

@section('subcontent')
    <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Label</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <a href="{{ route('invitation-label-add-view') }}" class="btn btn-primary shadow-md mr-2">Add New Label</a>
            <div class="dropdown ml-auto sm:ml-0">
                <button class="dropdown-toggle btn px-2 box text-gray-700 dark:text-gray-300" aria-expanded="false">
                    <span class="w-5 h-5 flex items-center justify-center">
                        <i class="w-4 h-4" data-feather="plus"></i>
                    </span>
                </button>
                <div class="dropdown-menu w-40">
                    <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                        <a href="{{ route('invitation-label-add-view') }}"
                           class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">
                            <i data-feather="file-plus" class="w-4 h-4 mr-2"></i> New Label
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN: HTML Table Data -->
    <div class="intro-y box p-5 mt-5">
        <div class="flex flex-col sm:flex-row sm:items-end xl:items-start">
            <form id="tabulator-html-filter-form" class="xl:flex sm:mr-auto">
                <div class="sm:flex items-center sm:mr-4">
                    <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Field</label>
                    <select id="tabulator-html-filter-field"
                            class="form-select w-full sm:w-32 2xl:w-full mt-2 sm:mt-0 sm:w-auto">
                        <option value="name">Name</option>
                        <option value="whatsapp_template">Whatsapp Template</option>
                        <option value="priority">Priority</option>
                    </select>
                </div>
                <div class="sm:flex items-center sm:mr-4 mt-2 xl:mt-0">
                    <label class="w-12 flex-none xl:w-auto xl:flex-initial mr-2">Value</label>
                    <input id="tabulator-html-filter-value" type="text"
                           class="form-control sm:w-40 2xl:w-full mt-2 sm:mt-0" placeholder="Search...">
                </div>
                <div class="mt-2 xl:mt-0">
                    <button id="tabulator-html-filter-go" type="button" class="btn btn-primary w-full sm:w-16">Go
                    </button>
                    <button id="tabulator-html-filter-reset" type="button"
                            class="btn btn-secondary w-full sm:w-16 mt-2 sm:mt-0 sm:ml-1">Reset
                    </button>
                </div>
            </form>
            <div class="flex mt-5 sm:mt-0">
                <button id="tabulator-print" class="btn btn-outline-secondary w-1/2 sm:w-auto mr-2">
                    <i data-feather="printer" class="w-4 h-4 mr-2"></i> Print
                </button>
                <div class="dropdown w-1/2 sm:w-auto">
                    <button class="dropdown-toggle btn btn-outline-secondary w-full sm:w-auto" aria-expanded="false">
                        <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export <i data-feather="chevron-down"
                                                                                        class="w-4 h-4 ml-auto sm:ml-2"></i>
                    </button>
                    <div class="dropdown-menu w-40">
                        <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                            <a id="tabulator-export-csv" href="javascript:;"
                               class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">
                                <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export CSV
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="overflow-x-auto scrollbar-hidden">
            <div id="tabulator-label" class="mt-5 table-report table-report--tabulator"></div>
        </div>
    </div>
    <!-- END: HTML Table Data -->

    <div id="btn-modal-delete" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <a data-dismiss="modal" href="javascript:;">
                    <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
                </a>
                <div class="modal-body p-0">
                    <div class="p-5 text-center">
                        <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Warning</div>
                        <div class="text-gray-600 mt-2">Are you sure want to delete this label?</div>
                    </div>
                    <div class="px-5 pb-8 text-center">
                        <button id="btn-cancel" type="button" data-dismiss="modal" class="btn btn-secondary w-24">
                            Cancel
                        </button>
                        <button id="btn-remove" type="button" class="btn btn-primary w-24">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/tabulator/4.9.3/js/tabulator.min.js"></script>
    <script>
        let table;
        (function (cash) {
            "use strict";

            if (cash("#tabulator-label").length) {
                table = new Tabulator("#tabulator-label", {
                    ajaxURL: "{{ route('invitation-label-get') }}",
                    ajaxFiltering: true,
                    ajaxSorting: true,
                    printAsHtml: true,
                    printStyled: true,
                    pagination: "remote",
                    paginationSize: 10,
                    paginationSizeSelector: [10, 20, 30, 40, 50],
                    layout: "fitDataStretch",
                    responsiveLayout: "collapse",
                    placeholder: "No matching records found",
                    columns: [
                        // For HTML table
                        {
                            title: "NAME",
                            minWidth: 10,
                            responsive: 0,
                            field: "name",
                            vertAlign: "middle",
                            print: false,
                            download: false,
                            formatter(cell, formatterParams) {
                                return `<div>
                                    <div class="font-medium whitespace-nowrap">${
                                    cell.getData().name
                                }</div>
                                    <div class="text-gray-600 text-xs whitespace-nowrap">${
                                    cell.getData().description
                                }</div>
                                </div>`;
                            },
                        },
                        {
                            title: "PRIORITY",
                            minWidth: 100,
                            field: "priority",
                            hozAlign: "center",
                            vertAlign: "middle",
                            print: false,
                            download: false,
                            formatter(cell, formatterParams) {
                                var priority = cell.getData().priority.toString();
                                var lastDigit = priority.charAt(priority.length - 1);
                                let divPriority = ``;
                                switch (lastDigit) {
                                    case "0":
                                        divPriority = `<span class="px-2 py-1 rounded-full bg-gray-200 text-gray-600 mr-1">${cell.getData().priority}</span>`;
                                        break;
                                    case "1":
                                    case "6" :
                                        divPriority = `<span class="px-2 py-1 rounded-full bg-theme-1 text-white mr-1">${cell.getData().priority}</span>`;
                                        break;
                                    case "2":
                                    case "7" :
                                        divPriority = `<span
                                    class="px-2 py-1 rounded-full border text-gray-700 dark:text-gray-600 dark:border-dark-5 mr-1">${cell.getData().priority}</span>`;
                                        break;
                                    case "3":
                                    case "8":
                                        divPriority = `<span class="px-2 py-1 rounded-full bg-theme-9 text-white mr-1">${cell.getData().priority}</span>`;
                                        break;
                                    case "4":
                                    case "9" :
                                        divPriority = `<span class="px-2 py-1 rounded-full bg-theme-12 text-white mr-1">${cell.getData().priority}</span>`;
                                        break;
                                    case "5" :
                                        divPriority = `<span class="px-2 py-1 rounded-full bg-theme-6 text-white mr-1">${cell.getData().priority}</span>`;
                                        break;
                                }

                                return divPriority;
                            },
                        },
                        {
                            title: "ACTIONS",
                            minWidth: 100,
                            field: "actions",
                            responsive: 1,
                            hozAlign: "center",
                            vertAlign: "middle",
                            headerSort: false,
                            print: false,
                            download: false,
                            formatter(cell, formatterParams) {
                                let a = cash(`<div class="flex lg:justify-center items-center">
                                    <a class="edit flex items-center mr-1 btn btn-secondary btn-sm" href="javascript:;">
                                        <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit
                                    </a>
                                </div>`);
                                cash(a)
                                    .find(".edit")
                                    .on("click", function () {
                                        window.location.href = "invitation-label/" + cell.getData().id;
                                    });

                                cash(a)
                                    .find(".delete")
                                    .on("click", function () {
                                        cash('#btn-modal-delete').modal('show');
                                        cash('#btn-remove').data('label-id', cell.getData().id);
                                    });

                                return a[0];
                            },
                        },

                        // For print format
                        {
                            title: "NAME",
                            field: "name",
                            visible: false,
                            print: true,
                            download: true,
                        },
                        {
                            title: "WHATSAPP TEMPLATE",
                            field: "whatsapp_template",
                            visible: false,
                            print: true,
                            download: true,
                        },
                        {
                            title: "PRIORITY",
                            field: "priority",
                            visible: false,
                            print: true,
                            download: true,
                        },
                    ],
                    renderComplete() {
                        feather.replace({
                            "stroke-width": 1.5,
                        });
                    },
                });

                // Redraw table onresize
                window.addEventListener("resize", () => {
                    table.redraw();
                    feather.replace({
                        "stroke-width": 1.5,
                    });
                });

                // Filter function
                function filterHTMLForm() {
                    let field = cash("#tabulator-html-filter-field").val();
                    let type = 'like';
                    let value = cash("#tabulator-html-filter-value").val();
                    table.setFilter(field, type, value);
                }

                // On submit filter form
                cash("#tabulator-html-filter-form")[0].addEventListener(
                    "keypress",
                    function (event) {
                        let keycode = event.keyCode ? event.keyCode : event.which;
                        if (keycode == "13") {
                            event.preventDefault();
                            filterHTMLForm();
                        }
                    }
                );

                // On click go button
                cash("#tabulator-html-filter-go").on("click", function (event) {
                    filterHTMLForm();
                });

                // On reset filter form
                cash("#tabulator-html-filter-reset").on("click", function (event) {
                    cash("#tabulator-html-filter-field").val("name");
                    cash("#tabulator-html-filter-value").val("");
                    filterHTMLForm();
                });

                // Export
                cash("#tabulator-export-csv").on("click", function (event) {
                    table.download("csv", "data.csv");
                });

                // Print
                cash("#tabulator-print").on("click", function (event) {
                    table.print();
                });
            }
        })(cash);

        cash(function () {
            async function remove(id) {

                // set modal
                cash('#btn-modal-delete').modal('show');

                // set link data
                cash('#btn-remove').data('label-id', id);

                // Loading state
                cash('#btn-cancel').hide()
                cash('#btn-remove').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post(`invitation-label/` + id + `/remove`, {
                    id: id,
                }).then(res => {
                    cash('#btn-cancel').show();
                    cash('#btn-remove').html('Ok');
                    cash('#btn-modal-delete').modal('hide');
                    if (!res.data.success) {
                        helper.showNotification('error', 'error remove data', 'harap ulangi');
                    } else {
                        helper.showNotification('success', 'success remove data', '');
                        table.setData(table.getAjaxUrl());

                    }
                }).catch(err => {
                    cash('#btn-cancel').show();
                    cash('#btn-remove').html('Ok');
                    cash('#btn-modal-delete').modal('hide');
                    helper.showNotification('error', 'error remove data');
                })
            }

            cash('#btn-remove').on('click', function () {
                remove(cash(this).data('label-id'))
            })
        })
    </script>
@endsection
