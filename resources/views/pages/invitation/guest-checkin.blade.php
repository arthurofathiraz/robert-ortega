@extends('../../layout/' . $layout)

@section('subhead')
    <title>Fathariz Talita - Wedding CMS - Guest Check In</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/tabulator/4.9.3/css/tabulator.min.css" rel="stylesheet">
    {{--    <link href="https://cdnjs.cloudflare.com/ajax/libs/tom-select/1.7.8/css/tom-select.bootstrap5.min.css"--}}
    {{--          rel="stylesheet">--}}
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Guest Check In</h2>
    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 lg:col-span-6">
            <!-- BEGIN: Form Validation -->
            <div class="intro-y box">
                <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Check In</h2>
                </div>
                <div class="p-5">
                    <form id="inputan-form">
                        <div>
                            <label for="guest" class="form-label">Select</label>
                            <div id="div-guest">
                                <select id="select-guest" class="tom-select w-full" data-placeholder="Select guest"
                                        data-dont-initialize="true">
                                    <option value="">-- Select guest --</option>
                                    @foreach($invitation as $invit)
                                        <optgroup label="MALE ({{ $configuration->male_name }})">
                                            @if($invit->category == 'MALE')
                                                <option
                                                    value="{{ $invit->id }}"
                                                    data-id="{{ $invit->id }}"
                                                    data-name="{{ $invit->name }}"
                                                    data-description="{{ $invit->description }}"
                                                    data-base="{{ $invit->base }}"
                                                    data-invitee_count="{{ $invit->invitee_count }}"
                                                    data-is_vip="{{ $invit->is_vip ? 'yes' : 'no' }}"
                                                >
                                                    {{ $invit->name ." - " . $invit->base }}
                                                </option>
                                            @endif
                                        </optgroup>
                                        <optgroup label="FEMALE ({{ $configuration->female_name }})">
                                            @if($invit->category == 'FEMALE')
                                                <option
                                                    value="{{ $invit->id }}"
                                                    data-id="{{ $invit->id }}"
                                                    data-name="{{ $invit->name }}"
                                                    data-description="{{ $invit->description }}"
                                                    data-base="{{ $invit->base }}"
                                                    data-invitee_count="{{ $invit->invitee_count }}"
                                                    data-is_vip="{{ $invit->is_vip ? 'yes' : 'no' }}"
                                                >
                                                    {{ $invit->name ." - " . $invit->base }}
                                                </option>
                                            @endif
                                        </optgroup>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="mt-5 mb-5 text-center text-big">
                            <h1>
                                <strong>Or</strong>
                            </h1>
                        </div>

                        <div class="mt-3" id="div-qrcode">
                            <label for="reader" class="form-label">Scan</label>
                            <div class="grid grid-cols-1 place-items-center">
                                <a href="javascript:;">
                                    <img id="img-scanner" class="h-32"
                                         src="https://raw.githubusercontent.com/mebjas/html5-qrcode/master/assets/camera-scan.gif"
                                         style="opacity: 0.3"/>
                                </a>
                                <a href="javascript:;" id="btn-scanner" class="btn btn-primary btn-sm mt-2">Click to
                                    scan</a>
                                <div id="div-scanner" class="text-center items-center w-full" style="display: none">
                                    <div id="camera-scanner" style="width: 100%" class="mt-2"></div>
                                    <select id="select-scanner" class="form-control form-select mt-2"
                                            data-placeholder="Select camera">
                                    </select>
                                    <a href="javascript:;" id="btn-scanner-cancel" class="btn btn-danger btn-sm mt-2">
                                        Stop scanning
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END: Form Validation -->
        </div>
    </div>

    <div id="modal-select-guest" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <a data-dismiss="modal" href="javascript:;">
                    <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
                </a>
                <div class="modal-body p-0">
                    <div class="p-5 text-center">
                        <i data-feather="info" class="w-16 h-16 text-theme-3 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Check In</div>
                        <div class="text-gray-600 mt-2 mb-5">
                            Data guest check in without QR Code.
                        </div>
                        <form id="form-select-guest" class="mt-2">
                            <input id="input-select-id" type="hidden" class="form-control form__inputan" value=""/>
                            <table class="table table-borderless text-lg">
                                <tbody>
                                <tr>
                                    <td class="border-b dark:border-dark-5 text-right">Name</td>
                                    <td class="border-b dark:border-dark-5 text-center">:</td>
                                    <td class="border-b dark:border-dark-5 text-left">
                                        <strong id="label-select-name"></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border-b dark:border-dark-5 text-right">Description</td>
                                    <td class="border-b dark:border-dark-5 text-center">:</td>
                                    <td class="border-b dark:border-dark-5 text-left">
                                        <strong id="label-select-description"></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border-b dark:border-dark-5 text-right">Base</td>
                                    <td class="border-b dark:border-dark-5 text-center">:</td>
                                    <td class="border-b dark:border-dark-5 text-left">
                                        <strong id="label-select-base"></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border-b dark:border-dark-5 text-right">VIP</td>
                                    <td class="border-b dark:border-dark-5 text-center">:</td>
                                    <td class="border-b dark:border-dark-5 text-left">
                                        <strong id="label-select-vip"></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border-b dark:border-dark-5 text-right">Jumlah Invitee</td>
                                    <td class="border-b dark:border-dark-5 text-center">:</td>
                                    <td class="border-b dark:border-dark-5 text-left">
                                        <strong id="label-select-invitee_count"></strong>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <div class="px-5 pb-8 text-center mt-5">
                        <button id="btn-select-cancel" type="button" data-dismiss="modal"
                                class="btn btn-secondary w-24">
                            Cancel
                        </button>
                        <button id="btn-select-ok" type="button" class="btn btn-primary w-24">Check In</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-qrcode-guest" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <a data-dismiss="modal" href="javascript:;">
                    <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
                </a>
                <div class="modal-body p-0">
                    <div class="p-5 text-center">
                        <i data-feather="info" class="w-16 h-16 text-theme-3 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Check In</div>
                        <div class="text-gray-600 mt-2 mb-5">
                            Data guest check in with QR Code.
                        </div>
                        <form id="form-qrcode-guest" class="mt-2">
                            <input id="input-qrcode-id" type="hidden" class="form-control form__inputan" value=""/>
                            <input id="input-qrcode-mobile" type="hidden" class="form-control form__inputan" value=""/>
                            <input id="input-qrcode-qr_code_token" type="hidden" class="form-control form__inputan"
                                   value=""/>
                            <table class="table table-borderless text-lg">
                                <tbody>
                                <tr>
                                    <td class="border-b dark:border-dark-5 text-right">Name</td>
                                    <td class="border-b dark:border-dark-5 text-center">:</td>
                                    <td class="border-b dark:border-dark-5 text-left">
                                        <strong id="label-qrcode-name"></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border-b dark:border-dark-5 text-right">Description</td>
                                    <td class="border-b dark:border-dark-5 text-center">:</td>
                                    <td class="border-b dark:border-dark-5 text-left">
                                        <strong id="label-qrcode-description"></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border-b dark:border-dark-5 text-right">Base</td>
                                    <td class="border-b dark:border-dark-5 text-center">:</td>
                                    <td class="border-b dark:border-dark-5 text-left">
                                        <strong id="label-qrcode-base"></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border-b dark:border-dark-5 text-right">VIP</td>
                                    <td class="border-b dark:border-dark-5 text-center">:</td>
                                    <td class="border-b dark:border-dark-5 text-left">
                                        <strong id="label-qrcode-vip"></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border-b dark:border-dark-5 text-right">Jumlah Invitee</td>
                                    <td class="border-b dark:border-dark-5 text-center">:</td>
                                    <td class="border-b dark:border-dark-5 text-left">
                                        <strong id="label-qrcode-invitee_count"></strong>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <div class="px-5 pb-8 text-center mt-5">
                        <button id="btn-qrcode-cancel" type="button" data-dismiss="modal"
                                class="btn btn-secondary w-24">
                            Cancel
                        </button>
                        <button id="btn-qrcode-ok" type="button" class="btn btn-primary w-24">Check In</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5-qrcode/2.0.3/html5-qrcode.min.js"></script>
    <script>
        var tom_select_guest = helper.newTomSelect('#select-guest');

        cash(async function () {
                // get responsive width
                var width_qrcode = cash('#div-qrcode').map(function () {
                    return cash(this).width();
                }).get();

                // simply minus 20 of width
                width_qrcode -= Math.round(width_qrcode / 2);

                // set config
                var config = {
                    fps: 10,
                    qrbox: width_qrcode,
                    aspectRatio: 1.777778,
                };

                var html5QrCode = null;

                async function scan() {

                    cash('#btn-scanner').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                    cash('#btn-scanner').attr('disabled', true);
                    cash('#img-scanner').hide();
                    await helper.delay(1000)

                    // This method will trigger user permissions
                    Html5Qrcode.getCameras().then(devices => {

                        cash('#img-scanner').hide();
                        cash('#btn-scanner').hide();
                        cash('#div-scanner').show();

                        /**
                         * devices would be an array of objects of type:
                         * { id: "id", label: "label" }
                         */
                        if (devices && devices.length) {

                            // clear option
                            cash('#select-scanner').html('');
                            cash('#select-scanner').append(`<option value="">-- Select camera --</option>`);

                            // loop each devices
                            for (device of devices) {

                                // add option
                                cash('#select-scanner').append(`
                                <option value="${device.id}">${device.label}</option>
                            `);
                            }

                            // start camera scanner
                            if (cash('#select-scanner').val() != '' && html5QrCode == null) {

                                // set camera
                                camera(cash('#select-scanner').val())

                            }
                        }

                        cash('#btn-scanner').html('Click to scan')
                        cash('#btn-scanner').removeAttr('disabled');
                    }).catch(err => {
                        helper.showNotification('error', 'error get camera permissions', 'harap ijinkan kamera di akses untuk scan');
                        cash('#btn-scanner').html('Click to scan')
                        cash('#btn-scanner').removeAttr('disabled');
                    });
                }

                async function stop() {
                    if (html5QrCode != null) {
                        html5QrCode.stop();
                        html5QrCode.clear();
                    }

                    cash('#img-scanner').show();
                    cash('#btn-scanner').show();
                    cash('#div-scanner').hide();
                }

                async function camera(camera_id) {
                    html5QrCode = new Html5Qrcode("camera-scanner", false);
                    html5QrCode.start(
                        {deviceId: {exact: camera_id}},
                        config,
                        (decodedText, decodedResult) => {

                            html5QrCode.stop();
                            html5QrCode.clear();

                            var invitation_data = decodedText.split('<|separator|>');
                            if (invitation_data.length < 2) {
                                helper.showNotification('error', 'error scan data', 'harap gunakan qr code yang valid');
                                helper.delay(1000);
                                camera(camera_id);
                            }

                            axios.get(`invitation-checkin-qrcode/` + invitation_data[1]).then(res => {
                                    if (!res.data.success) {
                                        helper.showNotification('error', 'error get check in data', 'harap ulangi');
                                        helper.delay(1000);
                                        camera(camera_id);
                                    } else {
                                        helper.showNotification('success', 'success get check in data', '');

                                        // option
                                        var option = res.data.data;

                                        // change label
                                        cash('#input-qrcode-id').attr('value', option.id);
                                        cash('#input-qrcode-mobile').attr('value', invitation_data[1]);
                                        cash('#input-qrcode-qr_code_token').attr('value', invitation_data[2]);
                                        cash('#label-qrcode-name').html(option.name);
                                        cash('#label-qrcode-description').html(option.description);
                                        cash('#label-qrcode-base').html(option.base);
                                        cash('#label-qrcode-vip').html(option.is_vip == 'yes' ? 'Tamu VIP' : 'Bukan Tamu VIP');
                                        cash('#label-qrcode-invitee_count').html(option.invitee_count);

                                        // show modal
                                        cash('#modal-qrcode-guest').modal('show');
                                    }
                                },
                                (errorMessage) => {
                                    // // parse error, ignore it.
                                    // console.log('errorMessage : ', errorMessage);
                                })
                                .catch((err) => {
                                    helper.showNotification('error', 'error start scanner', 'harap coba lagi, atau ganti kamera');
                                });
                        },
                    );
                }

                async function check_in() {
                    // Reset state
                    cash('#inputan-form').find('.form__inputan').removeClass('border-theme-6')
                    cash('#inputan-form').find('.form__inputan-error').html('')

                    // Post form
                    let id = cash('#input-select-id').val()

                    // Loading state
                    cash('#btn-select-ok').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                    cash('#btn-select-ok').attr('disabled', true);
                    cash('#btn-select-cancel').attr('disabled', true);
                    await helper.delay(1000)

                    axios.post('{{ route('invitation-checkin') }}', {
                        id: id,
                    }).then(res => {
                        cash('#btn-select-ok').html('Check In');
                        cash('#btn-select-ok').removeAttr('disabled');
                        cash('#btn-select-cancel').removeAttr('disabled');
                        if (!res.data.success) {
                            helper.showNotification('error', 'error check in data', res.data.message);
                            cash('#modal-select-guest').modal('hide');
                        } else {
                            helper.showNotification('success', 'success check in data', 'terimakasih atas kedatangan anda');
                            cash('#modal-select-guest').modal('hide');
                            camera(cash('#select-scanner').val());
                        }
                    }).catch(err => {
                        cash('#btn-select-ok').html('Check In');
                        cash('#btn-select-ok').removeAttr('disabled');
                        cash('#btn-select-cancel').removeAttr('disabled');
                        helper.showNotification('error', 'error check in data');
                        cash('#modal-select-guest').modal('hide');
                    })
                }

                async function check_in_qrcode() {
                    // Reset state
                    cash('#inputan-form').find('.form__inputan').removeClass('border-theme-6')
                    cash('#inputan-form').find('.form__inputan-error').html('')

                    // Post form
                    let id = cash('#input-qrcode-id').val()
                    let mobile = cash('#input-qrcode-mobile').val()
                    let qr_code_token = cash('#input-qrcode-qr_code_token').val()

                    // Loading state
                    cash('#btn-qrcode-ok').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                    cash('#btn-qrcode-ok').attr('disabled', true);
                    cash('#btn-qrcode-cancel').attr('disabled', true);
                    await helper.delay(1000)

                    axios.post('{{ route('invitation-checkin-qrcode') }}', {
                        id: id,
                        mobile: mobile,
                        qr_code_token: qr_code_token,
                    }).then(res => {
                        cash('#btn-qrcode-ok').html('Check In');
                        cash('#btn-qrcode-ok').removeAttr('disabled');
                        cash('#btn-qrcode-cancel').removeAttr('disabled');
                        if (!res.data.success) {
                            helper.showNotification('error', 'error check in data', res.data.message);
                            cash('#modal-qrcode-guest').modal('hide');
                            camera(cash('#select-scanner').val());
                        } else {
                            helper.showNotification('success', 'success check in data', 'terimakasih atas kedatangan anda');
                            cash('#modal-qrcode-guest').modal('hide');
                            camera(cash('#select-scanner').val());
                        }
                    }).catch(err => {
                        cash('#btn-qrcode-ok').html('Check In');
                        cash('#btn-qrcode-ok').removeAttr('disabled');
                        cash('#btn-qrcode-cancel').removeAttr('disabled');
                        helper.showNotification('error', 'error check in data', '');
                        cash('#modal-qrcode-guest').modal('hide');
                        camera(cash('#select-scanner').val());
                    })
                }

                cash('form input').on('keydown', function (e) {
                    if (e.keyCode === 13) {
                        e.preventDefault();
                        return false;
                    }
                })

                cash('#btn-select-ok').on('click', function () {
                    check_in()
                })

                cash('#btn-qrcode-ok').on('click', function () {
                    check_in_qrcode()
                })

                cash('#btn-scanner').on('click', function () {
                    scan()
                })

                cash('#img-scanner').on('click', function () {
                    scan()
                })

                cash('#btn-scanner-cancel').on('click', function () {
                    stop()
                })

                cash('#select-scanner').on('change', function () {
                    if (html5QrCode != null) {
                        html5QrCode.stop();
                        html5QrCode.clear();
                    }

                    // start camera
                    if (cash(this).val()) {
                        camera(cash(this).val())
                    }
                })

                cash('#select-guest').on('change', function () {
                    var value = cash(this).val();

                    if (value) {
                        var option = cash(this).find(`option[value="${value}"]`);

                        // change label
                        cash('#input-select-id').attr('value', option.data('id'));
                        cash('#label-select-name').html(option.data('name'));
                        cash('#label-select-description').html(option.data('description'));
                        cash('#label-select-base').html(option.data('base'));
                        cash('#label-select-vip').html(option.data('is_vip') == 'yes' ? 'Tamu VIP' : 'Bukan Tamu VIP');
                        cash('#label-select-invitee_count').html(option.data('invitee_count'));

                        // show modal
                        cash('#modal-select-guest').modal('show');
                        tom_select_guest.setValue('');
                    }
                })

                cash('#btn-select-cancel').on('click', function () {
                    tom_select_guest.setValue('');
                })

                cash('#btn-qrcode-cancel').on('click', function () {
                    cash('#select-scanner').val('');
                })
            }
        )
    </script>
@endsection
