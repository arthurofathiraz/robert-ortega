@extends('../../layout/' . $layout)

@section('subhead')
    <title>Fathariz Talita - Wedding CMS - Label Invitation Add</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/tabulator/4.9.3/css/tabulator.min.css" rel="stylesheet">
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Label Invitation Add</h2>
    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 lg:col-span-6">
            <!-- BEGIN: Form Validation -->
            <div class="intro-y box">
                <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Add</h2>
                </div>
                <div class="p-5">
                    <form id="inputan-form">
                        <div>
                            <label for="name" class="form-label">Name</label>
                            <input id="name" type="text" class="form-control form__inputan"
                                   placeholder="Input label name"
                                   value="">
                            <div id="error-name"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="description" class="form-label">Description</label>
                            <input id="description" type="text" class="form-control form__inputan"
                                   placeholder="Input label description"
                                   value="">
                            <div id="error-description"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="whatsapp_template" class="form-label">Whatsapp Template
                                <ul class="ml-3 list-disc">
                                    <li>
                                        <a class="text-small text-theme-3" href="javascript:;">
                                            <strong>{invitation_name}</strong>
                                            <span class="text-black"> - name of invitation guest (ex: fathariz)</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="text-small text-theme-3" href="javascript:;">
                                            <strong>{male_name}</strong>
                                            <span class="text-black">- name of CPP (ex: {{ $configuration->male_name }})</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="text-small text-theme-3" href="javascript:;">
                                            <strong>{male_description}</strong>
                                            <span class="text-black">- desc of CPP</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="text-small text-theme-3" href="javascript:;">
                                            <strong>{female_name}</strong>
                                            <span class="text-black">- name of CPW (ex: {{ $configuration->female_name }})</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="text-small text-theme-3" href="javascript:;">
                                            <strong>{female_description}</strong>
                                            <span class="text-black">- desc of CPW</span>
                                        </a>
                                    </li>
                                </ul></label>

                            <textarea id="whatsapp_template" type="text" rows="5" class="form-control  form__inputan"
                                      placeholder="Input label whatsapp template"></textarea>
                            <div id="error-whatsapp_template"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="priority" class="form-label">Priority</label>
                            <input id="priority" type="number" class="form-control form__inputan"
                                   placeholder="Input label priority"
                                   value="">
                            <div id="error-priority"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                    </form>
                    <button id="btn-back" class="btn btn-secondary mt-4">Back</button>
                    <button id="btn-update" class="btn btn-primary mt-4">Update</button>
                </div>
            </div>
            <!-- END: Form Validation -->
        </div>
    </div>
@endsection

@section('script')
    <script>
        cash(function () {
            async function update() {
                // Reset state
                cash('#inputan-form').find('.form__inputan').removeClass('border-theme-6')
                cash('#inputan-form').find('.form__inputan-error').html('')

                // Post form
                let name = cash('#name').val()
                let description = cash('#description').val()
                let priority = cash('#priority').val()
                let whatsapp_template = cash('#whatsapp_template').val()

                // Loading state
                cash('#btn-update').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                cash('#btn-back').attr('disabled', true);
                await helper.delay(1000)

                axios.post('{{ route('invitation-label-add') }}', {
                    name: name,
                    description: description,
                    whatsapp_template: whatsapp_template,
                    priority: priority,
                }).then(res => {
                    cash('#btn-update').html('Update');
                    cash('#btn-back').removeAttr('disabled');
                    if (!res.data.success) {
                        for (const [key, val] of Object.entries(res.data.errors)) {
                            cash(`#${key}`).addClass('border-theme-6')
                            cash(`#error-${key}`).html(val)
                        }
                        helper.showNotification('error', 'error update data', 'harap cek kembali inputan anda');
                    } else {
                        helper.showNotification('success', 'success update data', 'halaman akan di refresh');
                        setTimeout(function () {
                            window.location.reload(true);
                            window.location.reload(true);
                        }, 500);

                    }
                }).catch(err => {
                    cash('#btn-update').html('Update');
                    cash('#btn-back').removeAttr('disabled');
                    helper.showNotification('error', 'error update data');
                })
            }

            cash('form input').on('keydown', function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            })

            cash('#btn-update').on('click', function () {
                update()
            })

            cash('#btn-back').on('click', function () {
                window.location.href = "{{ route('invitation-label-view') }}";
            })
        })
    </script>
@endsection
