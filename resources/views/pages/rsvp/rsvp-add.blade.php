@extends('../../layout/' . $layout)

@section('subhead')
    <title>Fathariz Talita - Wedding CMS - Rsvp Invitation Add</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/tabulator/4.9.3/css/tabulator.min.css" rel="stylesheet">
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Rsvp Invitation Add</h2>
    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 lg:col-span-6">
            <!-- BEGIN: Form Validation -->
            <div class="intro-y box">
                <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Add</h2>
                </div>
                <div class="p-5">
                    <form id="inputan-form">
                        <div>
                            <label for="nama" class="form-label">Nama</label>
                            <input id="nama" type="text" class="form-control form__inputan"
                                   placeholder="Input rsvp nama"
                                   value="">
                            <div id="error-nama"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="alamat" class="form-label">Alamat</label>
                            <input id="alamat" type="text" class="form-control form__inputan"
                                   placeholder="Input rsvp alamat"
                                   value="">
                            <div id="error-alamat"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="hadir" class="form-label">Hadir?</label>
                            <select id="hadir" data-placeholder="Select label" class="tom-select w-full form__inputan">
                                <option value="" disabled>Apakah anda akan hadir?</option>
                                <option value="ya">Ya</option>
                                <option value="tidak">Tidak</option>
                            </select>
                            <div id="error-hadir"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div>
                            <label for="jumlah" class="form-label">Jumlah</label>
                            <input id="jumlah" type="number" class="form-control form__inputan"
                                   placeholder="Input rsvp jumlah"
                                   value="">
                            <div id="error-jumlah"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                    </form>
                    <button id="btn-back" class="btn btn-secondary mt-4">Back</button>
                    <button id="btn-update" class="btn btn-primary mt-4">Update</button>
                </div>
            </div>
            <!-- END: Form Validation -->
        </div>
    </div>
@endsection

@section('script')
    <script>
        cash(function () {
            async function update() {
                // Reset state
                cash('#inputan-form').find('.form__inputan').removeClass('border-theme-6')
                cash('#inputan-form').find('.form__inputan-error').html('')

                // Post form
                let nama = cash('#nama').val()
                let alamat = cash('#alamat').val()
                let hadir = cash('#hadir').val()
                let jumlah = cash('#jumlah').val()

                // Loading state
                cash('#btn-update').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                cash('#btn-back').attr('disabled', true);
                await helper.delay(1000)

                axios.post('{{ route('invitation-rsvp-add') }}', {
                    nama: nama,
                    alamat: alamat,
                    hadir: hadir,
                    jumlah: jumlah,
                }).then(res => {
                    cash('#btn-update').html('Update');
                    cash('#btn-back').removeAttr('disabled');
                    if (!res.data.success) {
                        for (const [key, val] of Object.entries(res.data.errors)) {
                            cash(`#${key}`).addClass('border-theme-6')
                            cash(`#error-${key}`).html(val)
                        }
                        helper.showNotification('error', 'error update data', 'harap cek kembali inputan anda');
                    } else {
                        helper.showNotification('success', 'success update data', 'halaman akan di refresh');
                        setTimeout(function () {
                            window.location.reload(true);
                            window.location.reload(true);
                        }, 500);

                    }
                }).catch(err => {
                    cash('#btn-update').html('Update');
                    cash('#btn-back').removeAttr('disabled');
                    helper.showNotification('error', 'error update data');
                })
            }

            cash('form input').on('keydown', function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            })

            cash('#btn-update').on('click', function () {
                update()
            })

            cash('#btn-back').on('click', function () {
                window.location.href = "{{ route('invitation-rsvp-view') }}";
            })
        })
    </script>
@endsection
