@extends('../../layout/' . $layout)

@section('subhead')
    <title>Fathariz Talita - Wedding CMS - User Invitation Edit</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/tabulator/4.9.3/css/tabulator.min.css" rel="stylesheet">
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">User Invitation Edit</h2>
    </div>
    <div class="grid grid-cols-12 gap-6 mt-5">
        <div class="intro-y col-span-12 lg:col-span-6">
            <!-- BEGIN: Form Validation -->
            <div class="intro-y box">
                <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Edit</h2>
                </div>
                <div class="p-5">
                    <form id="inputan-form">
                        <div>
                            <label for="name" class="form-label">Name</label>
                            <input id="name" type="text" class="form-control form__inputan"
                                   placeholder="Input user name"
                                   value="{{ $user->name }}" {{ ($user->name == 'streaming') ? 'disabled' : '' }}>
                            <div id="error-name"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="email" class="form-label">Email</label>
                            <input id="email" type="text" class="form-control form__inputan"
                                   placeholder="Input user email"
                                   value="{{ $user->email }}">
                            <div id="error-email"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="username" class="form-label">Username</label>
                            <input id="username" type="text" class="form-control form__inputan"
                                   placeholder="Input user username"
                                   value="{{ $user->username }}">
                            <div id="error-username"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="role" class="form-label">Role</label>
                            <select id="role" class="tom-select w-full form__inputan"
                                    data-placeholder="Select role">
                                <option value="ADMIN" {{ ($user->role == 'ADMIN' ? 'selected' : '') }}>ADMIN</option>
                                <option value="USER" {{ ($user->role == 'USER' ? 'selected' : '') }}>USER</option>
                                <option value="WO {{ ($user->role == 'WO' ? 'selected' : '') }}">WO</option>
                            </select>
                            <div id="error-category"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="password" class="form-label">Password</label>
                            <input id="password" type="password" class="form-control form__inputan"
                                   placeholder="Input user password" value="">
                            <div id="error-password"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                    </form>
                    <button id="btn-back" class="btn btn-secondary mt-4">Back</button>
                    <button id="btn-update" class="btn btn-primary mt-4">Update</button>
                </div>
            </div>
            <!-- END: Form Validation -->
        </div>
    </div>
@endsection

@section('script')
    <script>
        cash(function () {
            async function update() {
                // Reset state
                cash('#inputan-form').find('.form__inputan').removeClass('border-theme-6')
                cash('#inputan-form').find('.form__inputan-error').html('')

                // Post form
                let name = cash('#name').val()
                let email = cash('#email').val()
                let username = cash('#username').val()
                let password = cash('#password').val()
                let role = cash('#role').val()

                // Loading state
                cash('#btn-update').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                cash('#btn-back').attr('disabled', true);
                await helper.delay(1000)

                axios.post('{{ route('user-edit', $user->id) }}', {
                    name: name,
                    email: email,
                    username: username,
                    password: password,
                    role: role,
                }).then(res => {
                    cash('#btn-update').html('Update');
                    cash('#btn-back').removeAttr('disabled');
                    if (!res.data.success) {
                        for (const [key, val] of Object.entries(res.data.errors)) {
                            cash(`#${key}`).addClass('border-theme-6')
                            cash(`#error-${key}`).html(val)
                        }
                        helper.showNotification('error', 'error update data', 'harap cek kembali inputan anda');
                    } else {
                        helper.showNotification('success', 'success update data', 'halaman akan di refresh');
                        setTimeout(function () {
                            window.location.reload(true);
                            window.location.reload(true);
                        }, 500);

                    }
                }).catch(err => {
                    cash('#btn-update').html('Update');
                    cash('#btn-back').removeAttr('disabled');
                    helper.showNotification('error', 'error update data');
                })
            }

            cash('form input').on('keydown', function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            })

            cash('#btn-update').on('click', function () {
                update()
            })

            cash('#btn-back').on('click', function () {
                window.location.href = "{{ route('user-view') }}";
            })
        })
    </script>
@endsection
