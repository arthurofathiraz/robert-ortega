@extends('../../layout/' . $layout)

@section('subhead')
    <title>Fathariz Talita - Wedding CMS - Gallery Configuration</title>
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Gallery Configuration</h2>
    </div>
    <div class="grid grid-cols-12 gap-6">
        <!-- BEGIN: Profile Menu -->
        <div class="col-span-12 lg:col-span-4 2xl:col-span-3 flex lg:block flex-col-reverse">
            <div class="intro-y box mt-5">
                <div class="relative flex items-center p-5">
                    <div class="w-12 h-12 image-fit">
                        <img alt="TalitaFathariz Tailwind HTML Admin Template" class="rounded-full"
                             src="{{ asset('dist/images/' . $fakers[0]['photos'][0]) }}">
                    </div>
                    <div class="ml-4 mr-auto">
                        <div class="font-medium text-base">{{ auth()->user()->name }}</div>
                        <div class="text-gray-600">{{ auth()->user()->role }}</div>
                    </div>
                </div>
                <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                    <a class="flex items-center"
                       href="{{ route('configuration-personal-view') . App\Helper::url_query_string() }}">
                        <i data-feather="user" class="w-4 h-4 mr-2"></i> Personal Configuration
                    </a>
                </div>
                @if(auth()->user()->role == "ADMIN")
                    <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                        <a class="flex items-center" href="{{ route('configuration-couple-view') . App\Helper::url_query_string() }}">
                            <i data-feather="users" class="w-4 h-4 mr-2"></i> Couple Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-akad-view') . App\Helper::url_query_string() }}">
                            <i data-feather="map" class="w-4 h-4 mr-2"></i> Akad Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-walimah-view') . App\Helper::url_query_string() }}">
                            <i data-feather="map-pin" class="w-4 h-4 mr-2"></i> Walimah Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-guide-view') . App\Helper::url_query_string() }}">
                            <i data-feather="file" class="w-4 h-4 mr-2"></i> File Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-bank-view') . App\Helper::url_query_string() }}">
                            <i data-feather="dollar-sign" class="w-4 h-4 mr-2"></i> Bank Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-live-view') . App\Helper::url_query_string() }}">
                            <i data-feather="phone" class="w-4 h-4 mr-2"></i> Live Configuration
                        </a>
                        <a class="flex items-center text-theme-1 dark:text-theme-10 font-medium mt-5" href="{{ route('configuration-gallery-view') . App\Helper::url_query_string() }}">
                            <i data-feather="film" class="w-4 h-4 mr-2"></i> Gallery Configuration
                        </a>
                    </div>
                @endif
                <div class="p-5 border-t border-gray-200 dark:border-dark-5 flex">
                    @if(auth()->user()->role == "ADMIN")
                        <a href="{{ route('configuration-live-view') . App\Helper::url_query_string() }}"
                           class="btn btn-primary py-1 px-2">Prev</a>
                    @endif
                </div>
            </div>
        </div>
        <!-- END: Profile Menu -->
        <div class="col-span-12 lg:col-span-8 2xl:col-span-9">
            <!-- BEGIN: Change Password -->
            <div class="intro-y box lg:mt-5">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Gallery Configuration</h2>
                </div>
                <div class="p-5">
                    <div class="intro-y box">
                        <div
                            class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                            <h2 class="font-medium text-base mr-auto">Gallery File</h2>
                        </div>
                        <div class="preview">
                            <form data-file-types="image/jpeg|image/png|image/jpg"
                                  action="{{ route('configuration-gallery-upload') }}" class="dropzone" data-max-filesize="5"
                                  enctype="multipart/form-data">
                                <div class="fallback">
                                    <input name="file" type="file"/>
                                </div>
                                <div class="dz-message" data-dz-message>
                                    <div class="text-lg font-medium">Drop files here or click to upload.</div>
                                    <div class="text-gray-600">
                                        Only jpeg, png, jpg and pdf <span class="font-medium">can</span> be uploaded.
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y grid grid-cols-12 gap-3 sm:gap-6 mt-5">
                @foreach ($gallerys as $gallery)
                    <div class="intro-y col-span-12 sm:col-span-8 md:col-span-6 2xl:col-span-4">
                        <div class="file box rounded-md px-5 pt-8 pb-5 px-3 sm:px-5 relative zoom-in">
                            <div class="w-3/5 file__icon file__icon--image mx-auto">
                                <div class="file__icon--image__preview image-fit">
                                    <img src="{{ 'storage/'. $gallery['photo_file']}}" data-action="zoom">
                                </div>
                            </div>
                            <a href=""
                               class="block font-medium mt-4 text-center truncate">photo</a>
                            <div class="text-gray-600 text-xs text-center mt-0.5">{{ $no }}</div>
                            <div class="absolute top-0 right-0 mr-2 mt-2 dropdown ml-auto">
                                <a class="dropdown-toggle w-5 h-5 block" href="javascript:;" aria-expanded="false">
                                    <i data-feather="more-vertical" class="w-5 h-5 text-gray-600"></i>
                                </a>
                                <div class="dropdown-menu w-40">
                                    <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                                        <a
                                            id="btn-update"
                                            href="javascript:void(0)"
                                            class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2  rounded-md"
                                            data-gallery-id="{{ $gallery['id'] }}">
                                            <i data-feather="trash" class="w-4 h-4 mr-2"></i> Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php($no = $no + 1)
                @endforeach
            </div>
            @if(!empty($gallerys))
                <div class="intro-y flex flex-wrap sm:flex-row sm:flex-nowrap items-center mt-6">
                    {!! $pagination !!}
                </div>
        @endif
        <!-- END: Change Password -->
        </div>
    </div>

    <div id="btn-modal-delete" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <a data-dismiss="modal" href="javascript:;">
                    <i data-feather="x" class="w-8 h-8 text-gray-500"></i>
                </a>
                <div class="modal-body p-0">
                    <div class="p-5 text-center">
                        <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Warning</div>
                        <div class="text-gray-600 mt-2">Are you sure want to delete this image?</div>
                    </div>
                    <div class="px-5 pb-8 text-center">
                        <button id="btn-cancel" type="button" data-dismiss="modal" class="btn btn-secondary w-24">
                            Cancel
                        </button>
                        <button id="btn-remove" type="button" class="btn btn-primary w-24">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Modal Content -->
@endsection

@section('script')
    <script>
        cash(function () {
            async function modalremove(id) {

                // set modal
                cash('#btn-modal-delete').modal('show');

                // set link data
                cash('#btn-remove').data('gallery-id', id);
            }

            cash('#btn-update').on('click', function () {
                modalremove(cash(this).data('gallery-id'))
            })

            async function remove(id) {

                // set modal
                cash('#btn-modal-delete').modal('show');

                // set link data
                cash('#btn-remove').data('gallery-id', id);

                // Loading state
                cash('#btn-cancel').hide()
                cash('#btn-remove').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post('{{ route('configuration-gallery-remove') }}', {
                    id: id,
                }).then(res => {
                    cash('#btn-cancel').show();
                    cash('#btn-remove').html('Ok');
                    cash('#btn-modal-delete').modal('hide');
                    if (!res.data.success) {
                        helper.showNotification('error', 'error remove data', 'harap ulangi');
                    } else {
                        helper.showNotification('success', 'success remove data', 'halaman akan di refresh');
                        setTimeout(function () {
                            window.location.reload(true);
                            window.location.reload(true);
                        }, 500);

                    }
                }).catch(err => {
                    cash('#btn-cancel').show();
                    cash('#btn-remove').html('Ok');
                    cash('#btn-modal-delete').modal('hide');
                    helper.showNotification('error', 'error remove data');
                })
            }

            cash('#btn-remove').on('click', function () {
                remove(cash(this).data('gallery-id'))
            })
        })
    </script>
@endsection
