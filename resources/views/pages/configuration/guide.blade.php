@extends('../../layout/' . $layout)

@section('subhead')
    <title>Fathariz Talita - Wedding CMS - File Configuration</title>
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">File Configuration</h2>
    </div>
    <div class="grid grid-cols-12 gap-6">
        <!-- BEGIN: Profile Menu -->
        <div class="col-span-12 lg:col-span-4 2xl:col-span-3 flex lg:block flex-col-reverse">
            <div class="intro-y box mt-5">
                <div class="relative flex items-center p-5">
                    <div class="w-12 h-12 image-fit">
                        <img alt="TalitaFathariz Tailwind HTML Admin Template" class="rounded-full"
                             src="{{ asset('dist/images/' . $fakers[0]['photos'][0]) }}">
                    </div>
                    <div class="ml-4 mr-auto">
                        <div class="font-medium text-base">{{ auth()->user()->name }}</div>
                        <div class="text-gray-600">{{ auth()->user()->role }}</div>
                    </div>
                </div>
                <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                    <a class="flex items-center"
                       href="{{ route('configuration-personal-view') . App\Helper::url_query_string() }}">
                        <i data-feather="user" class="w-4 h-4 mr-2"></i> Personal Configuration
                    </a>
                </div>
                @if(auth()->user()->role == "ADMIN")
                    <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                        <a class="flex items-center"
                           href="{{ route('configuration-couple-view') . App\Helper::url_query_string() }}">
                            <i data-feather="users" class="w-4 h-4 mr-2"></i> Couple Configuration
                        </a>
                        <a class="flex items-center mt-5"
                           href="{{ route('configuration-akad-view') . App\Helper::url_query_string() }}">
                            <i data-feather="map" class="w-4 h-4 mr-2"></i> Akad Configuration
                        </a>
                        <a class="flex items-center mt-5"
                           href="{{ route('configuration-walimah-view') . App\Helper::url_query_string() }}">
                            <i data-feather="map-pin" class="w-4 h-4 mr-2"></i> Walimah Configuration
                        </a>
                        <a class="flex items-center text-theme-1 dark:text-theme-10 font-medium mt-5"
                           href="{{ route('configuration-guide-view') . App\Helper::url_query_string() }}">
                            <i data-feather="file" class="w-4 h-4 mr-2"></i> File Configuration
                        </a>
                        <a class="flex items-center mt-5"
                           href="{{ route('configuration-bank-view') . App\Helper::url_query_string() }}">
                            <i data-feather="dollar-sign" class="w-4 h-4 mr-2"></i> Bank Configuration
                        </a>
                        <a class="flex items-center mt-5"
                           href="{{ route('configuration-live-view') . App\Helper::url_query_string() }}">
                            <i data-feather="phone" class="w-4 h-4 mr-2"></i> Live Configuration
                        </a>
                        <a class="flex items-center mt-5"
                           href="{{ route('configuration-gallery-view') . App\Helper::url_query_string() }}">
                            <i data-feather="film" class="w-4 h-4 mr-2"></i> Gallery Configuration
                        </a>
                    </div>
                @endif
                <div class="p-5 border-t border-gray-200 dark:border-dark-5 flex">
                    @if(auth()->user()->role == "ADMIN")
                        <a href="{{ route('configuration-walimah-view') . App\Helper::url_query_string() }}"
                           class="btn btn-primary py-1 px-2">Prev</a>
                        <a href="{{ route('configuration-bank-view') . App\Helper::url_query_string() }}"
                           class="btn btn-outline-secondary py-1 px-2 ml-auto">Next</a>
                    @endif
                </div>
            </div>
        </div>
        <!-- END: Profile Menu -->
        <div class="col-span-12 lg:col-span-8 2xl:col-span-9">
            <!-- BEGIN: Change Password -->
            <div class="intro-y box lg:mt-5">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">File Configuration</h2>
                </div>
                <div class="p-5">
                    <div class="intro-y box">
                        <div
                            class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                            <h2 class="font-medium text-base mr-auto">Qris File</h2>
                        </div>
                        <div class="preview">
                            @if(!empty($configuration['qris_file']))
                                <div class="w-full h-64 image-fit">
                                    <img src="{{ 'storage/'. $configuration['qris_file']}}" data-action="zoom"
                                         class="object-center md:object-top sm:w-full w-full rounded-md">
                                </div>
                            @endif
                            <form data-single="true" data-file-types="image/jpeg|image/png|image/jpg"
                                  action="{{ route('configuration-qris-upload') }}" class="dropzone"
                                  data-max-filesize="5"
                                  enctype="multipart/form-data">
                                <div class="fallback">
                                    <input name="file" type="file"/>
                                </div>
                                <div class="dz-message" data-dz-message>
                                    <div class="text-lg font-medium">Drop files here or click to upload.</div>
                                    <div class="text-gray-600">
                                        Only jpeg, png, and jpg <span class="font-medium">can</span> be uploaded.
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="intro-y box mt-3">
                        <div
                            class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                            <h2 class="font-medium text-base mr-auto">Song File</h2>
                        </div>
                        <div class="preview text-center">
                            @if(!empty($configuration['song_file']))
                                <div class="m-5">
                                    <button id="btn-play" class="w-full h-12 btn btn-primary mb-2">
                                        <i data-feather="play" class="w-4 h-4 mr-2"></i> Play song
                                    </button>
                                    <button id="btn-pause" class="w-full h-12 btn btn-secondary mb-2"
                                            style="display: none">
                                        <i data-feather="pause" class="w-4 h-4 mr-2"></i> Pause song
                                    </button>
                                </div>
                                <div id="progress-div" class="m-5" style="display: none">
                                    <strong><p id="progress-text" class="text-big text-center mb-2"></p></strong>
                                    <div class="progress mt-2 mb-2" style="height:15px;">
                                        <div id="progress-song"
                                             class="progress-bar progress-bar-striped progress-bar-animated mt-2 bg-theme-1 rounded"
                                             role="progressbar"
                                             style="width: 0%" aria-valuenow="0"
                                             aria-valuemin="0" aria-valuemax="100">0%
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <form data-single="true" data-file-types="audio/mpeg"
                                  action="{{ route('configuration-song-upload') }}" class="dropzone"
                                  data-max-filesize="5"
                                  enctype="multipart/form-data">
                                <div class="fallback">
                                    <input name="file" type="file"/>
                                </div>
                                <div class="dz-message" data-dz-message>
                                    <div class="text-lg font-medium">Drop files here or click to upload.</div>
                                    <div class="text-gray-600">
                                        Only mp3 <span class="font-medium">can</span> be uploaded.
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    {{--                    <div class="intro-y box mt-3">--}}
                    {{--                        <div--}}
                    {{--                            class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">--}}
                    {{--                            <h2 class="font-medium text-base mr-auto">Invitation File</h2>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="preview">--}}
                    {{--                            @if(!empty($configuration['invitation_file']))--}}
                    {{--                                <div class="w-full h-64 image-fit">--}}
                    {{--                                    <img src="{{ 'storage/'. $configuration['invitation_file']}}" data-action="zoom"--}}
                    {{--                                         class="object-center md:object-top sm:w-full w-full rounded-md">--}}
                    {{--                                </div>--}}
                    {{--                            @endif--}}
                    {{--                            <form data-single="true" data-file-types="image/jpeg|image/png|image/jpg"--}}
                    {{--                                  action="{{ route('configuration-invitation-upload') }}" class="dropzone" data-max-filesize="5"--}}
                    {{--                                  enctype="multipart/form-data">--}}
                    {{--                                <div class="fallback">--}}
                    {{--                                    <input name="file" type="file"/>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="dz-message" data-dz-message>--}}
                    {{--                                    <div class="text-lg font-medium">Drop files here or click to upload.</div>--}}
                    {{--                                    <div class="text-gray-600">--}}
                    {{--                                        Only jpeg, png, and jpg <span class="font-medium">can</span> be uploaded.--}}
                    {{--                                    </div>--}}
                    {{--                                </div>--}}
                    {{--                            </form>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <div class="intro-y box mt-3">
                        <div
                            class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                            <h2 class="font-medium text-base mr-auto">Protocol File</h2>
                        </div>
                        <div class="preview">
                            @if(!empty($configuration['protocol_file']))
                                <div class="w-full h-64 image-fit">
                                    <img src="{{ 'storage/'. $configuration['protocol_file']}}" data-action="zoom"
                                         class="object-center md:object-top sm:w-full w-full rounded-md">
                                </div>
                            @endif
                            <form data-single="true" data-file-types="image/jpeg|image/png|image/jpg"
                                  action="{{ route('configuration-protocol-upload') }}" class="dropzone"
                                  data-max-filesize="5"
                                  enctype="multipart/form-data">
                                <div class="fallback">
                                    <input name="file" type="file"/>
                                </div>
                                <div class="dz-message" data-dz-message>
                                    <div class="text-lg font-medium">Drop files here or click to upload.</div>
                                    <div class="text-gray-600">
                                        Only jpeg, png, and jpg <span class="font-medium">can</span> be uploaded.
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="intro-y box mt-3">
                        <div
                            class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                            <h2 class="font-medium text-base mr-auto">Guide File (optional)</h2>
                        </div>
                        <div class="preview">
                            @if(!empty($configuration['guide_file']))
                                @if(!str_contains($configuration['guide_file'], 'pdf'))
                                    <div class="w-full h-64 image-fit">
                                        <img src="{{ 'storage/'. $configuration['guide_file']}}" data-action="zoom"
                                             class="object-center md:object-top sm:w-full w-full rounded-md">
                                    </div>
                                @else
                                    <object data="{{ 'storage/'. $configuration['guide_file']}}"
                                            type="application/pdf" width="100%" height="800px">
                                        <p>It appears you don't have a PDF plugin for this browser.
                                            No biggie... you can <a
                                                href="{{ 'storage/'. $configuration['guide_file']}}">click here
                                                to
                                                download the PDF file.</a></p>
                                    </object>
                                @endif
                            @endif
                            <form data-single="true" data-file-types="image/jpeg|image/png|image/jpg|application/pdf"
                                  action="{{ route('configuration-guide-upload') }}" class="dropzone"
                                  data-max-filesize="5"
                                  enctype="multipart/form-data">
                                <div class="fallback">
                                    <input name="file" type="file"/>
                                </div>
                                <div class="dz-message" data-dz-message>
                                    <div class="text-lg font-medium">Drop files here or click to upload.</div>
                                    <div class="text-gray-600">
                                        Only jpeg, png, jpg and pdf <span class="font-medium">can</span> be uploaded.
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="intro-y box mt-3">
                        <div
                            class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                            <h2 class="font-medium text-base mr-auto">Guide Non QR File (optional)</h2>
                        </div>
                        <div class="preview">
                            @if(!empty($configuration['guide_old_file']))
                                @if(!str_contains($configuration['guide_old_file'], 'pdf'))
                                    <div class="w-full h-64 image-fit">
                                        <img src="{{ 'storage/'. $configuration['guide_old_file']}}" data-action="zoom"
                                             class="object-center md:object-top sm:w-full w-full rounded-md">
                                    </div>
                                @else
                                    <object data="{{ 'storage/'. $configuration['guide_old_file']}}"
                                            type="application/pdf" width="100%" height="800px">
                                        <p>It appears you don't have a PDF plugin for this browser.
                                            No biggie... you can <a
                                                href="{{ 'storage/'. $configuration['guide_old_file']}}">click here
                                                to
                                                download the PDF file.</a></p>
                                    </object>
                                @endif
                            @endif
                            <form data-single="true" data-file-types="image/jpeg|image/png|image/jpg|application/pdf"
                                  action="{{ route('configuration-guide-old-upload') }}" class="dropzone"
                                  data-max-filesize="5"
                                  enctype="multipart/form-data">
                                <div class="fallback">
                                    <input name="file" type="file"/>
                                </div>
                                <div class="dz-message" data-dz-message>
                                    <div class="text-lg font-medium">Drop files here or click to upload.</div>
                                    <div class="text-gray-600">
                                        Only jpeg, png, jpg and pdf <span class="font-medium">can</span> be uploaded.
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: Change Password -->
        </div>
    </div>
@endsection

@section('script')
    @if(!empty($configuration['song_file']))
        <script src="https://cdnjs.cloudflare.com/ajax/libs/howler/2.2.3/howler.min.js"></script>
        <script>
            var interval_duration, song, percent, progress_bar, progress_text;

            // set percent
            percent = 0;

            // set progress
            progress_bar = cash('#progress-song');
            progress_text = cash('#progress-text');

            // set song
            song = new Howl({
                src: ['{{ 'storage/'. $configuration['song_file']}}', 'song.mp3'],
                html: true,
                onplay: function () {
                    cash('#btn-play').hide();
                    cash('#btn-pause').show();
                    cash('#progress-div').show();

                    // set interval within 1 second
                    interval_duration = setInterval(frame, 100);

                },
                onpause: function () {
                    cash('#btn-play').show();
                    cash('#btn-pause').hide();
                    cash('#progress-div').hide();

                    // clear interval
                    clearInterval(interval_duration);
                },
                onend: function () {
                    cash('#btn-play').show();
                    cash('#btn-pause').hide();
                    cash('#progress-div').hide();
                },
            });

            function frame() {
                if (percent == 100) {
                    clearInterval(interval_duration);
                    percent = 0;
                } else {
                    percent = Math.round((((song.seek() / song.duration()) * 100) || 0));
                }

                // set percent and html
                progress_bar.css('width', percent + '%');
                progress_bar.html(percent + '%');
                progress_bar.attr('aria-valuenow', percent);
                progress_text.html(formatTimefunction(Math.round(song.seek())) + " - " + formatTimefunction(Math.round(song.duration())));
            }

            function formatTimefunction(secs) {
                var minutes = Math.floor(secs / 60) || 0;
                var seconds = (secs - minutes * 60) || 0;

                return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
            }

            cash(function () {
                async function play() {
                    song.play();
                }

                cash('#btn-play').on('click', function () {
                    play()
                })

                async function pause() {
                    song.pause();
                }

                cash('#btn-pause').on('click', function () {
                    pause()
                })
            })

        </script>
    @endif
@endsection
