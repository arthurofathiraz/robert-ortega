@extends('../../layout/' . $layout)

@section('subhead')
    <title>Fathariz Talita - Wedding CMS - Bank Configuration</title>
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Bank Configuration</h2>
    </div>
    <div class="grid grid-cols-12 gap-6">
        <!-- BEGIN: Profile Menu -->
        <div class="col-span-12 lg:col-span-4 2xl:col-span-3 flex lg:block flex-col-reverse">
            <div class="intro-y box mt-5">
                <div class="relative flex items-center p-5">
                    <div class="w-12 h-12 image-fit">
                        <img alt="TalitaFathariz Tailwind HTML Admin Template" class="rounded-full"
                             src="{{ asset('dist/images/' . $fakers[0]['photos'][0]) }}">
                    </div>
                    <div class="ml-4 mr-auto">
                        <div class="font-medium text-base">{{ auth()->user()->name }}</div>
                        <div class="text-gray-600">{{ auth()->user()->role }}</div>
                    </div>
                </div>
                <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                    <a class="flex items-center"
                       href="{{ route('configuration-personal-view') . App\Helper::url_query_string() }}">
                        <i data-feather="user" class="w-4 h-4 mr-2"></i> Personal Configuration
                    </a>
                </div>
                @if(auth()->user()->role == "ADMIN")
                    <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                        <a class="flex items-center" href="{{ route('configuration-couple-view') . App\Helper::url_query_string() }}">
                            <i data-feather="users" class="w-4 h-4 mr-2"></i> Couple Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-akad-view') . App\Helper::url_query_string() }}">
                            <i data-feather="map" class="w-4 h-4 mr-2"></i> Akad Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-walimah-view') . App\Helper::url_query_string() }}">
                            <i data-feather="map-pin" class="w-4 h-4 mr-2"></i> Walimah Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-guide-view') . App\Helper::url_query_string() }}">
                            <i data-feather="file" class="w-4 h-4 mr-2"></i> File Configuration
                        </a>
                        <a class="flex items-center text-theme-1 dark:text-theme-10 font-medium mt-5" href="{{ route('configuration-bank-view') . App\Helper::url_query_string() }}">
                            <i data-feather="dollar-sign" class="w-4 h-4 mr-2"></i> Bank Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-live-view') . App\Helper::url_query_string() }}">
                            <i data-feather="phone" class="w-4 h-4 mr-2"></i> Live Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-gallery-view') . App\Helper::url_query_string() }}">
                            <i data-feather="film" class="w-4 h-4 mr-2"></i> Gallery Configuration
                        </a>
                    </div>
                @endif
                <div class="p-5 border-t border-gray-200 dark:border-dark-5 flex">
                    @if(auth()->user()->role == "ADMIN")
                        <a href="{{ route('configuration-guide-view') . App\Helper::url_query_string() }}"
                           class="btn btn-primary py-1 px-2">Prev</a>
                        <a href="{{ route('configuration-live-view') . App\Helper::url_query_string() }}"
                           class="btn btn-outline-secondary py-1 px-2 ml-auto">Next</a>
                    @endif
                </div>
            </div>
        </div>
        <!-- END: Profile Menu -->
        <div class="col-span-12 lg:col-span-8 2xl:col-span-9">
            <!-- BEGIN: Change Password -->
            <div class="intro-y box lg:mt-5">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Bank Configuration</h2>
                </div>
                <div class="p-5">
                    <form id="inputan-form">
                        <div>
                            <label>Switch</label>
                            <div class="mt-2">
                                <div class="form-check">
                                    <input id="is_active" class="form-check-switch form__inputan" type="checkbox"
                                           value="{{ !empty($configuration->bank_account_number) ? 'yes' : 'no' }}" {{ !empty($configuration->bank_account_number) ? 'checked' : '' }}>
                                    <label class="form-check-label" for="is_active">Bank account show</label>
                                </div>
                            </div>
                            <div id="error-is_active"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="bank_account_number" class="form-label">Bank Account Number</label>
                            <input id="bank_account_number" type="text" class="form-control form__inputan"
                                   placeholder="Input bank account number"
                                   value="{{ $configuration->bank_account_number }}">
                            <div id="error-bank_account_number"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="bank_account_name" class="form-label">Bank Account Name</label>
                            <input id="bank_account_name" type="text" class="form-control form__inputan"
                                   placeholder="Input bank account name"
                                   value="{{ $configuration->bank_account_name }}">
                            <div id="error-bank_account_name"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="bank_account" class="form-label">Bank Account</label>
                            <input id="bank_account" type="text" class="form-control form__inputan"
                                   placeholder="Input bank account (BCA/MANDIRI)"
                                   value="{{ $configuration->bank_account }}">
                            <div id="error-bank_account"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                    </form>
                    <button id="btn-update" class="btn btn-primary mt-4">Update</button>
                </div>
            </div>
            <!-- END: Change Password -->
        </div>
    </div>
@endsection

@section('script')
    <script>
        cash(function () {
            async function update() {
                // Reset state
                cash('#inputan-form').find('.form__inputan').removeClass('border-theme-6')
                cash('#inputan-form').find('.form__inputan-error').html('')

                // Post form
                let bank_account_number = cash('#bank_account_number').val()
                let bank_account_name = cash('#bank_account_name').val()
                let bank_account = cash('#bank_account').val()
                let is_active = cash('#is_active').val()

                // Loading state
                cash('#btn-update').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post('{{ route('configuration-bank') }}', {
                    bank_account_number: bank_account_number,
                    bank_account_name: bank_account_name,
                    bank_account: bank_account,
                    is_active: is_active,
                }).then(res => {
                    cash('#btn-update').html('Update');
                    if (!res.data.success) {
                        for (const [key, val] of Object.entries(res.data.errors)) {
                            cash(`#${key}`).addClass('border-theme-6')
                            cash(`#error-${key}`).html(val)
                        }
                        helper.showNotification('error', 'error update data', 'harap cek kembali inputan anda');
                    } else {
                        helper.showNotification('success', 'success update data', 'halaman akan di refresh');
                        setTimeout(function () {
                            window.location.reload(true);
                            window.location.reload(true);
                        }, 500);

                    }
                }).catch(err => {
                    cash('#btn-update').html('Update');
                    helper.showNotification('error', 'error update data');
                })
            }

            cash('form input').on('keydown', function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            })

            cash('#btn-update').on('click', function () {
                update()
            })

            // bank button
            @if(!empty($configuration->bank_account_number))
            bankEnabled()
            @else
            bankDisabled()
            @endif

            async function disabled(value) {
                if (value == 'yes') {
                    bankDisabled()
                    cash('#is_active').attr('value', 'no');
                } else if (value == 'no') {
                    bankEnabled()
                    cash('#is_active').attr('value', 'yes');
                }
            }

            async function bankDisabled() {
                cash('#bank_account_number').attr('disabled', true);
                cash('#bank_account_name').attr('disabled', true);
                cash('#bank_account').attr('disabled', true);
            }

            async function bankEnabled() {
                cash('#bank_account_number').removeAttr('disabled');
                cash('#bank_account_name').removeAttr('disabled');
                cash('#bank_account').removeAttr('disabled');
            }

            cash('#is_active').on('click', function () {
                disabled(cash('#is_active').val())
            })
        })
    </script>
@endsection
