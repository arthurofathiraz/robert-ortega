@extends('../../layout/' . $layout)

@section('subhead')
    <title>Fathariz Talita - Wedding CMS - Couple Configuration</title>
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Couple Configuration</h2>
    </div>
    <div class="grid grid-cols-12 gap-6">
        <!-- BEGIN: Profile Menu -->
        <div class="col-span-12 lg:col-span-4 2xl:col-span-3 flex lg:block flex-col-reverse">
            <div class="intro-y box mt-5">
                <div class="relative flex items-center p-5">
                    <div class="w-12 h-12 image-fit">
                        <img alt="TalitaFathariz Tailwind HTML Admin Template" class="rounded-full"
                             src="{{ asset('dist/images/' . $fakers[0]['photos'][0]) }}">
                    </div>
                    <div class="ml-4 mr-auto">
                        <div class="font-medium text-base">{{ auth()->user()->name }}</div>
                        <div class="text-gray-600">{{ auth()->user()->role }}</div>
                    </div>
                </div>
                <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                    <a class="flex items-center"
                       href="{{ route('configuration-personal-view') . App\Helper::url_query_string() }}">
                        <i data-feather="user" class="w-4 h-4 mr-2"></i> Personal Configuration
                    </a>
                </div>
                @if(auth()->user()->role == "ADMIN")
                    <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                        <a class="flex items-center text-theme-1 dark:text-theme-10 font-medium" href="{{ route('configuration-couple-view') . App\Helper::url_query_string() }}">
                            <i data-feather="users" class="w-4 h-4 mr-2"></i> Couple Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-akad-view') . App\Helper::url_query_string() }}">
                            <i data-feather="map" class="w-4 h-4 mr-2"></i> Akad Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-walimah-view') . App\Helper::url_query_string() }}">
                            <i data-feather="map-pin" class="w-4 h-4 mr-2"></i> Walimah Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-guide-view') . App\Helper::url_query_string() }}">
                            <i data-feather="file" class="w-4 h-4 mr-2"></i> File Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-bank-view') . App\Helper::url_query_string() }}">
                            <i data-feather="dollar-sign" class="w-4 h-4 mr-2"></i> Bank Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-live-view') . App\Helper::url_query_string() }}">
                            <i data-feather="phone" class="w-4 h-4 mr-2"></i> Live Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-gallery-view') . App\Helper::url_query_string() }}">
                            <i data-feather="film" class="w-4 h-4 mr-2"></i> Gallery Configuration
                        </a>
                    </div>
                @endif
                <div class="p-5 border-t border-gray-200 dark:border-dark-5 flex">
                    @if(auth()->user()->role == "ADMIN")
                        <a href="{{ route('configuration-personal-view') . App\Helper::url_query_string() }}"
                           class="btn btn-primary py-1 px-2">Prev</a>
                        <a href="{{ route('configuration-akad-view') . App\Helper::url_query_string() }}"
                           class="btn btn-outline-secondary py-1 px-2 ml-auto">Next</a>
                    @endif
                </div>
            </div>
        </div>
        <!-- END: Profile Menu -->
        <div class="col-span-12 lg:col-span-8 2xl:col-span-9">
            <!-- BEGIN: Change Password -->
            <div class="intro-y box lg:mt-5">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Couple Configuration</h2>
                </div>
                <div class="p-5">
                    <form id="inputan-form">
                        <div>
                            <label for="male_name" class="form-label">Male Name</label>
                            <input id="male_name" type="text" class="form-control form__inputan"
                                   placeholder="Input male name" value="{{ $configuration->male_name }}">
                            <div id="error-male_name" class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div>
                            <label for="male_nickname" class="form-label">Male Nickname</label>
                            <input id="male_nickname" type="text" class="form-control form__inputan"
                                   placeholder="Input male nickname" value="{{ $configuration->male_nickname }}">
                            <div id="error-male_nickname" class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="male_description" class="form-label">Male Description</label>
                            <textarea id="male_description" type="text" rows="5" class="form-control  form__inputan"
                                      placeholder="Input male description">{{ $configuration->male_description }}</textarea>
                            <div id="error-male_description" class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="female_name" class="form-label">Female Name</label>
                            <input id="female_name" type="text" class="form-control form__inputan"
                                   placeholder="Input female name" value="{{ $configuration->female_name }}">
                            <div id="error-female_name" class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div>
                            <label for="female_nickname" class="form-label">Female Nickname</label>
                            <input id="female_nickname" type="text" class="form-control form__inputan"
                                   placeholder="Input female nickname" value="{{ $configuration->female_nickname }}">
                            <div id="error-female_nickname" class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="female_description" class="form-label">Female Description</label>
                            <textarea id="female_description" type="text" rows="5" class="form-control  form__inputan"
                                      placeholder="Input female description">{{ $configuration->female_description }}</textarea>
                            <div id="error-female_description"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                    </form>
                    <button id="btn-update" class="btn btn-primary mt-4">Update</button>
                </div>
            </div>
            <!-- BEGIN: Change Password -->
            <div class="intro-y box lg:mt-5">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Couple Photo Configuration</h2>
                </div>
                <div class="p-5">
                    <div class="intro-y box mt-3">
                        <div
                            class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                            <h2 class="font-medium text-base mr-auto">Male Photo</h2>
                        </div>
                        <div class="preview">
                            @if(!empty($configuration['male_photo']))
                                <div class="w-full h-64 image-fit">
                                    <img src="{{ 'storage/'. $configuration['male_photo']}}" data-action="zoom"
                                         class="object-center md:object-top sm:w-full w-full rounded-md">
                                </div>
                            @endif
                            <form data-single="true" data-file-types="image/jpeg|image/png|image/jpg"
                                  action="{{ route('configuration-male-upload') }}" class="dropzone" data-max-filesize="5"
                                  enctype="multipart/form-data">
                                <div class="fallback">
                                    <input name="file" type="file"/>
                                </div>
                                <div class="dz-message" data-dz-message>
                                    <div class="text-lg font-medium">Drop files here or click to upload.</div>
                                    <div class="text-gray-600">
                                        Only jpeg, png, and jpg <span class="font-medium">can</span> be uploaded.
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="intro-y box mt-3">
                        <div
                            class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                            <h2 class="font-medium text-base mr-auto">Female Photo</h2>
                        </div>
                        <div class="preview">
                            @if(!empty($configuration['female_photo']))
                                <div class="w-full h-64 image-fit">
                                    <img src="{{ 'storage/'. $configuration['female_photo']}}" data-action="zoom"
                                         class="object-center md:object-top sm:w-full w-full rounded-md">
                                </div>
                            @endif
                            <form data-single="true" data-file-types="image/jpeg|image/png|image/jpg"
                                  action="{{ route('configuration-female-upload') }}" class="dropzone" data-max-filesize="5"
                                  enctype="multipart/form-data">
                                <div class="fallback">
                                    <input name="file" type="file"/>
                                </div>
                                <div class="dz-message" data-dz-message>
                                    <div class="text-lg font-medium">Drop files here or click to upload.</div>
                                    <div class="text-gray-600">
                                        Only jpeg, png, and jpg <span class="font-medium">can</span> be uploaded.
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END: Change Password -->
        </div>
    </div>
@endsection

@section('script')
    <script>
        cash(function () {
            async function update() {
                // Reset state
                cash('#inputan-form').find('.form__inputan').removeClass('border-theme-6')
                cash('#inputan-form').find('.form__inputan-error').html('')

                // Post form
                let male_name = cash('#male_name').val()
                let female_name = cash('#female_name').val()
                let male_description = cash('#male_description').val()
                let female_description = cash('#female_description').val()
                let male_nickname = cash('#male_nickname').val()
                let female_nickname = cash('#female_nickname').val()

                // Loading state
                cash('#btn-update').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post('{{ route('configuration-couple') }}', {
                    male_name: male_name,
                    female_name: female_name,
                    male_description: male_description,
                    female_description: female_description,
                    male_nickname: male_nickname,
                    female_nickname: female_nickname,
                }).then(res => {
                    cash('#btn-update').html('Update');
                    if (!res.data.success) {
                        for (const [key, val] of Object.entries(res.data.errors)) {
                            cash(`#${key}`).addClass('border-theme-6')
                            cash(`#error-${key}`).html(val)
                        }
                        helper.showNotification('error', 'error update data', 'harap cek kembali inputan anda');
                    } else {
                        helper.showNotification('success', 'success update data', 'halaman akan di refresh');
                        setTimeout(function () {
                            window.location.reload(true);
                            window.location.reload(true);
                        }, 500);

                    }
                }).catch(err => {
                    cash('#btn-update').html('Update');
                    helper.showNotification('error', 'error update data');
                })
            }

            cash('form input').on('keydown', function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            })

            cash('#btn-update').on('click', function () {
                update()
            })
        })
    </script>
@endsection
