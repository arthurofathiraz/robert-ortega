@extends('../../layout/' . $layout)

@section('subhead')
    <title>Fathariz Talita - Wedding CMS - Live Configuration</title>
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Live Configuration</h2>
    </div>
    <div class="grid grid-cols-12 gap-6">
        <!-- BEGIN: Profile Menu -->
        <div class="col-span-12 lg:col-span-4 2xl:col-span-3 flex lg:block flex-col-reverse">
            <div class="intro-y box mt-5">
                <div class="relative flex items-center p-5">
                    <div class="w-12 h-12 image-fit">
                        <img alt="TalitaFathariz Tailwind HTML Admin Template" class="rounded-full"
                             src="{{ asset('dist/images/' . $fakers[0]['photos'][0]) }}">
                    </div>
                    <div class="ml-4 mr-auto">
                        <div class="font-medium text-base">{{ auth()->user()->name }}</div>
                        <div class="text-gray-600">{{ auth()->user()->role }}</div>
                    </div>
                </div>
                <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                    <a class="flex items-center"
                       href="{{ route('configuration-personal-view') . App\Helper::url_query_string() }}">
                        <i data-feather="user" class="w-4 h-4 mr-2"></i> Personal Configuration
                    </a>
                </div>
                @if(auth()->user()->role == "ADMIN")
                    <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                        <a class="flex items-center" href="{{ route('configuration-couple-view') . App\Helper::url_query_string() }}">
                            <i data-feather="users" class="w-4 h-4 mr-2"></i> Couple Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-akad-view') . App\Helper::url_query_string() }}">
                            <i data-feather="map" class="w-4 h-4 mr-2"></i> Akad Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-walimah-view') . App\Helper::url_query_string() }}">
                            <i data-feather="map-pin" class="w-4 h-4 mr-2"></i> Walimah Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-guide-view') . App\Helper::url_query_string() }}">
                            <i data-feather="file" class="w-4 h-4 mr-2"></i> File Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-bank-view') . App\Helper::url_query_string() }}">
                            <i data-feather="dollar-sign" class="w-4 h-4 mr-2"></i> Bank Configuration
                        </a>
                        <a class="flex items-center text-theme-1 dark:text-theme-10 font-medium mt-5" href="{{ route('configuration-live-view') . App\Helper::url_query_string() }}">
                            <i data-feather="phone" class="w-4 h-4 mr-2"></i> Live Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-gallery-view') . App\Helper::url_query_string() }}">
                            <i data-feather="film" class="w-4 h-4 mr-2"></i> Gallery Configuration
                        </a>
                    </div>
                @endif
                <div class="p-5 border-t border-gray-200 dark:border-dark-5 flex">
                    @if(auth()->user()->role == "ADMIN")
                        <a href="{{ route('configuration-bank-view') . App\Helper::url_query_string() }}"
                           class="btn btn-primary py-1 px-2">Prev</a>
                        <a href="{{ route('configuration-gallery-view') . App\Helper::url_query_string() }}"
                           class="btn btn-outline-secondary py-1 px-2 ml-auto">Next</a>
                    @endif
                </div>
            </div>
        </div>
        <!-- END: Profile Menu -->
        <div class="col-span-12 lg:col-span-8 2xl:col-span-9">
            <!-- BEGIN: Change Password -->
            <div class="intro-y box lg:mt-5">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Live Configuration</h2>
                </div>
                <div class="p-5">
                    <form id="inputan-form">
                        <div>
                            <label>Switch</label>
                            <div class="mt-2">
                                <div class="form-check">
                                    <input id="is_active_zoom" class="form-check-switch form__inputan" type="checkbox"
                                           value="{{ !empty($configuration->zoom_live_url) ? 'yes' : 'no' }}" {{ !empty($configuration->zoom_live_url) ? 'checked' : '' }}>
                                    <label class="form-check-label" for="is_active_zoom">Zoom live show</label>
                                </div>
                            </div>
                            <div id="error-is_active_zoom"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="zoom_live_url" class="form-label">Zoom url</label>
                            <input id="zoom_live_url" type="text" class="form-control form__inputan"
                                   placeholder="Input zoom live url"
                                   value="{{ $configuration->zoom_live_url }}">
                            <div id="error-zoom_live_url"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>

                        <div class="mt-3">
                            <label>Switch</label>
                            <div class="mt-2">
                                <div class="form-check">
                                    <input id="is_active_meet" class="form-check-switch form__inputan" type="checkbox"
                                           value="{{ !empty($configuration->meet_live_url) ? 'yes' : 'no' }}" {{ !empty($configuration->meet_live_url) ? 'checked' : '' }}>
                                    <label class="form-check-label" for="is_active_meet">Meet live show</label>
                                </div>
                            </div>
                            <div id="error-is_active_meet"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="meet_live_url" class="form-label">Meet url</label>
                            <input id="meet_live_url" type="text" class="form-control form__inputan"
                                   placeholder="Input meet live url"
                                   value="{{ $configuration->meet_live_url }}">
                            <div id="error-meet_live_url"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>

                        <div class="mt-3">
                            <label>Switch</label>
                            <div class="mt-2">
                                <div class="form-check">
                                    <input id="is_active_instagram" class="form-check-switch form__inputan" type="checkbox"
                                           value="{{ !empty($configuration->instagram_live_id) ? 'yes' : 'no' }}" {{ !empty($configuration->instagram_live_id) ? 'checked' : '' }}>
                                    <label class="form-check-label" for="is_active_instagram">Instagram live show</label>
                                </div>
                            </div>
                            <div id="error-is_active_instagram"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="instagram_live_id" class="form-label">Instagram id</label>
                            <input id="instagram_live_id" type="text" class="form-control form__inputan"
                                   placeholder="Input instagram live id"
                                   value="{{ $configuration->instagram_live_id }}">
                            <div id="error-instagram_live_id"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                    </form>
                    <button id="btn-update" class="btn btn-primary mt-4">Update</button>
                </div>
            </div>
            <!-- END: Change Password -->
        </div>
    </div>
@endsection

@section('script')
    <script>
        cash(function () {
            async function update() {
                // Reset state
                cash('#inputan-form').find('.form__inputan').removeClass('border-theme-6')
                cash('#inputan-form').find('.form__inputan-error').html('')

                // Post form
                let is_active_zoom = cash('#is_active_zoom').val()
                let is_active_meet = cash('#is_active_meet').val()
                let is_active_instagram = cash('#is_active_instagram').val()
                let zoom_live_url = cash('#zoom_live_url').val()
                let meet_live_url = cash('#meet_live_url').val()
                let instagram_live_id = cash('#instagram_live_id').val()

                // Loading state
                cash('#btn-update').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post('{{ route('configuration-live') }}', {
                    is_active_zoom: is_active_zoom,
                    is_active_meet: is_active_meet,
                    is_active_instagram: is_active_instagram,
                    zoom_live_url: zoom_live_url,
                    meet_live_url: meet_live_url,
                    instagram_live_id: instagram_live_id,
                }).then(res => {
                    cash('#btn-update').html('Update');
                    if (!res.data.success) {
                        for (const [key, val] of Object.entries(res.data.errors)) {
                            cash(`#${key}`).addClass('border-theme-6')
                            cash(`#error-${key}`).html(val)
                        }
                        helper.showNotification('error', 'error update data', 'harap cek kembali inputan anda');
                    } else {
                        helper.showNotification('success', 'success update data', 'halaman akan di refresh');
                        setTimeout(function () {
                            window.location.reload(true);
                            window.location.reload(true);
                        }, 500);

                    }
                }).catch(err => {
                    cash('#btn-update').html('Update');
                    helper.showNotification('error', 'error update data');
                })
            }

            cash('form input').on('keydown', function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            })

            cash('#btn-update').on('click', function () {
                update()
            })

            // live zoom button
            @if(!empty($configuration->zoom_live_url))
            liveEnabledZoom()
            @else
            liveDisabledZoom()
            @endif

            async function disabledZoom(value) {
                if (value == 'yes') {
                    liveDisabledZoom()
                    cash('#is_active_zoom').attr('value', 'no');
                } else if (value == 'no') {
                    liveEnabledZoom()
                    cash('#is_active_zoom').attr('value', 'yes');
                }
            }

            async function liveDisabledZoom() {
                cash('#zoom_live_url').attr('disabled', true);
            }

            async function liveEnabledZoom() {
                cash('#zoom_live_url').removeAttr('disabled');
            }

            cash('#is_active_zoom').on('click', function () {
                disabledZoom(cash('#is_active_zoom').val())
            })

            // live meet button
            @if(!empty($configuration->meet_live_url))
            liveEnabledMeet()
            @else
            liveDisabledMeet()
            @endif

            async function disabledMeet(value) {
                if (value == 'yes') {
                    liveDisabledMeet()
                    cash('#is_active_meet').attr('value', 'no');
                } else if (value == 'no') {
                    liveEnabledMeet()
                    cash('#is_active_meet').attr('value', 'yes');
                }
            }

            async function liveDisabledMeet() {
                cash('#meet_live_url').attr('disabled', true);
            }

            async function liveEnabledMeet() {
                cash('#meet_live_url').removeAttr('disabled');
            }

            cash('#is_active_meet').on('click', function () {
                disabledMeet(cash('#is_active_meet').val())
            })

            // live instagram button
            @if(!empty($configuration->instagram_live_id))
            liveEnabledInstagram()
            @else
            liveDisabledInstagram()
            @endif

            async function disabledInstagram(value) {
                if (value == 'yes') {
                    liveDisabledInstagram()
                    cash('#is_active_instagram').attr('value', 'no');
                } else if (value == 'no') {
                    liveEnabledInstagram()
                    cash('#is_active_instagram').attr('value', 'yes');
                }
            }

            async function liveDisabledInstagram() {
                cash('#instagram_live_id').attr('disabled', true);
            }

            async function liveEnabledInstagram() {
                cash('#instagram_live_id').removeAttr('disabled');
            }

            cash('#is_active_instagram').on('click', function () {
                disabledInstagram(cash('#is_active_instagram').val())
            })
        })
    </script>
@endsection
