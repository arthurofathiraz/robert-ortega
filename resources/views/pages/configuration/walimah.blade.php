@extends('../../layout/' . $layout)

@section('subhead')
    <title>Fathariz Talita - Wedding CMS - Walimah Configuration</title>
@endsection

@section('subcontent')
    <div class="intro-y flex items-center mt-8">
        <h2 class="text-lg font-medium mr-auto">Walimah Configuration</h2>
    </div>
    <div class="grid grid-cols-12 gap-6">
        <!-- BEGIN: Profile Menu -->
        <div class="col-span-12 lg:col-span-4 2xl:col-span-3 flex lg:block flex-col-reverse">
            <div class="intro-y box mt-5">
                <div class="relative flex items-center p-5">
                    <div class="w-12 h-12 image-fit">
                        <img alt="TalitaFathariz Tailwind HTML Admin Template" class="rounded-full"
                             src="{{ asset('dist/images/' . $fakers[0]['photos'][0]) }}">
                    </div>
                    <div class="ml-4 mr-auto">
                        <div class="font-medium text-base">{{ auth()->user()->name }}</div>
                        <div class="text-gray-600">{{ auth()->user()->role }}</div>
                    </div>
                </div>
                <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                    <a class="flex items-center"
                       href="{{ route('configuration-personal-view') . App\Helper::url_query_string() }}">
                        <i data-feather="user" class="w-4 h-4 mr-2"></i> Personal Configuration
                    </a>
                </div>
                @if(auth()->user()->role == "ADMIN")
                    <div class="p-5 border-t border-gray-200 dark:border-dark-5">
                        <a class="flex items-center" href="{{ route('configuration-couple-view') . App\Helper::url_query_string() }}">
                            <i data-feather="users" class="w-4 h-4 mr-2"></i> Couple Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-akad-view') . App\Helper::url_query_string() }}">
                            <i data-feather="map" class="w-4 h-4 mr-2"></i> Akad Configuration
                        </a>
                        <a class="flex items-center text-theme-1 dark:text-theme-10 font-medium mt-5" href="{{ route('configuration-walimah-view') . App\Helper::url_query_string() }}">
                            <i data-feather="map-pin" class="w-4 h-4 mr-2"></i> Walimah Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-guide-view') . App\Helper::url_query_string() }}">
                            <i data-feather="file" class="w-4 h-4 mr-2"></i> File Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-bank-view') . App\Helper::url_query_string() }}">
                            <i data-feather="dollar-sign" class="w-4 h-4 mr-2"></i> Bank Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-live-view') . App\Helper::url_query_string() }}">
                            <i data-feather="phone" class="w-4 h-4 mr-2"></i> Live Configuration
                        </a>
                        <a class="flex items-center mt-5" href="{{ route('configuration-gallery-view') . App\Helper::url_query_string() }}">
                            <i data-feather="film" class="w-4 h-4 mr-2"></i> Gallery Configuration
                        </a>
                    </div>
                @endif
                <div class="p-5 border-t border-gray-200 dark:border-dark-5 flex">
                    @if(auth()->user()->role == "ADMIN")
                        <a href="{{ route('configuration-akad-view') . App\Helper::url_query_string() }}"
                           class="btn btn-primary py-1 px-2">Prev</a>
                        <a href="{{ route('configuration-guide-view') . App\Helper::url_query_string() }}"
                           class="btn btn-outline-secondary py-1 px-2 ml-auto">Next</a>
                    @endif
                </div>
            </div>
        </div>
        <!-- END: Profile Menu -->
        <div class="col-span-12 lg:col-span-8 2xl:col-span-9">
            <!-- BEGIN: Change Password -->
            <div class="intro-y box lg:mt-5">
                <div class="flex items-center p-5 border-b border-gray-200 dark:border-dark-5">
                    <h2 class="font-medium text-base mr-auto">Walimah Configuration</h2>
                </div>
                <div class="p-5">
                    <form id="inputan-form">
                        <div>
                            <label for="walimah_name" class="form-label">Location</label>
                            <input id="walimah_name" type="text" class="form-control form__inputan"
                                   placeholder="Input walimah location name"
                                   value="{{ $configuration->walimah_name }}">
                            <div id="error-walimah_name"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="walimah_date" class="form-label">Date</label>
                            <input id="walimah_date" type="text"
                                   class="datepicker form-control form__inputan"
                                   data-single-mode="true" placeholder="Input walimah date"
                                   value="{{ $configuration->walimah_date }}">
                            <div id="error-walimah_date"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="walimah_time" class="form-label">Time</label>
                            <input id="walimah_time" type="text" class="form-control form__inputan"
                                   placeholder="Input walimah time"
                                   value="{{ $configuration->walimah_time }}">
                            <div id="error-walimah_time"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="walimah_coordinate" class="form-label">Coordinate</label>
                            <input id="walimah_coordinate" type="text" class="form-control form__inputan"
                                   placeholder="Input walimah coordinates"
                                   value="{{ $configuration->walimah_coordinate }}">
                            <div id="error-walimah_coordinate"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                        <div class="mt-3">
                            <label for="walimah_maps" class="form-label">Maps</label>
                            <input id="walimah_maps" type="text" class="form-control form__inputan"
                                   placeholder="Input walimah maps"
                                   value="{{ $configuration->walimah_maps }}">
                            <div id="error-walimah_maps"
                                 class="form__inputan-error w-5/6 text-theme-6 mt-2"></div>
                        </div>
                    </form>
                    <button id="btn-update" class="btn btn-primary mt-4">Update</button>
                </div>
            </div>
            <!-- END: Change Password -->
        </div>
    </div>
@endsection

@section('script')
    <script>
        cash(function () {
            async function update() {
                // Reset state
                cash('#inputan-form').find('.form__inputan').removeClass('border-theme-6')
                cash('#inputan-form').find('.form__inputan-error').html('')

                // Post form
                let walimah_date = cash('#walimah_date').val()
                let walimah_time = cash('#walimah_time').val()
                let walimah_name = cash('#walimah_name').val()
                let walimah_coordinate = cash('#walimah_coordinate').val()
                let walimah_maps = cash('#walimah_maps').val()

                // Loading state
                cash('#btn-update').html('<i data-loading-icon="oval" data-color="white" class="w-5 h-5 mx-auto"></i>').svgLoader()
                await helper.delay(1000)

                axios.post('{{ route('configuration-walimah') }}', {
                    walimah_date: walimah_date,
                    walimah_time: walimah_time,
                    walimah_name: walimah_name,
                    walimah_coordinate: walimah_coordinate,
                    walimah_maps: walimah_maps,
                }).then(res => {
                    cash('#btn-update').html('Update');
                    if (!res.data.success) {
                        for (const [key, val] of Object.entries(res.data.errors)) {
                            cash(`#${key}`).addClass('border-theme-6')
                            cash(`#error-${key}`).html(val)
                        }
                        helper.showNotification('error', 'error update data', 'harap cek kembali inputan anda');
                    } else {
                        helper.showNotification('success', 'success update data', 'halaman akan di refresh');
                        setTimeout(function () {
                            window.location.reload(true);
                            window.location.reload(true);
                        }, 500);

                    }
                }).catch(err => {
                    cash('#btn-update').html('Update');
                    helper.showNotification('error', 'error update data');
                })
            }

            cash('form input').on('keydown', function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            })

            cash('#btn-update').on('click', function () {
                update()
            })
        })
    </script>
@endsection
