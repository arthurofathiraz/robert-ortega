<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Http;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        \jdavidbakr\LaravelCacheGarbageCollector\LaravelCacheGarbageCollector::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->call(function () {
            // hit whatsapp health check
            // header
            $headers = [
                'Authorization' => 'Basic ' . base64_encode(env('WHATSAPP_BASIC_USERNAME') . ":" . env('WHATSAPP_BASIC_PASSWORD')),
            ];

            print_r($headers);
            print(env('WHATSAPP_URL') . "/healthcheck");

            // get received event github api, by latest 100 and page 1
            $response = Http::withHeaders($headers)->get(
                env('WHATSAPP_URL') . "/healthcheck"
            )->json();

            print_r($response);
        })
            ->name('healthcheck_whatsapp')
            ->cron("*/1 * * * *")
            ->withoutOverlapping()
            ->runInBackground();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
