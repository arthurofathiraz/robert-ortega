<?php

namespace App\Imports;

use App\Models\Invitation;
use Maatwebsite\Excel\Concerns\ToModel;

class InvitationsImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Invitation();
    }
}
