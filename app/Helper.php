<?php

namespace App;

use App\Models\Label;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Models\Invitation;
use Illuminate\Support\Facades\Http;

class Helper
{
    public static function formatCode($code)
    {
        return str_replace('>', 'HTMLCloseTag', str_replace('<', 'HTMLOpenTag', $code));
    }

    public static function create_slug($string)
    {
        $string = Str::slug($string);
        $string = str_replace("-", "_", $string);

        return $string;
    }

    public static function mime_type($string)
    {
        $result = '';

        switch ($string) {
            case 'image/jpeg':
                $result = 'jpeg';
                break;
            case 'image/png':
                $result = 'png';
                break;
            case 'image/jpg':
                $result = 'jpg';
                break;
            case 'application/pdf':
                $result = 'pdf';
                break;
            case 'audio/mpeg':
                $result = 'mp3';
                break;
        }

        return $result;
    }

    public static function url_query_string()
    {
        return !empty(request()->getQueryString()) ? '?' . request()->getQueryString() : '';
    }

    public static function get_paginate_number($page, $limit)
    {
        // nomor = ((current page nya ke berapa - 1) * per page nya berapa) + 1
        return (($page - 1) * $limit) + 1;
    }

    public static function route_name($route)
    {
        // replace all numbers
        $result = preg_replace('/[0-9]+/', '', $route);

        // replace all words
        $result = preg_replace([
            '/overview/',
            '/view/',
            '/add/',
            '/get/',
        ], [
            '',
            '',
            '',
            '',
        ], $result);

        // split string
        $result = explode('-', $result);

        return ucwords(implode(' ', $result));
    }

    public static function generate_rsvp_code()
    {
        do {
            $code = random_int(100000, 999999);
        } while (Invitation::where("rsvp_code", $code)->first());

        return $code;
    }

    public static function generate_qr_code($generate = '')
    {
        return QrCode::size(200)
            ->color(25, 42, 86)
            ->margin(2)
            ->format('svg')
            ->generate($generate);
    }

    public static function generate_qr_file($generate = '', $file_path = '')
    {
        return QrCode::size(350)
            ->color(25, 42, 86)
            ->margin(2)
            ->format('svg')
            ->generate($generate, $file_path);
    }

    public static function send_whatsapp($url = '', $body = [])
    {
        // check contains message in body, replace with bitly
        if (!empty($body['message'])) {
            $body['message'] = self::short_link($body['message']);
        }

        return Http::timeout(60)
            ->retry(5, 500)
            ->withBasicAuth(
                env('WHATSAPP_BASIC_USERNAME'),
                env('WHATSAPP_BASIC_PASSWORD'),
            )->post(
                $url,
                $body,
            )->json();
    }

    public static function get_priority_label($invitation)
    {
        // get label
        if (count($invitation['invitation_labels']) <= 0) {
            return Label::where('name', 'streaming')->first();
        }

        // loop invitation labels
        $priority_label = [];
        $labels = [];
        foreach ($invitation['invitation_labels'] as $label) {
            $priority_label[] = $label['labels']['priority'];
            $labels[$label['labels']['priority']] = $label['labels'];
        }

        // get lowest priority label
        return $labels[min($priority_label)];
    }

    public static function get_qrcode_message()
    {
        return [
            'message' => '*[INFO]*
Silakan tunjukkan QR code di bawah ini di meja penerima tamu',
        ];
    }

    public static function get_location_message()
    {
        return [
            'message' => '*[INFO]*
Silakan klik maps di bawah ini untuk menuju lokasi acara pernikahan',
        ];
    }

    public static function get_rsvp_message($name = '', $rsvp_code = '', $mobile = '', $is_auto_rsvp = false)
    {
        // set auto rsvp
        $rsvp_url = $is_auto_rsvp ? "&rsvp=false" : "";

        return [
            'message' => '*[INFO]*
Harap masukkan kode RSVP berikut ini di halaman website, sebagai konfirmasi untuk data undangan digital dengan cara sbb:
1. kunjungi https://www.fathariztalita.com?to=' . urlencode($name) . $rsvp_url . ' dengan browser
2. pada bagian kanan, scroll ke bawah sampai menemukan rsvp undangan
3. masukkan no. handphone: "' . $mobile . '" dan code: "' . $rsvp_code . '" (tanpa tanda kutip)
4. anda akan mendapatkan pesan melalui whatsapp ke nomor berikut ini ' . $mobile . ' berupa QR Code
5. harap simpan dan scan QR Code ini di meja penerima tamu',
        ];
    }

    public static function get_streaming_message($zoom_live_url = '', $meet_live_url = '', $instagram_live_id = '')
    {
        $data = [
            'message' => '*[INFO]*
Silakan menonton live streaming acara akad kami pada tautan berikut ini:',
        ];

        if (!empty($zoom_live_url)) {
            $data['message'] = $data['message'] . '
- ' . $zoom_live_url . '';
        }

        if (!empty($meet_live_url)) {
            $data['message'] = $data['message'] . '
- ' . $meet_live_url . '';
        }

        if (!empty($instagram_live_id)) {
            $data['message'] = $data['message'] . '
- https://www.instagram.com/' . $instagram_live_id . '/live';
        }

        return $data;
    }

    public static function get_qrcode_image($qr_code_file = '')
    {
        $data = [
            'caption' => 'QR Code.png',
        ];

        if (env('APP_ENV') == 'local') {
            $data['url'] = 'https://www.svgrepo.com/show/125020/qr-code.svg';
        } else {
            $data['url'] = env('APP_URL') . '/storage/' . $qr_code_file;
        }

        return $data;
    }

    public static function get_qrcode_rsvp_message($name)
    {
        return [
            'message' => '*[RSVP SUCCESS]*
Terimakasih *_' . $name . '_*, anda telah berhasil melakukan RSVP, semoga diberikan kesehatan dan keselamatan sampai hari H dan setelahnya.'
        ];
    }

    public static function get_h7_message($invitation, $configuration, $is_ortu = false)
    {
        $rsvp_url = $invitation->is_auto_rsvp ? "&rsvp=false" : "";
        $url = urlencode($invitation->name) . $rsvp_url;
        $middle = '*' . $configuration->female_name . '*
*&*
*' . $configuration->male_name . '*';

        if ($is_ortu) {
            $middle = '*' . $configuration->female_name . '*
' . self::replace_newline($configuration->female_description) . '
*&*
*' . $configuration->male_name . '*
' . self::replace_newline($configuration->male_description);
        }

        return [
            'message' => '*[REMINDER]*
Assalaamu\'alaykum *_' . $invitation->name . '_*,
Kindly remind, H-7 menuju hari pernikahan antara:

' . $middle . '

Yang akan dilaksanakan pada:
📅 Hari/Tanggal : Minggu, 07 November 2021
⏱️ Akad 07.30 - 10.00 | Resepsi 10.30 - 13:00 WIB
📍 Lokasi : Masjid Nurul Jamil, Dago, Bandung
🌐 Website : https://fathariztalita.com?to=' . $url . '
👔 Dresscode : Putih

Semoga Allah jaga dalam kesehatan hingga hari-H.'
        ];
    }

    public static function get_h2_message($invitation, $configuration, $is_ortu = false)
    {
        $rsvp_url = $invitation->is_auto_rsvp ? "&rsvp=false" : "";
        $url = urlencode($invitation->name) . $rsvp_url;
        $middle = '*' . $configuration->female_name . '*
*&*
*' . $configuration->male_name . '*';

        if ($is_ortu) {
            $middle = '*' . $configuration->female_name . '*
' . self::replace_newline($configuration->female_description) . '
*&*
*' . $configuration->male_name . '*
' . self::replace_newline($configuration->male_description);
        }

        return [
            'message' => '*[REMINDER]*
Assalaamu\'alaykum *_' . $invitation->name . '_*,
Kindly remind, H-2 menuju hari pernikahan antara:

' . $middle . '

Yang akan dilaksanakan pada:
📅 Hari/Tanggal : Minggu, 07 November 2021
⏱️ Akad 07.30 - 10.00 | Resepsi 10.30 - 13:00 WIB
📍 Lokasi : Masjid Nurul Jamil, Dago, Bandung
🌐 Website : https://fathariztalita.com?to=' . $url . '
👔 Dresscode : Putih

Semoga Allah jaga dalam kesehatan hingga hari-H.'
        ];
    }

    public static function get_sesi1_message($invitation, $configuration, $is_ortu = false)
    {
        $middle = '*' . $configuration->female_name . '*
*&*
*' . $configuration->male_name . '*';

        if ($is_ortu) {
            $middle = '*' . $configuration->female_name . '*
' . self::replace_newline($configuration->female_description) . '
*&*
*' . $configuration->male_name . '*
' . self::replace_newline($configuration->male_description);
        }

        return [
            'message' => '*[SESI 1]*
Assalaamu\'alaykum *_' . $invitation->name . '_*,
Dalam ikhtiar pemutusan rantai penyebaran virus COVID-19 di hari pernikahan antara:

' . $middle . '

Anda berada dalam :
⏱ Sesi 1 : 10.30 WIB - 11.30 WIB

Diharapkan tamu undangan datang sesuai dengan sesi yang di tentukan, sebagai bagian dari peraturan pemerintah dalam menyelenggarakan acara resepsi pernikahan.'
        ];
    }

    public static function get_sesi2_message($invitation, $configuration, $is_ortu = false)
    {
        $middle = '*' . $configuration->female_name . '*
*&*
*' . $configuration->male_name . '*';

        if ($is_ortu) {
            $middle = '*' . $configuration->female_name . '*
' . self::replace_newline($configuration->female_description) . '
*&*
*' . $configuration->male_name . '*
' . self::replace_newline($configuration->male_description);
        }

        return [
            'message' => '*[SESI 2]*
Assalaamu\'alaykum *_' . $invitation->name . '_*
Dalam ikhtiar pemutusan rantai penyebaran virus COVID-19 di hari pernikahan antara:

' . $middle . '

Anda berada dalam :
⏱ Sesi 2 : 12.00 WIB - 13.00 WIB

Diharapkan tamu undangan datang sesuai dengan sesi yang di tentukan, sebagai bagian dari peraturan pemerintah dalam menyelenggarakan acara resepsi pernikahan.'
        ];
    }

    public static function get_guide_message($invitation, $configuration)
    {
        $middle = '*' . $configuration->female_name . '* & *' . $configuration->male_name . '*';

        return [
            'message' => '*[INFO]*
Assalaamu\'alaykum *_' . $invitation->name . '_*,
Berikut adalah dokumen panduan untuk hadir di hari pernikahan ' . $middle
        ];
    }

    public static function get_guide_image($configuration, $is_ortu = false)
    {
        $data = [
            'caption' => 'Guide.png',
        ];

        if (env('APP_ENV') == 'local') {
            $data['url'] = 'https://www.svgrepo.com/show/125020/qr-code.svg';
        } else {
            if ($is_ortu) {
                $data['url'] = env('APP_URL') . '/storage/' . $configuration->guide_old_file;
            } else {
                $data['url'] = env('APP_URL') . '/storage/' . $configuration->guide_file;
            }
        }

        return $data;
    }

    public static function get_location_point($walimah_latitude = '', $walimah_longitude = '', $walimah_name = '')
    {
        return [
            'latitude' => $walimah_latitude,
            'longitude' => $walimah_longitude,
            'description' => $walimah_name,
        ];
    }

    public static function get_percentage($result)
    {
        return number_format((float)$result, 0, '.', '');
    }

    public static function short_link($text = '')
    {
        // search all link
        $pattern = '~[a-z]+://\S+~';
        if (preg_match_all($pattern, $text, $out)) {
            if (!empty($out)) {
                foreach ($out[0] as $link) {

                    // get bitly api request
                    $bitly_link = Http::withHeaders(
                        [
                            'Authorization' => 'Bearer ' . env('BITLY_TOKEN'),
                        ]
                    )->post(
                        env('BITLY_URL'),
                        [
                            'long_url' => $link,
                        ]
                    )->json();

                    // check if not error
                    if (empty($bitly_link['errors'])) {
                        $text = str_replace($link, $bitly_link['link'], $text);
                    }
                }
            }
        }

        return $text;
    }

    public static function check_is_ortu($type = '')
    {
        if (str_contains($type, 'ortu')) {
            return true;
        } else {
            return false;
        }
    }

    public static function replace_newline($str = '')
    {
        return trim(preg_replace('/\s+/', ' ', $str));
    }
}
