<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof TokenInvalidException) {
                return response()->json(response_error(
                    'error token is invalid'
                ), 401);
            } else if ($e instanceof TokenExpiredException) {
                return response()->json(response_error(
                    'error token is expired'
                ), 401);
            } else {
                return response()->json(response_error(
                    'error authorization token not found'
                ), 401);
            }
        }
        return $next($request);
    }
}
