<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Models\Configuration;
use App\Models\Gallery;
use App\Models\Invitation;
use App\Models\InvitationLog;
use App\Models\Rsvp;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->key = 'configuration_cache';
        $this->middleware('jwt.verify', ['except' => ['login', 'refresh_token']]);
    }

    public function login(Request $request)
    {
        // validasi contains authorization
        $authorization = $request->header('Authorization', null);
        if (!$authorization) {
            return response()->json(response_error(
                "error get authorization"
            ), 401);
        }

        // get header basic auth
        $header_basic_auth = explode(" ", $authorization);
        if (count($header_basic_auth) != 2) {
            return response()->json(response_error(
                "error basic auth authorization"
            ), 401);
        }

        // validate basic auth
        $basic_auth = base64_encode(env('BASIC_USERNAME') . ":" . env('BASIC_PASSWORD'));
        if ($basic_auth != $header_basic_auth[1]) {
            return response()->json(response_error(
                "error validate basic auth authorization"
            ), 401);
        }

        $rule = [
            'email' => 'required',
            'password' => 'required',
        ];

        // validate data form email and password
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                implode(", ", $validate->messages()->all()),
                $validate->errors(),
            ), 400);
        }

        // validate
        if (!$token = auth('api')->attempt($validate->validated())) {
            return response()->json(response_error(
                "error email / password invalid",
            ), 400);
        }

        // generate token
        $jwt = $this->create_new_token($token);

        return response()->json(response_success(
            "success login",
            $jwt,
        ), 200);
    }

    public function refresh_token(Request $request)
    {
        // validate
        if (!$token = auth('api')->refresh()) {
            return response()->json(response_error(
                "error refresh_token invalid",
            ), 400);
        }

        // generate token
        $jwt = $this->create_new_token($token);

        return response()->json(response_success(
            "success refresh token",
            $jwt,
        ), 200);
    }

    public function get_configuration(Request $request)
    {
        if (!$configuration = Cache::get($this->key, null)) {
            $configuration = Configuration::first();
            if (!$configuration) {
                return response()->json(response_error(
                    "error get configuration"
                ), 400);
            }

            // unset some field
            unset($configuration['created_at']);
            unset($configuration['updated_at']);
            unset($configuration['guide_file']);
            unset($configuration['invitation_file']);
            unset($configuration['id']);

            // set url
            if (!empty($configuration['male_photo'])) {
                $configuration['male_photo'] = env('APP_URL') . '/storage/' . $configuration['male_photo'];
            }

            if (!empty($configuration['female_photo'])) {
                $configuration['female_photo'] = env('APP_URL') . '/storage/' . $configuration['female_photo'];
            }

            if (!empty($configuration['qris_file'])) {
                $configuration['qris_file'] = env('APP_URL') . '/storage/' . $configuration['qris_file'];
            }

            if (!empty($configuration['protocol_file'])) {
                $configuration['protocol_file'] = env('APP_URL') . '/storage/' . $configuration['protocol_file'];
            }

            if (!empty($configuration['song_file'])) {
                $configuration['song_file'] = env('APP_URL') . '/storage/' . $configuration['song_file'];
            }

            // get gallery
            $gallery = Gallery::all();
            if (!$configuration) {
                return response()->json(response_error(
                    "error get configuration"
                ), 400);
            }

            // loop gallery
            foreach ($gallery as $gal) {
                unset($gal['created_at']);
                unset($gal['updated_at']);
                unset($gal['id']);

                // set url
                $gal['photo_file'] = env('APP_URL') . '/storage/' . $gal['photo_file'];
            }

            // set gallery
            $configuration['gallery'] = $gallery;

            // set cache
            Cache::put('cache_configuration', $this->key, 60);
        }

        return response()->json(response_success(
            "success get configuration",
            $configuration,
        ), 200);
    }

    public function post_rsvp_code(Request $request)
    {
        $rule = [
            'mobile' => 'required|exists:invitations,mobile',
            'rsvp_code' => 'required|exists:invitations,rsvp_code',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                implode(", ", $validate->messages()->all()),
                $validate->errors(),
            ), 400);
        }

        // get data
        $data = Invitation::where('mobile', $request->mobile)->where('rsvp_code', $request->rsvp_code)->first();
        if (!$data) {
            return response()->json(response_error(
                "error validate rsvp data"
            ), 400);
        }

        // update data only when status SENT
        if ($data->status == 'SENT' || $data->status == 'DRAFT') {
            $update = $data->update([
                'status' => 'RSVP',
                'rsvp_at' => Carbon::now()
            ]);
            if (!$update) {
                return response()->json(response_error(
                    "error set rsvp data"
                ), 400);
            }

            // update log
            $update_log = InvitationLog::create([
                'user_id' => auth()->user()->id,
                'invitation_id' => $data->id,
                'status' => 'RSVP',
                'remarks' => '',
            ]);
            if (!$update_log) {
                return response()->json(response_error(
                    "error set rsvp data"
                ), 400);
            }
        }

        $response = Helper::send_whatsapp(
            env('WHATSAPP_URL') . '/message/' . $data->mobile . '/string',
            Helper::get_qrcode_rsvp_message($data->name),
        );
        if (!$response['success']) {
            return response()->json(response_error(
                "error send whatsapp data " . $response['messsage']
            ), 400);
        }

        $response = Helper::send_whatsapp(
            env('WHATSAPP_URL') . '/message/' . $data->mobile . '/string',
            Helper::get_qrcode_message(),
        );
        if (!$response['success']) {
            return response()->json(response_error(
                "error send whatsapp data " . $response['messsage']
            ), 400);
        }

        $response = Helper::send_whatsapp(
            env('WHATSAPP_URL') . '/message/' . $data->mobile . '/media',
            Helper::get_qrcode_image($data->qr_code_file),
        );
        if (!$response['success']) {
            return response()->json(response_error(
                "error send whatsapp data " . $response['messsage']
            ), 400);
        }

        return response()->json(response_success(
            "success rsvp",
            $request->all(),
        ), 200);
    }

    public function post_rsvp_send(Request $request)
    {
        $rule = [
            'nama' => 'required|min:1|max:200',
            'alamat' => 'required|min:1|max:200',
            'hadir' => 'required|in:ya,tidak',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                implode(", ", $validate->messages()->all()),
                $validate->errors(),
            ), 400);
        }

        // update data
        $update = Rsvp::create([
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'hadir' => $request->hadir,
            'jumlah' => 0,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data create"

            ), 200);
        }

        return response()->json(response_success(
            "success rsvp",
            $request->all(),
        ), 200);
    }


    protected function create_new_token($token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => env('JWT_TTL'),
            'user' => [
                'name' => auth('api')->user()->name,
            ]
        ];
    }
}
