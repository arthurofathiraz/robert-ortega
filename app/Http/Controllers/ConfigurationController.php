<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Models\User;
use App\Models\Configuration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use Axiom\Rules\LocationCoordinates;
use Faker\Factory as Faker;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;
use App\Helper;

class ConfigurationController extends Controller
{
    public function __construct()
    {
        $this->key = 'configuration_cache';
    }

    public function personalView()
    {
        return view('pages/configuration/personal');
    }

    public function personal(Request $request)
    {
        $rule = [
            'name' => 'required|min:1|max:50',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // update data
        $update = User::where('id', auth()->user()->id)->update([
            'name' => $request->name,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        $user = User::where('id', auth()->user()->id)->first();

        return response()->json(response_success(
            "success update data",
            $user,
        ), 200);
    }

    public function coupleView()
    {
        $configuration = Configuration::first();

        return view('pages/configuration/couple', [
            'configuration' => $configuration,
        ]);
    }

    public function couple(Request $request)
    {
        $rule = [
            'male_name' => 'required|min:1|max:100',
            'male_nickname' => 'required|min:1|max:100',
            'male_description' => 'required|min:1|max:1000',
            'female_name' => 'required|min:1|max:100',
            'female_nickname' => 'required|min:1|max:100',
            'female_description' => 'required|min:1|max:1000',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // update data
        $update = Configuration::first()->update([
            'male_name' => $request->male_name,
            'male_description' => $request->male_description,
            'male_nickname' => $request->male_nickname,
            'female_name' => $request->female_name,
            'female_description' => $request->female_description,
            'female_nickname' => $request->female_nickname,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        Cache::forget($this->key);

        $configuration = Configuration::first();

        return response()->json(response_success(
            "success update data",
            $configuration,
        ), 200);
    }

    public function akadView()
    {
        $configuration = Configuration::first();

        $configuration['akad_coordinate'] = '';
        if (!empty($configuration->akad_latitude) && !empty($configuration->akad_longitude)) {
            $configuration['akad_coordinate'] = $configuration->akad_latitude . ',' . $configuration->akad_longitude;
        }

        return view('pages/configuration/akad', [
            'configuration' => $configuration,
        ]);
    }

    public function akad(Request $request)
    {
        $rule = [
            'akad_date' => 'required|date_format:Y-m-d',
            'akad_time' => 'required|date_format:H:i',
            'akad_name' => 'required|min:1|max:1000',
            'akad_maps' => 'required|min:1|max:1000|url',
            'akad_coordinate' => ['required'],
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // get lat long
        $coordinates = explode(",", $request->akad_coordinate);

        // update data
        $update = Configuration::first()->update([
            'akad_date' => $request->akad_date,
            'akad_time' => $request->akad_time,
            'akad_name' => $request->akad_name,
            'akad_latitude' => $coordinates[0],
            'akad_longitude' => $coordinates[1],
            'akad_maps' => $request->akad_maps,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        Cache::forget($this->key);

        $configuration = Configuration::first();

        return response()->json(response_success(
            "success update data",
            $configuration,
        ), 200);
    }

    public function walimahView()
    {
        $configuration = Configuration::first();

        $configuration['walimah_coordinate'] = '';
        if (!empty($configuration->walimah_latitude) && !empty($configuration->walimah_longitude)) {
            $configuration['walimah_coordinate'] = $configuration->walimah_latitude . ',' . $configuration->walimah_longitude;
        }

        return view('pages/configuration/walimah', [
            'configuration' => $configuration,
        ]);
    }

    public function walimah(Request $request)
    {
        $rule = [
            'walimah_date' => 'required|date_format:Y-m-d',
            'walimah_time' => 'required|date_format:H:i',
            'walimah_name' => 'required|min:1|max:1000',
            'walimah_maps' => 'required|min:1|max:1000|url',
            'walimah_coordinate' => ['required'],
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // get lat long
        $coordinates = explode(",", $request->walimah_coordinate);

        // update data
        $update = Configuration::first()->update([
            'walimah_date' => $request->walimah_date,
            'walimah_time' => $request->walimah_time,
            'walimah_name' => $request->walimah_name,
            'walimah_latitude' => $coordinates[0],
            'walimah_longitude' => $coordinates[1],
            'walimah_maps' => $request->walimah_maps,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        Cache::forget($this->key);

        $configuration = Configuration::first();

        return response()->json(response_success(
            "success update data",
            $configuration,
        ), 200);
    }

    public function bankView()
    {
        $configuration = Configuration::first();

        return view('pages/configuration/bank', [
            'configuration' => $configuration,
        ]);
    }

    public function bank(Request $request)
    {
        $rule = [
            'is_active' => 'required|in:yes,no',
        ];

        $data_update = [
            'bank_account_number' => null,
            'bank_account_name' => null,
            'bank_account' => null,
        ];

        if ($request->is_active == 'yes') {
            $rule['bank_account_number'] = 'required';
            $rule['bank_account_name'] = 'required';
            $rule['bank_account'] = 'required';

            $data_update = [
                'bank_account_number' => $request->bank_account_number,
                'bank_account_name' => $request->bank_account_name,
                'bank_account' => $request->bank_account,
            ];
        }

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // update data
        $update = Configuration::first()->update($data_update);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        Cache::forget($this->key);

        $configuration = Configuration::first();

        return response()->json(response_success(
            "success update data",
            $configuration,
        ), 200);
    }

    public function liveView()
    {
        $configuration = Configuration::first();

        return view('pages/configuration/live', [
            'configuration' => $configuration,
        ]);
    }

    public function live(Request $request)
    {
        $rule = [
            'is_active_zoom' => 'required|in:yes,no',
            'is_active_meet' => 'required|in:yes,no',
            'is_active_instagram' => 'required|in:yes,no',
        ];

        $data_update = [
            'zoom_live_url' => null,
            'meet_live_url' => null,
            'instagram_live_id' => null,
        ];
        if ($request->is_active_zoom == 'yes') {
            $rule['zoom_live_url'] = 'required';

            $data_update['zoom_live_url'] = $request->zoom_live_url;
        }

        if ($request->is_active_meet == 'yes') {
            $rule['meet_live_url'] = 'required';

            $data_update['meet_live_url'] = $request->meet_live_url;
        }

        if ($request->is_active_instagram == 'yes') {
            $rule['instagram_live_id'] = 'required';

            $data_update['instagram_live_id'] = $request->instagram_live_id;
        }

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // update data
        $update = Configuration::first()->update($data_update);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        Cache::forget($this->key);

        $configuration = Configuration::first();

        return response()->json(response_success(
            "success update data",
            $configuration,
        ), 200);
    }

    public function guideView()
    {
        $configuration = Configuration::first();

        return view('pages/configuration/guide', [
            'configuration' => $configuration,
        ]);
    }

    public function galleryView(Request $request)
    {
        // set pagination limit
        $per_page = 9;

        // set current page
        $page = (!empty($request->get('page'))) ? $request->get('page') : 1;

        // set offset
        $offset = ($page - 1) * $per_page;

        // gallery query
        $gallery_query = Gallery::query()
            ->orderBy('created_at', 'asc');

        // set result
        $total = $gallery_query->count();
        $gallerys = $gallery_query->skip($offset)->take($per_page)
            ->get()
            ->toArray();

        // set pagination
        $pagination = new LengthAwarePaginator($gallerys, $total, $per_page, $page, ['path' => $request->url(), 'query' => $request->query()]);

        // set no
        $no = Helper::get_paginate_number($page, $per_page);

        return view('pages/configuration/gallery', [
            'gallerys' => $gallerys,
            'pagination' => $pagination,
            'no' => $no,
        ]);
    }

    public function galleryRemove(Request $request)
    {
        $rule = [
            'id' => 'required|exists:galleries,id',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // delete data
        $delete = Gallery::where('id', $request->id)->first()->delete();

        if (!$delete) {
            return response()->json(response_error(
                "error remove data"
            ), 200);
        }

        return response()->json(response_success(
            "success remove data",
            [],
        ), 200);
    }

    public function guideUpload(Request $request)
    {
        $url = $this->uploadFile($request);

        // update data
        $update = Configuration::first()->update([
            'guide_file' => $url,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        Cache::forget($this->key);

        return response()->json(response_success(
            "success update data",
            [
                'url' => $url,
            ],
        ), 200);
    }

    public function guideOldUpload(Request $request)
    {
        $url = $this->uploadFile($request);

        // update data
        $update = Configuration::first()->update([
            'guide_old_file' => $url,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        Cache::forget($this->key);

        return response()->json(response_success(
            "success update data",
            [
                'url' => $url,
            ],
        ), 200);
    }

    public function invitationUpload(Request $request)
    {
        $url = $this->uploadFile($request);

        // update data
        $update = Configuration::first()->update([
            'invitation_file' => $url,
        ]);

        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        Cache::forget($this->key);

        return response()->json(response_success(
            "success update data",
            [
                'url' => $url,
            ],
        ), 200);
    }

    public function qrisUpload(Request $request)
    {
        $url = $this->uploadFile($request);

        // update data
        $update = Configuration::first()->update([
            'qris_file' => $url,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        Cache::forget($this->key);

        return response()->json(response_success(
            "success update data",
            [
                'url' => $url,
            ],
        ), 200);
    }

    public function protocolUpload(Request $request)
    {
        $url = $this->uploadFile($request);

        // update data
        $update = Configuration::first()->update([
            'protocol_file' => $url,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        Cache::forget($this->key);

        return response()->json(response_success(
            "success update data",
            [
                'url' => $url,
            ],
        ), 200);
    }

    public function songUpload(Request $request)
    {
        $url = $this->uploadFile($request);

        // update data
        $update = Configuration::first()->update([
            'song_file' => $url,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        Cache::forget($this->key);

        return response()->json(response_success(
            "success update data",
            [
                'url' => $url,
            ],
        ), 200);
    }

    public function maleUpload(Request $request)
    {
        $url = $this->uploadFile($request);

        // update data
        $update = Configuration::first()->update([
            'male_photo' => $url,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        Cache::forget($this->key);

        return response()->json(response_success(
            "success update data",
            [
                'url' => $url,
            ],
        ), 200);
    }

    public function femaleUpload(Request $request)
    {
        $url = $this->uploadFile($request);

        // update data
        $update = Configuration::first()->update([
            'female_photo' => $url,
        ]);
        if (!$update) {
            return response()->json(response_error(
                "error update data"
            ), 200);
        }

        Cache::forget($this->key);

        return response()->json(response_success(
            "success update data",
            [
                'url' => $url,
            ],
        ), 200);
    }

    public function galleryUpload(Request $request)
    {
        $url = $this->uploadFile($request);

        // update data
        $update = Gallery::create([
            'photo_file' => $url,
        ]);

        if (!$update) {
            return response()->json(response_error(
                "error insert data"
            ), 200);
        }

        Cache::forget($this->key);

        return response()->json(response_success(
            "success insert data",
            [
                'url' => $url,
            ],
        ), 200);
    }

    public function uploadFile($request)
    {
        $rule = [
            'file' => 'required',
        ];

        // validate data
        $validate = Validator::make($request->all(), $rule);
        if ($validate->fails()) {
            return response()->json(response_error(
                "error validasi data",
                $validate->errors(),
            ), 200);
        }

        // get input
        $input = $request->all();
        $faker = Faker::create('id');
        $url = '';

        // set data file kalo ada
        if (isset($input['file'])) {
            // cari ekstensi
            $ekstensi = Helper::mime_type($input['file']->getClientMimeType());

            // bikin nama gambar
            $random = md5($faker->name . Str::random(10));
            $nama_file = Helper::create_slug($faker->name) . "_" . $random . "." . $ekstensi;

            $path = 'public/';

            // upload to local
            $request->file('file')->storeAs($path, $nama_file);

            // set nama file
            $url = $path . $nama_file;
        }

        return $url;
    }
}
