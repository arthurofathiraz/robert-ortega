<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $invitation_id
 * @property string $status
 * @property string $remarks
 * @property string $created_at
 * @property string $updated_at
 */
class InvitationLog extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'invitation_id', 'status', 'remarks', 'created_at', 'updated_at'];

    public function invitations() {
        return $this->hasOne(Invitation::class, 'id', 'invitation_id');
    }

    public function users() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
