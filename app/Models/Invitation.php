<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $status
 * @property int $invitee_count
 * @property string $base
 * @property boolean $is_accomodation
 * @property string $accomodation_info
 * @property boolean $is_vip
 * @property boolean $is_physic_invitation
 * @property boolean $is_auto_rsvp
 * @property string $mobile
 * @property string $rsvp_code
 * @property string $qr_code_file
 * @property string $qr_code_token
 * @property string $category
 * @property string $rsvp_at
 * @property string $created_at
 * @property string $updated_at
 */
class Invitation extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'description', 'status', 'invitee_count', 'base', 'is_accomodation', 'accomodation_info', 'is_vip', 'is_physic_invitation', 'is_auto_rsvp', 'mobile', 'rsvp_code', 'qr_code_file', 'qr_code_token', 'category', 'rsvp_at', 'checked_in_at', 'created_at', 'updated_at'];

    public function invitation_labels()
    {
        return $this->hasMany(InvitationLabel::class, 'invitation_id', 'id');
    }

}
