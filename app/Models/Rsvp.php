<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Rsvp
 *
 * @property int $id
 * @property string $nama
 * @property string|null $alamat
 * @property string|null $hadir
 * @property string|null $jumlah
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class Rsvp extends Model
{
    protected $table = 'rsvp';

    protected $fillable = [
        'nama',
        'alamat',
        'hadir',
        'jumlah'
    ];
}
